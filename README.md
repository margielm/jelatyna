# Prerequisites
*	MySql 5+
*	git
*	Maven 3
*	Java 8+

# First run

1. Download/clone git repo
2. Create MySql database confitura
e.g:
```
#!MySQL
	CREATE DATABASE confitura CHARACTER SET utf8 COLLATE utf8_general_ci;
```	
3. Create MySql user confitura with password confitura, give him all grants to db (confitura)
e.g:
```
#!MySQL
	CREATE USER 'confitura'@'localhost' IDENTIFIED BY 'confitura';
	GRANT ALL ON confitura.* TO 'confitura'@'localhost';
```	
4. Run 
	mvn jetty:run
or
	run.bat
if in Windows

You should see now something like this:

	********************************************************************
	*** WARNING: Wicket is running in DEVELOPMENT mode.              ***
	***                               ^^^^^^^^^^^                    ***
	*** Do NOT deploy to your live server(s) without changing this.  ***
	*** See Application#getConfigurationType() for more information. ***
	********************************************************************

Goto localhost:8080 and see the page. Not much to see though. You need to login to admin panel and open C4P, registration and add some static content.

# Login as admin

Goto localhost:8080/admin 
user: admin@admin.pl
pass: admin

# Deployment configuration

To be able to keep html (markup) files outside of war/tomcat, please add -Dconfitura.html.dir='/yourdir'
to jvm of the webserver you use. Then, all html files which are present in that dir, will override html from
inside the war.

Please, remember that you still need to use subdirectories relevant to your page, i.e. if you want to
override BaseWebPage.html, and your add -Dconfitura.html.dir='/yourdir' to your JVM, your new file should be under:

	yourdir
	└── jelatyna
	    └── pages
	        └── confitura
	            └── BaseWebPage.html

# To generate maven site reports run


mvn site:site

package jelatyna;

import jelatyna.mounter.PageMounter;
import jelatyna.pages.ErrorPage;
import jelatyna.pages.confitura.c4p.login.LoginSpeakerPage;
import org.apache.wicket.Page;
import org.apache.wicket.RuntimeConfigurationType;
import org.apache.wicket.Session;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.request.Request;
import org.apache.wicket.request.Response;
import org.apache.wicket.settings.IExceptionSettings;
import org.apache.wicket.settings.def.RequestCycleSettings;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.apache.wicket.util.file.Path;
import org.apache.wicket.util.lang.Bytes;

public class Confitura extends WebApplication {

    public static final Bytes UPLOADED_PHOTO_MAX_SIZE = Bytes.kilobytes(300);
    public static final String FEED_URL = "/feed";
    public static final String NEWS_BASE_URL = "/news";

    public Confitura() {
    }

    public static Confitura get() {
        return (Confitura) WebApplication.get();
    }

    @Override
    public Session newSession(Request request, Response response) {
        return new ConfituraSession(request);
    }

    @Override
    protected void init() {
        this.getRequestCycleSettings().setRenderStrategy(RequestCycleSettings.RenderStrategy.ONE_PASS_RENDER);
        getMarkupSettings().setDefaultMarkupEncoding("UTF-8");
        getRequestCycleSettings().setResponseRequestEncoding("UTF-8");
        getMarkupSettings().setStripWicketTags(true);
        getResourceSettings().setThrowExceptionOnMissingResource(false);
        setExternalHtmlDirIfSystemPropertyIsPresent();
        new PageMounter(this);
        productionInit();
    }

    private void setExternalHtmlDirIfSystemPropertyIsPresent() {
        String externalDir = System.getProperty("confitura.html.dir");
        if (externalDir != null) {
            getResourceSettings().getResourceFinders().add(new Path(externalDir));
        }
    }

    protected void productionInit() {
        getComponentInstantiationListeners().add(new SpringComponentInjector(this));
        if (getConfigurationType().equals(RuntimeConfigurationType.DEPLOYMENT)) {
            getApplicationSettings().setInternalErrorPage(ErrorPage.class);
            getExceptionSettings().setUnexpectedExceptionDisplay(IExceptionSettings.SHOW_INTERNAL_ERROR_PAGE);
        }
    }

    @Override
    public Class<? extends Page> getHomePage() {
        return LoginSpeakerPage.class;
    }
}

package jelatyna.utils;

import com.google.common.base.CharMatcher;

public class TextInUrlUtils {

    public static String prettify(String text) {
        String escapedText = text.toLowerCase();

        escapedText = removeSpaces(escapedText);
        escapedText = removePolishCharacters(escapedText);
        escapedText = removeOtherSpecialCharacters(escapedText);

        return escapedText;
    }

    private static String removeSpaces(String escapedText) {
        return CharMatcher.WHITESPACE.replaceFrom(escapedText, "_");
    }

    private static String removePolishCharacters(String escapedText) {
        return escapedText
                .replace("ą", "a")
                .replace("ć", "c")
                .replace("ę", "e")
                .replace("ł", "l")
                .replace("ń", "n")
                .replace("ó", "o")
                .replace("ś", "s")
                .replace("ż", "z")
                .replace("ź", "z");
    }

    private static String removeOtherSpecialCharacters(String escapedText) {
        return CharMatcher.inRange('a', 'z')
                .or(CharMatcher.inRange('A', 'Z')
                        .or(CharMatcher.inRange('0', '9'))
                        .or(CharMatcher.is('_')))
                .retainFrom(escapedText);
    }

}

package jelatyna.repositories;

import jelatyna.domain.MainMenu;
import jelatyna.domain.MenuItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface MenuRepository extends JpaRepository<MenuItem, Integer> {

    @Query("FROM MainMenu WHERE parent IS NULL")
    public MainMenu getMainMenu();

}

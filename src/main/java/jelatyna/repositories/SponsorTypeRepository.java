package jelatyna.repositories;

import jelatyna.domain.SponsorType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SponsorTypeRepository extends JpaRepository<SponsorType, Integer> {

    @Query("FROM SponsorType ORDER BY position")
    public List<SponsorType> findAllSorted();
}

package jelatyna.repositories;

import jelatyna.domain.Sponsor;
import jelatyna.domain.SponsorType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SponsorRepository extends JpaRepository<Sponsor, Integer> {

    @Query("FROM Sponsor WHERE sponsorType = ?1 ORDER BY money desc")
    List<Sponsor> findByType(SponsorType sponsorType);

    Sponsor findByName(String name);

}

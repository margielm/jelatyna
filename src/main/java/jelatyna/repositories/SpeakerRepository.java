package jelatyna.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import jelatyna.domain.Speaker;

public interface SpeakerRepository extends UserRepository<Speaker> {

    Speaker findByToken(String token);

    Speaker findByOpenIdLogin(String login);

    @Query("SELECT DISTINCT (s) " +
            " FROM Speaker s  " +
            " JOIN s.presentations  p" +
            " WHERE p in " +
            " (SELECT p1 FROM Presentation p1 WHERE p1.accepted = true )" +
            " AND (s.accepted = TRUE) ORDER BY s.lastName, s.firstName")
    List<Speaker> allAccepted();

}

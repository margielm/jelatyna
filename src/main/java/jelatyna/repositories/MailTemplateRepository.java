package jelatyna.repositories;

import jelatyna.domain.MailTemplate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MailTemplateRepository extends JpaRepository<MailTemplate, Integer> {

    MailTemplate readByType(String type);

}

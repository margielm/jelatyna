package jelatyna.repositories;

import jelatyna.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository<USER extends User<?>> extends JpaRepository<USER, Integer> {

    USER findByMail(String eMail);
}

package jelatyna.repositories;

import jelatyna.domain.Draw;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DrawRepository extends JpaRepository<Draw, Integer> {

}

package jelatyna.mounter;

import jelatyna.Confitura;
import jelatyna.pages.volunteer.VolunteerHomePage;
import jelatyna.pages.volunteer.login.VolunteerLoginPage;
import jelatyna.pages.volunteer.scan.ScanPage;
import jelatyna.pages.volunteer.user.ChangePasswordPage;
import jelatyna.pages.volunteer.user.EditProfilePage;

public class VolunteerModuleMounter extends ModuleMounter {

    public VolunteerModuleMounter(Confitura confitura) {
        super("/volunteer", confitura);
    }

    @Override
    public void mount() {
        mountPage("", VolunteerHomePage.class);
        mountPage("/login", VolunteerLoginPage.class);
        mountPage("/profile", EditProfilePage.class);
        mountPage("/password", ChangePasswordPage.class);
        mountPage("/participant/${token}", ScanPage.class);
        mountPage("/participant", ScanPage.class);

    }

}

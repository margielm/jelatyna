package jelatyna.services;

import static com.google.common.collect.Lists.*;
import static jelatyna.domain.RegistrationStatus.*;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jelatyna.domain.Draw;
import jelatyna.domain.Participant;
import jelatyna.repositories.DrawRepository;

@Service
public class LotteryService {
    @Autowired
    private RandomGenerator randomGenerator;
    @Autowired
    private ParticipantService participantService;
    @Autowired
    private DrawRepository drawRepository;

    public Participant draw() {
        System.out.println("abc");
        List<Participant> participants = drawableParticipants();
        if (participants.isEmpty() == false) {
            return doDraw(participants);
        }
        return Participant.EMPTY;
    }

    public void clear() {
        drawRepository.deleteAllInBatch();
    }

    private Participant doDraw(List<Participant> participants) {
        int idx = randomGenerator.getRandomIdx(participants);
        Participant participant = participants.get(idx);
        saveDraw(participant);
        return participant;
    }

    private void saveDraw(Participant participant) {
        drawRepository.save(new Draw(participant));
    }

    private List<Participant> drawableParticipants() {
        List<Participant> participants = fetchProperParticipants();
        if (participants.isEmpty() == false) {
            participants.removeAll(getDrownParticipants());
        }
        return participants;
    }

    private List<Participant> fetchProperParticipants() {
        return participantService
                .findByStatus(newArrayList(TICKET_SENT))
                .limitToParticipated(true)
                .asList();
    }

    private List<Participant> getDrownParticipants() {
        return drawRepository.findAll().stream().map(d -> d.getParticipant()).collect(Collectors.toList());
    }

}

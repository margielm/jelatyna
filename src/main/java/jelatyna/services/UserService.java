package jelatyna.services;

import java.util.List;

import org.apache.wicket.markup.html.form.upload.FileUpload;

import jelatyna.domain.User;

public interface UserService<USER extends User<?>> {

    void save(USER user, FileUpload fileUpload);

    USER loginWith(String mail, String password);

    void deleteById(Integer id);

    List<USER> findAll();

    USER findById(int id);

    void changePassword(USER user, String oldPassword, String newPassword);

    void resetPassword(USER user, String newPassword);
}

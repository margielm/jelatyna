package jelatyna.services;

import static java.lang.String.*;
import static org.apache.http.entity.ContentType.*;

import java.util.Base64;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class Twitter {

    @Value("${twitter.key}")
    private String key;

    @Value("${twitter.secret}")
    private String secret;

    private CloseableHttpClient client = HttpClients.createDefault();

    private String url = "https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=%s&count=1";

    public String doGetTweetsFor(String user) throws Exception {
        HttpGet get = new HttpGet(format(url, user));
        get.addHeader("Authorization", "Bearer " + getToken());
        CloseableHttpResponse response = client.execute(get);
        return EntityUtils.toString(response.getEntity());
    }

    public String getToken() throws Exception {
        Base64.Encoder encoder = Base64.getEncoder();
        HttpPost post = new HttpPost("https://api.twitter.com/oauth2/token");
        post.setHeader("Authorization", "Basic " + encoder.encodeToString((key + ":" + secret).getBytes()));
        post.setEntity(new StringEntity("grant_type=client_credentials", create("application/x-www-form-urlencoded", "UTF-8")));
        return getAccessTokenFrom(client.execute(post).getEntity());
    }

    private String getAccessTokenFrom(HttpEntity entity) throws Exception {
        return new ObjectMapper().readTree(EntityUtils.toString(entity)).get("access_token").asText();
    }

}

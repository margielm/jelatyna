package jelatyna.services;

import java.util.List;

import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jelatyna.domain.Presentation;
import jelatyna.repositories.PresentationRepository;
import jelatyna.utils.HtmlUtils;

@Component
public class PresentationServiceImpl implements PresentationService {
    @Autowired
    private PresentationRepository repository;
    @Autowired
    private FileService fileService;

    @Override
    public void save(Presentation presentation, FileUpload fileUpload) {
        if (fileUpload != null) {
            presentation.fileName(fileUpload.getClientFileName());
        }
        presentation.description(HtmlUtils.clearHtml(presentation.getDescription()));
        Presentation savedPresentation = repository.save(presentation);
        fileService.save(fileUpload, savedPresentation);
    }

    @Override
    public Presentation findBy(Integer id) {
        return repository.findOne(id);
    }

    @Override
    public List<Presentation> acceptedPresentations() {
        return repository.findAllAccepted();
    }

}
package jelatyna.services;

import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.util.file.File;
import org.apache.wicket.util.file.Folder;
import org.springframework.stereotype.Component;

import static java.lang.String.*;

@Component
public class FileSystem {

    java.io.File[] listFilesIn(String path) {
        return new Folder(path).listFiles();
    }

    Folder createPath(String path) {
        Folder folder = new Folder(path);
        folder.mkdirs();
        return folder;
    }

    boolean delete(String path) {
        return new File(path).delete();
    }

    void saveFile(FileUpload fileUpload, String path, String fileName) {
        try {
            Folder folder = createPath(path);
            File file = new File(folder, fileName);
            fileUpload.writeTo(file);
        } catch (Exception ex) {
            throw new RuntimeException(format("Exception on saving file[%s]", fileUpload.getClientFileName()), ex);
        }
    }

}

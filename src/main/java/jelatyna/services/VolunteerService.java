package jelatyna.services;

import jelatyna.domain.Volunteer;

public interface VolunteerService extends UserService<Volunteer> {
}

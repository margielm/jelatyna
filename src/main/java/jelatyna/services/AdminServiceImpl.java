package jelatyna.services;

import jelatyna.domain.Admin;
import jelatyna.repositories.AdminRepository;
import jelatyna.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceImpl extends UserServiceImpl<Admin> implements AdminService {

    @Autowired
    private AdminRepository repository;

    @Override
    public Admin loginWith(String email, String password) {
        return super.loginWith(email, password);
    }

    @Override
    boolean oldPasswordIsCorrect(Admin user, String password) {
        return true;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected <REPO extends UserRepository<Admin>> REPO getRepository() {
        return (REPO) repository;
    }
}

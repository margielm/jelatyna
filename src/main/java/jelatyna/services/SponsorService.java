package jelatyna.services;

import static com.google.common.collect.Lists.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jelatyna.domain.EmptySponsor;
import jelatyna.domain.Sponsor;
import jelatyna.domain.SponsorType;
import jelatyna.repositories.SponsorRepository;
import jelatyna.repositories.SponsorTypeRepository;

@Service
public class SponsorService {

    @Autowired
    private SponsorTypeRepository sponsorTypeRepository;

    @Autowired
    private SponsorRepository sponsorRepository;

    @Autowired
    private FileService fileService;

    public void moveUp(SponsorType sponsorType) {
        if (sponsorType.isNotFirst()) {
            doMoveUp(sponsorType);
        }
    }

    public void moveDown(SponsorType sponsorType) {
        List<SponsorType> allTypes = findAllTypes();
        int idx = allTypes.indexOf(sponsorType);
        if (isNotLast(allTypes, idx)) {
            swypePositions(allTypes, idx, idx + 1);
        }
    }

    public List<SponsorType> findAllTypes() {
        return sponsorTypeRepository.findAllSorted();
    }

    public void save(SponsorType sponsorType) {
        setPositionIfNew(sponsorType);
        sponsorTypeRepository.save(sponsorType);
    }

    private void setPositionIfNew(SponsorType sponsorType) {
        if (sponsorType.isNew()) {
            List<SponsorType> sponsorTypes = findAllTypes();
            int newPosition = getNewPosition(sponsorTypes);
            sponsorType.position(newPosition);
        }
    }

    public List<Sponsor> findDisplayableSponsorsBy(final SponsorType sponsorType) {
        List<Sponsor> sponsors = findSponsorsBy(sponsorType);
        while (sponsors.size() < sponsorType.getMinimalNo()) {
            sponsors.add(new EmptySponsor(sponsorType));
        }
        return sponsors;
    }

    public List<Sponsor> findSponsorsBy(SponsorType sponsorType) {
        return sponsorRepository.findByType(sponsorType);
    }

    public void save(Sponsor sponsor, FileUpload fileUpload) {
        sponsorRepository.save(sponsor);
        fileService.save(fileUpload, sponsor);
    }

    public List<Sponsor> findAllSponsors() {
        return sponsorRepository.findAll();
    }

    public boolean canRemove(SponsorType sponsorType) {
        return hasAnySponsorsFor(sponsorType) == false;
    }

    public List<SponsorType> findDisplayableSponsorTypes() {
        List<SponsorType> result = newArrayList();
        for (SponsorType sponsorType : findAllTypes()) {
            if (isDisplayable(sponsorType)) {
                result.add(sponsorType);
            }
        }
        return result;
    }

    public Sponsor findById(int id) {
        return sponsorRepository.findOne(id);
    }

    public Sponsor findByName(String name) {
        return sponsorRepository.findByName(name);
    }

    public SponsorType findTypeById(int id) {
        return sponsorTypeRepository.findOne(id);
    }

    public void deleteSponsorBy(Integer id) {
        sponsorRepository.delete(id);
    }

    public void deleteTypeBy(Integer id) {
        sponsorTypeRepository.delete(id);
    }

    public List<SponsorType> getMainTypes() {
        List<SponsorType> types = findDisplayableSponsorTypes();
        return types.isEmpty() ? new ArrayList<SponsorType>() : types.subList(0, 1);
    }

    public List<SponsorType> getOtherTypes() {
        List<SponsorType> types = findDisplayableSponsorTypes();
        return types.isEmpty() ? new ArrayList<SponsorType>() : types.subList(1, Math.min(3, types.size()));
    }

    public List<SponsorType> findTypesWithSponsors() {
        List<SponsorType> withSponsors = newArrayList();
        for (SponsorType sponsorType : findAllTypes()) {
            if (hasAnySponsorsFor(sponsorType)) {
                withSponsors.add(sponsorType);
            }
        }
        return withSponsors;
    }

    private boolean hasAnySponsorsFor(SponsorType sponsorType) {
        return findSponsorsBy(sponsorType).size() > 0;
    }

    private int getNewPosition(List<SponsorType> sponsorTypes) {
        if (sponsorTypes.isEmpty()) {
            return 0;
        } else {
            return getLastPosition(sponsorTypes) + 1;
        }
    }

    private int getLastPosition(List<SponsorType> sponsorTypes) {
        return sponsorTypes.get(sponsorTypes.size() - 1).getPosition();
    }

    private boolean isNotLast(List<SponsorType> allTypes, int idx) {
        return allTypes.size() - 1 != idx;
    }

    private void doMoveUp(SponsorType sponsorType) {
        List<SponsorType> allTypes = findAllTypes();
        int idx = allTypes.indexOf(sponsorType);
        swypePositions(allTypes, idx, idx - 1);
    }

    private void swypePositions(List<SponsorType> allTypes, int idx, int idx2) {
        Integer prevPosition = allTypes.get(idx).getPosition();
        sponsorTypeRepository.save(allTypes.get(idx).position(allTypes.get(idx2).getPosition()));
        sponsorTypeRepository.save(allTypes.get(idx2).position(prevPosition));
    }

    private boolean isDisplayable(SponsorType sponsorType) {
        return sponsorType.shouldDisplayPlaceholders() || hasAnySponsorsFor(sponsorType);
    }

}

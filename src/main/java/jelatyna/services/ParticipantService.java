package jelatyna.services;

import static java.util.Collections.*;
import static org.apache.commons.lang.StringUtils.*;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jelatyna.domain.Participant;
import jelatyna.domain.RegistrationStatus;
import jelatyna.pages.admin.registration.GroupMailSender;
import jelatyna.pages.admin.registration.Participants;
import jelatyna.repositories.ParticipantRepository;

@Service
public class ParticipantService {
    @Autowired
    private ParticipantRepository repository;
    @Autowired
    private GroupMailSender sender;

    public Participant findByMail(String eMail) {
        return repository.findByMail(eMail);
    }

    public Participant findByToken(String token) {
        return repository.findByToken(token);
    }

    public List<Participant> findByLastName(String value) {
        if (isBlank(value)) {
            return emptyList();
        }
        return repository.findWithLastNameLike(value + "%");
    }

    public void toggleParticipation(Participant item) {
        item.participated(!item.isParticipated());
        save(item);
    }
    @Transactional
    public void save(Participant participant) {
        repository.save(participant);
    }

    public Participants findByStatus(Collection<RegistrationStatus> statuses) {
        if (statuses.isEmpty()) {
            return new Participants(repository.findAll());
        }
        return new Participants(repository.findByRegistrationTypeIn(statuses));
    }

    public void sendGroupMessage(Collection<RegistrationStatus> statuses, boolean mailing, boolean participated) {
        Participants participants = findByStatus(statuses);
        sender.sendMessages(participants.limitToWithMailing(mailing).limitToParticipated(participated).asList());
    }

    public List<Participant> allParticipated() {
        return repository.findRealParticipants();
    }
}

package jelatyna.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jelatyna.domain.Volunteer;
import jelatyna.repositories.UserRepository;
import jelatyna.repositories.VolunteerRepository;

@Service
public class VolunteerServiceImpl extends UserServiceImpl<Volunteer> implements VolunteerService {

    @Autowired
    private VolunteerRepository repository;

    @Override
    public Volunteer loginWith(String email, String password) {
        if (noUsersDefinedYet()) {
            return new Volunteer(email);
        } else {
            return super.loginWith(email, password);
        }
    }

    private boolean noUsersDefinedYet() {
        return getRepository().count() == 0;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected <REPO extends UserRepository<Volunteer>> REPO getRepository() {
        return (REPO) repository;
    }
}

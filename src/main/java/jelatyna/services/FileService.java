package jelatyna.services;

import static com.google.common.collect.Lists.*;
import static java.util.Comparator.*;
import static java.util.Optional.*;
import static java.util.stream.Collectors.*;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.common.base.Joiner;

import jelatyna.domain.WithFile;

@Service
public class FileService {
    private static final String SEPARATOR = "/";

    @Value("${files.folder}")
    String mainFolder;

    @Value("${files.context}")
    String context;

    @Value("${host}")
    String host;

    @Autowired
    private FileSystem fileSystem;

    public void save(FileUpload fileUpload, List<String> folders) {
        if (fileUpload != null) {
            save(fileUpload, folders, fileUpload.getClientFileName());
        }
    }

    public void save(FileUpload fileUpload, WithFile withFile) {
        save(fileUpload, newArrayList(withFile.getSubfolder()), withFile.getFileName());
    }

    public void save(FileUpload fileUpload, List<String> folders, String fileName) {
        if (fileUpload != null) {
            fileSystem.saveFile(fileUpload, toPathInFolder(folders), fileName);
        }
    }

    public String getUrlTo(WithFile withFile) {
        return ofNullable(withFile.getFileName())
                .map(name -> getUrlTo(newArrayList(withFile.getSubfolder()), withFile.getFileName()))
                .orElse("");
    }

    public String getUrlTo(List<String> folderPath, String fileName) {
        folderPath.add(fileName);
        return host + SEPARATOR + toPath(context, newArrayList(folderPath));
    }

    @SuppressWarnings("unchecked")
    public List<java.io.File> listFilesIn(List<String> path) {
        File[] items = fileSystem.listFilesIn(toPathInFolder(path));
        List<File> folders = selectAndSort(items, true);
        List<File> files = selectAndSort(items, false);
        return ListUtils.union(folders, files);
    }

    public void newFolder(List<String> path, String newFolder) {
        fileSystem.createPath(toPath(path, newFolder));
    }

    public void delete(List<String> folders, String name) {
        fileSystem.delete(toPath(folders, name));
    }

    private String toPath(List<String> path, String name) {
        List<String> folders = newArrayList(path);
        folders.add(name);
        return toPathInFolder(folders);
    }

    private String toPathInFolder(List<String> folders) {
        return toPath(mainFolder, folders);
    }

    private String toPath(String main, List<String> folders) {
        return main + SEPARATOR + Joiner.on(SEPARATOR).join(folders);
    }

    private List<File> selectAndSort(File[] items, boolean isDir) {
        return Arrays.stream(items)
                .filter(i -> i.isDirectory() == isDir)
                .sorted(comparing(File::getName))
                .collect(toList());
    }

    public String getPathTo(WithFile withFile) {
        return ofNullable(withFile.getFileName())
                .map(name -> toPath(newArrayList(withFile.getSubfolder()), name))
                .orElse("");
    }

}

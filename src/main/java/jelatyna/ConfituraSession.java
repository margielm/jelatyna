package jelatyna;

import jelatyna.domain.Admin;
import jelatyna.domain.Agenda;
import jelatyna.domain.Presentation;
import jelatyna.domain.Speaker;
import jelatyna.domain.User;
import jelatyna.domain.Volunteer;
import org.apache.wicket.Session;
import org.apache.wicket.protocol.http.WebSession;
import org.apache.wicket.request.Request;

import java.util.List;

@SuppressWarnings("serial")
public class ConfituraSession extends WebSession {
    private User<?> user;
    private Agenda agenda;
    private List<Presentation> presentations;

    public ConfituraSession(Request request) {
        super(request);
    }

    public static ConfituraSession get() {
        return (ConfituraSession) Session.get();
    }

    public <T extends User<?>> T setUser(T user) {
        this.user = user;
        return user;
    }

    public boolean hasAdmin() {
        return user != null && user instanceof Admin;
    }

    public boolean hasSpeaker() {
        return user != null && user instanceof Speaker;
    }

    public boolean isVolunteerAvailable() {
        return user != null && user instanceof Volunteer;
    }

    @SuppressWarnings("unchecked")
    public <T extends User<?>> T getUser() {
        return (T) this.user;
    }

    public Admin getAdmin() {
        return (Admin) this.user;
    }

    public Speaker getSpeaker() {
        return (Speaker) this.user;
    }

    public Volunteer getVolunteer() {
        return (Volunteer) this.user;
    }

    public Agenda getAgenda() {
        return agenda;
    }

    public void setAgenda(Agenda agenda) {
        this.agenda = agenda;
    }

    public List<Presentation> getPresentations() {
        return presentations;
    }

    public void setPresentation(List<Presentation> presentations) {
        this.presentations = presentations;

    }

}
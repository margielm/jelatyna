package jelatyna.domain.dto.converters;

import static java.util.stream.Collectors.*;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jelatyna.domain.Presentation;
import jelatyna.domain.Speaker;
import jelatyna.domain.dto.PresentationDto;
import jelatyna.domain.dto.SpeakerDto;
import jelatyna.services.FileService;

@Component
public class SpeakerConverter {

    @Autowired
    private FileService fileService;


    public SpeakerDto map(Speaker speaker) {
        if (speaker == null) {
            throw new EntityNotFoundException();
        } else {
            return (SpeakerDto) new SpeakerDto()
                    .presentations(speaker.getAcceptedPresentations().stream().map((p) -> map(p)).collect(toList()))
                    .id(speaker.getId())
                    .fullName(speaker.getFirstName(), speaker.getLastName())
                    .bio(speaker.getBio())
                    .url(speaker.getWebPage())
                    .twitter(speaker.getTwitter())
                    .photo(fileService.getUrlTo(speaker));
        }
    }

    public PresentationDto map(Presentation presentation) {
        PresentationDto pojo = new PresentationDto();
        pojo.setId(presentation.getId());
        pojo.setTitle(presentation.getTitle());
        pojo.setLongDesc(presentation.getDescription());
        return pojo;
    }
}

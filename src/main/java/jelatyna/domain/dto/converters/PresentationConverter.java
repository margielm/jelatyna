package jelatyna.domain.dto.converters;

import static java.util.stream.Collectors.*;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jelatyna.domain.Presentation;
import jelatyna.domain.dto.PresentationDto;
import jelatyna.services.FileService;

@Component
public class PresentationConverter {

    @Autowired
    private UserConverter userConverter;

    @Autowired
    private FileService fileService;

    public PresentationDto map(Presentation presentation) {
        PresentationDto presentationDto = new PresentationDto();
        presentationDto.setId(presentation.getId());
        presentationDto.setLongDesc(presentation.getDescription());
        presentationDto.setShortDesc(presentation.getShortDescription());
        presentationDto.setSpeakers(presentation.getSpeakers().stream().map(userConverter::map).collect(toList()));
        presentationDto.setTitle(presentation.getTitle());
        if (presentation.hasYouTube()) {
            presentationDto.setYouTube(presentation.getYouTube());
        }
        if (presentation.hasFile()) {
            presentationDto.setFile(fileService.getUrlTo(presentation));
        }
        return presentationDto;
    }

    public List<PresentationDto> mapAll(List<Presentation> presentations) {
        return presentations
                .stream().map(this::map).collect(toList());
    }

}

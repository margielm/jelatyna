package jelatyna.domain.dto;

public class PageDto {
    private String text;

    public PageDto(String text) {

        this.text = text;
    }

    public String getText() {
        return text;
    }
}

package jelatyna.domain.dto;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class AgendaSlotDto implements AbstractDto {
    private String room;
    private WithTitle item;

    public WithTitle getPresentation() {
        return item;
    }

    public void setItem(WithTitle item) {
        this.item = item;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public AgendaSlotDto room(final String room) {
        this.room = room;
        return this;
    }

    public AgendaSlotDto item(WithTitle item) {
        this.item = item;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o, new String[]{"room"});
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this, new String[]{"room"});
    }

    @Override
    public String toString() {
        return "AgendaSlotDto{" +
                "room='" + room + '\'' +
                ", item=" + item +
                '}';
    }
}

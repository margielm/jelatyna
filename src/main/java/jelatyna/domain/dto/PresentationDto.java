package jelatyna.domain.dto;

import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class PresentationDto implements AbstractDto, WithTitle {
    private int id;

    private String title;

    private String shortDesc;

    private String longDesc;

    private List<UserDto> speakers;

    private String youTube;

    private String file;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getLongDesc() {
        return longDesc;
    }

    public void setLongDesc(String longDesc) {
        this.longDesc = longDesc;
    }

    public List<UserDto> getSpeakers() {
        return speakers;
    }

    public void setSpeakers(List<UserDto> speakers) {
        this.speakers = speakers;
    }

    public PresentationDto id(Integer id) {
        this.id = id;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    public void setYouTube(String youTube) {
        this.youTube = youTube;
    }

    public String getYouTube() {
        return youTube;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getFile() {
        return file;
    }
}

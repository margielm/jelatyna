package jelatyna.domain.dto;

public interface WithTitle {

    String getTitle();
}

package jelatyna.domain;

import javax.persistence.Entity;
import javax.persistence.Lob;

@SuppressWarnings("serial")
@Entity
public class SimpleContent extends AbstractEntity<SimpleContent> implements WithTitle {

    private String title;

    @Lob
    private String content;

    @Override
    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return "SimpleContent [id=" + getId() + ", title=" + title + "]";
    }

}

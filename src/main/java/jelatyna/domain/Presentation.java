package jelatyna.domain;

import static com.google.common.collect.Lists.*;
import static org.apache.commons.lang.StringUtils.*;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@SuppressWarnings("serial")
@Entity
public class Presentation extends AbstractEntity<Presentation> implements WithFile {
    public static final int SHORT_DESC_MAX_LENGTH = 300;
    private String title;

    @Column(length = 300)
    private String shortDescription;
    @Lob
    private String description;
    @ManyToOne
    private Speaker owner;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany
    @JoinTable(name = "PRESENTATION_SPEAKER",
            inverseJoinColumns = {@JoinColumn(name = "SPEAKER_ID", nullable = false, updatable = false)},
            joinColumns = {@JoinColumn(name = "PRESENTATION_ID", nullable = false, updatable = false)})
    private List<Speaker> speakers = newArrayList();

    private String fileName;

    private String language;

    private String youTube;

    private String parleys;

    private boolean accepted = false;

    public String getTitle() {
        return title;
    }

    public Presentation setTitle(String title) {
        this.title = title;
        return this;
    }

    public Speaker getOwner() {
        return owner;
    }

    public Presentation owner(Speaker speaker) {
        this.owner = speaker;
        this.speakers.add(speaker);
        return this;
    }

    public String getDescription() {
        return description;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public Presentation toggleAccepted() {
        this.accepted = !this.accepted;
        return this;
    }

    public Presentation accepted(boolean accepted) {
        this.accepted = accepted;
        return this;
    }

    public Presentation title(String title) {
        this.title = title;
        return this;
    }

    public Presentation description(String description) {
        this.description = description;
        return this;
    }

    public void fileName(String fileName) {
        this.fileName = fileName;
    }

    public boolean hasFile() {
        return isNotBlank(fileName);
    }

    @Override
    public String getSubfolder() {
        return owner.getSubfolder();
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    public void youTube(String youTube) {
        this.youTube = youTube;
    }

    public String getYouTube() {
        return youTube;
    }

    public void parleys(String parleys) {
        this.parleys = parleys;
    }

    public String getParleys() {
        return parleys;
    }

    public boolean hasYouTube() {
        return isNotBlank(youTube);
    }

    public boolean hasParleys() {
        return isNotBlank(parleys);
    }

    public String getStatusKey() {
        return "item.status." + (accepted ? "accepted" : "new");
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public String getLanguage() {
        return language;
    }

    public List<Speaker> getSpeakers() {
        return speakers;
    }

    public void removeSpeaker(Speaker speaker) {
        speakers.remove(speaker);
    }

    public Presentation addSpeaker(Speaker speaker) {
        speakers.add(speaker);
        return this;
    }

    public boolean notOwnedBy(Speaker speaker) {
        return owner.getId().equals(speaker.getId()) == false;
    }

    public Presentation shortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
        return this;
    }
}

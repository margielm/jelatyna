package jelatyna.domain;

public interface WithMail {
    String getMail();
}

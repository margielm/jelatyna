package jelatyna.domain;

import static org.apache.commons.lang.StringUtils.*;

import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@SuppressWarnings("serial")
@MappedSuperclass
public abstract class User<USER extends User<?>> extends AbstractEntity<USER> implements WithFile {

    private String firstName;

    private String lastName;

    private String mail;

    private String password;

    @Transient
    private String plainPassword;

    private String openIdLogin;

    private String webPage;

    private String twitter;

    private String fileName;

    private boolean facebookAccount;

    @Lob
    private String bio = "";

    public User() {
    }

    public String getPassword() {
        return password;
    }

    @SuppressWarnings("unchecked")
    public USER password(String password) {
        this.password = password;
        return (USER) this;
    }

    public USER plainPassword(String plainPassword) {
        this.plainPassword = plainPassword;
        return (USER) this;
    }

    @SuppressWarnings("unchecked")
    public USER firstName(String firstName) {
        this.firstName = firstName;
        return (USER) this;
    }

    @SuppressWarnings("unchecked")
    public USER lastName(String lastName) {
        this.lastName = lastName;
        return (USER) this;
    }

    @SuppressWarnings("unchecked")
    public USER webPage(String page) {
        this.webPage = page;
        return (USER) this;
    }

    @SuppressWarnings("unchecked")
    public USER twitter(String twitter) {
        this.twitter = twitter;
        return (USER) this;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMail() {
        return mail;
    }

    public String getFullName() {
        if (isBlank(firstName) && isBlank(lastName)) {
            return "";
        } else {
            return getFirstName() + " " + getLastName();
        }
    }

    public String getBio() {
        return bio;
    }

    @SuppressWarnings("unchecked")
    public USER mail(String mail) {
        this.mail = mail;
        return (USER) this;
    }

    public String getOpenIdLogin() {
        return openIdLogin;
    }

    @SuppressWarnings("unchecked")
    public USER openIdLogin(String openIdLogin) {
        this.openIdLogin = openIdLogin;
        return (USER) this;
    }

    public boolean openIdNotUsed() {
        return openIdLogin == null;
    }

    public String getWebPage() {
        return webPage;
    }

    public String getTwitter() {
        return twitter;
    }

    public User<USER> bio(String bio) {
        this.bio = bio;
        return this;
    }

    public boolean isProfileFilled() {
        return isNotBlank(this.getBio());
    }

    public boolean isFacebookAccount() {
        return facebookAccount;
    }

    public void setFacebookAccount(boolean facebookAccount) {
        this.facebookAccount = facebookAccount;
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getPlainPassword() {
        return plainPassword;
    }

}

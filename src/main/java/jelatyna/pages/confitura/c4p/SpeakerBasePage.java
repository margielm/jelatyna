package jelatyna.pages.confitura.c4p;

import jelatyna.ConfituraSession;
import jelatyna.pages.confitura.BaseWebPage;
import jelatyna.pages.confitura.c4p.login.LoginSpeakerPage;
import jelatyna.security.AuthenticationUserDetails;
import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@SuppressWarnings("serial")
public class SpeakerBasePage extends BaseWebPage {
    public SpeakerBasePage() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            if (authentication.getPrincipal() instanceof AuthenticationUserDetails) {
                AuthenticationUserDetails userDetails = (AuthenticationUserDetails) authentication.getPrincipal();
                ConfituraSession.get().setUser(userDetails.getSpeaker());
            }
        }
        if (ConfituraSession.get().hasSpeaker() == false) {
            throw new RestartResponseAtInterceptPageException(LoginSpeakerPage.class);
        }
    }

}

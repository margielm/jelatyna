package jelatyna.pages.confitura.c4p.login;

import jelatyna.ConfituraSession;
import jelatyna.components.RedirectLink;
import jelatyna.components.feedback.ConfituraFeedbackPanel;
import jelatyna.domain.Speaker;
import jelatyna.pages.confitura.BaseWebPage;
import jelatyna.pages.confitura.FacebookLoginPanel;
import jelatyna.pages.confitura.c4p.RegisterSpeakerPage;
import jelatyna.pages.confitura.c4p.ViewSpeakerPage;
import jelatyna.services.SpeakerService;
import jelatyna.services.exceptions.LoginException;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.SubmitLink;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import static jelatyna.utils.Components.*;

@SuppressWarnings("serial")
public class LoginSpeakerPage extends BaseWebPage {

    @SpringBean
    private SpeakerService service;

    public LoginSpeakerPage() {
        setPageTitlePostfix(new ResourceModel("c4p.label"));
        add(i18nLabel("c4p.label"));
        add(new ConfituraFeedbackPanel("feedback"));
        LoginForm loginForm = new LoginForm("form");
        add(loginForm);
        add(new FacebookLoginPanel("facebookLoginPanel").setVisible(false));
        if (ConfituraSession.get().hasSpeaker()) {
            setResponsePage(ViewSpeakerPage.class);
        }
    }

    private final class LoginForm extends Form<Void> {
        private TextField<String> mail = withLabel("e-mail", textField("mail", new Model<String>(), true));
        private TextField<String> password = withLabelKey("password.label", passwordField("password", new Model<String>(), true));

        private LoginForm(String id) {
            super(id);
            add(i18nLabel("email.label"), mail);
            add(i18nLabel("password.label"), password);
            add(new SubmitLink("login").add(i18nLabel("login.label")));
            add(new RedirectLink("register", RegisterSpeakerPage.class).add(i18nLabel("register.label"))
                    .setVisible(service.isCall4PapersActive()));
            add(i18nLabel("password.forgot.label"));
        }

        @Override
        protected void onSubmit() {
            try {
                Speaker speaker = service.loginWith(mail.getValue(), password.getValue());
                ConfituraSession.get().setUser(speaker);
                setResponsePage(ViewSpeakerPage.class);
            } catch (LoginException ex) {
                error(new ResourceModel(ex.getMessage()).getObject());
            }
        }
    }
}

package jelatyna.pages.confitura.c4p.login;

import jelatyna.components.feedback.ConfituraFeedbackPanel;
import jelatyna.domain.Speaker;
import jelatyna.pages.PageNotFound;
import jelatyna.pages.confitura.BaseWebPage;
import jelatyna.services.SpeakerService;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.validation.EqualPasswordInputValidator;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import static jelatyna.utils.Components.*;
import static jelatyna.utils.Models.*;

@SuppressWarnings("serial")
public class ResetPasswordPage extends BaseWebPage {
    @SpringBean
    private SpeakerService service;

    public ResetPasswordPage(PageParameters params) {
        Speaker speaker = findSpeaker(params);
        add(i18nLabel("password.reset.label"));
        add(new ResetPasswordForm(speaker));
        setPageTitlePostfix(new ResourceModel("password.reset.label"));
        if (speaker == null) {
            setResponsePage(PageNotFound.class);
        }
    }

    private Speaker findSpeaker(PageParameters params) {
        return service.findByToken(params.get("token").toString());
    }

    private final class ResetPasswordForm extends Form<Void> {
        private PasswordTextField password = withLabelKey("password.new.label", passwordField("password", model(), true));
        private PasswordTextField repassword = withLabelKey("password.new.repeat.label", passwordField("repassword", model(), true));
        private Speaker speaker;

        private ResetPasswordForm(Speaker speaker) {
            super("form");
            this.speaker = speaker;
            add(new ConfituraFeedbackPanel("feedback"));
            add(i18nLabel("password.new.label"), password);
            add(i18nLabel("password.new.repeat.label"), repassword);
            add(i18nLabel("save.label"));
            add(new EqualPasswordInputValidator(password, repassword));
        }

        @Override
        protected void onSubmit() {
            service.resetPasswordFor(speaker, password.getValue());
            getSession().info(new ResourceModel("password.changed.message").getObject());
            setResponsePage(LoginSpeakerPage.class);
        }

    }
}

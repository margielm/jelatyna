package jelatyna.pages.confitura.c4p.login;

import jelatyna.domain.Speaker;
import jelatyna.services.AbsoluteUrlProvider;
import jelatyna.mail.MailSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static jelatyna.utils.PageParametersBuilder.*;

@Component
public class RequestPasswordResetMailSender {
    @Autowired
    private MailSender sender;
    @Autowired
    private AbsoluteUrlProvider absoluteUrlProvider;

    public void sendMessage(Speaker speaker) {
        sender.setTemplateName("speakerResetPass");
        sender.set("url", getUrlToResetPassword(speaker));
        sender.set("firstName", speaker.getFirstName());
        sender.set("lastName", speaker.getLastName());
        sender.sendMessage(speaker);
    }

    private String getUrlToResetPassword(Speaker speaker) {
        return absoluteUrlProvider.getPathFor(ResetPasswordPage.class, paramsFor("token", speaker.getToken()));
    }

}

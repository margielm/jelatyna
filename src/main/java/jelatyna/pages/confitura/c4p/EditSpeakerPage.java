package jelatyna.pages.confitura.c4p;

import static jelatyna.components.user.UserFormPanel.*;

import org.apache.wicket.spring.injection.annot.SpringBean;

import jelatyna.domain.Speaker;
import jelatyna.services.SpeakerService;

@SuppressWarnings("serial")
public class EditSpeakerPage extends SpeakerBasePage {
    @SpringBean
    private SpeakerService service;

    public EditSpeakerPage() {
        Speaker speaker = getSpeaker();
        add(newUserFormPanel(service, speaker, ViewSpeakerPage.class));
        setPageTitlePostfix("Edycja prelegenta " + speaker.getFullName());
    }

    private Speaker getSpeaker() {
        return getSession().getSpeaker();
    }
}

package jelatyna.pages.confitura.c4p.voting;

import jelatyna.ConfituraSession;
import org.apache.wicket.util.cookies.CookieUtils;
import org.springframework.stereotype.Component;

@Component
public class HttpProvider {
    public ConfituraSession getSession() {
        return ConfituraSession.get();
    }

    public String getCookieValue(String key) {
        return new CookieUtils().load(key);
    }

    public void setCookieValue(String key, String value) {
        new CookieUtils().save(key, value);
    }

}

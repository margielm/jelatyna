package jelatyna.pages.confitura.speaker;

import jelatyna.components.user.UserInfoPanel;
import jelatyna.domain.Speaker;
import jelatyna.pages.confitura.BaseWebPage;
import jelatyna.services.FileService;
import jelatyna.services.SpeakerService;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.Collections;
import java.util.List;

@SuppressWarnings("serial")
public class ListSpeakerPage extends BaseWebPage {
    @SpringBean
    private SpeakerService service;
    @SpringBean
    private FileService fileService;

    public ListSpeakerPage() {
        List<Speaker> speakers = service.allAcceptedSpeakers();
        Collections.shuffle(speakers);
        add(new SpeakersList("speakers", speakers));
        setPageTitlePostfix("Prelegenci");
    }

    private final class SpeakersList extends ListView<Speaker> {
        private SpeakersList(String id, List<Speaker> list) {
            super(id, list);
        }

        @Override
        protected void populateItem(ListItem<Speaker> item) {
            Speaker speaker = item.getModelObject();
            item.add(new UserInfoPanel<Speaker>("speaker", speaker));
            addImageForFacebook(fileService.getUrlTo(speaker));
        }
    }
}

package jelatyna.pages.confitura.registration.form;

import jelatyna.pages.confitura.BaseWebPage;
import jelatyna.pages.confitura.registration.Registration;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import static jelatyna.utils.Components.*;

@SuppressWarnings("serial")
public class RegistrationPage extends BaseWebPage {
    public static final String CLOSE_INFO_ID = "closeInfo";
    @SpringBean
    Registration registration;

    public RegistrationPage() {
        RegistrationForm form = new RegistrationForm("form");
        add(form);
        add(new FeedbackPanel("feedback"));
        setUpRegistration(form);
        setPageTitlePostfix("Rejestracja");
    }

    private void setUpRegistration(RegistrationForm form) {
        if (registration.isActive()) {
            add(richLabel(CLOSE_INFO_ID, "").setVisible(false));
        } else {
            form.setVisible(false);
            add(richLabel(CLOSE_INFO_ID, registration.getCloseInfo()));
        }
    }

}

package jelatyna.pages.confitura.registration.form;

import static com.google.common.collect.Lists.*;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class RegistrationOptions {
    public List<String> getExperiance() {
        return newArrayList(
                "Dopiero się uczę",
                "Zawodowo mniej niż rok",
                "Zawodowo 1-2 lata",
                "Zawodowo 2-4 lata",
                "Zawodowo 4-8 lata",
                "Zawodowo powyżej 8 lat");
    }

    public List<String> getPosition() {
        return newArrayList(
                "Studentem/uczniem",
                "Programistą",
                "Testerem",
                "Kierownikiem zespołu",
                "Kierownikiem projektu",
                "Inne");
    }

    public List<String> getShirtSize() {
        return newArrayList("S", "M", "L", "XL", "XXL");
    }

    public List<String> getInfo() {
        return newArrayList("Warszawa JUG",
                "Inny JUG",
                "Pracuj.pl",
                "Od koleżanki/kolegi",
                "Facebook/Twitter",
                "GoldenLine/LinkedIn",
                "inne źródło");
    }
}

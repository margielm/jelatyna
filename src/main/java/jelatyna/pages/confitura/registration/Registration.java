package jelatyna.pages.confitura.registration;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jelatyna.domain.Participant;
import jelatyna.domain.Presentation;
import jelatyna.domain.RegistrationConfiguration;
import jelatyna.domain.RegistrationStatus;
import jelatyna.pages.admin.registration.ParticipantMailSender;
import jelatyna.repositories.ParticipantRepository;
import jelatyna.repositories.PresentationRepository;
import jelatyna.repositories.RegistrationConfigurationRepository;
import jelatyna.services.RandomGenerator;

@Component
public class Registration {
    static final String PRESENTATION_LIMIT_ERROR = "Możesz wybrać maksymalnie 7 prezentacji!";

    static final String NOT_FOUND_ERROR = "Nie odnaleziono uczestnika dla podanego klucza!";

    static final String MAIL_EXISTS_ERROR = "Podany adres mailowy jest już zarejestrowany!";

    @Autowired
    private ParticipantMailSender mailSender;

    @Autowired
    private RandomGenerator randomGenerator;

    @Autowired
    private ParticipantRepository participantRepository;

    @Autowired
    private PresentationRepository presentationRepository;

    @Autowired
    private RegistrationConfigurationRepository configurationRepository;

    public boolean isActive() {
        return getConfiguration().isActive();
    }

    public List<Presentation> allPresentations() {
        return presentationRepository.findAllAccepted();
    }

    public String getCloseInfo() {
        return getConfiguration().getCloseInfo();
    }

    public void register(Participant participant) {
        if (participantRepository.findByToken(participant.getToken()) == null) {
            throw new RuntimeException("Incorrect token!");
        } else if (participant.countPresentations() > 7) {
            throw new RuntimeException(PRESENTATION_LIMIT_ERROR);
        } else if (mailIsNotUnique(participant)) {
            throw new RuntimeException(MAIL_EXISTS_ERROR);
        }
        else {
            doRegister(participant);
        }
    }

    private void doRegister(Participant participant) {
        participant.status(RegistrationStatus.FINAL_CONFIRMED);
        participantRepository.save(participant);
        mailSender.sendMessage(participant);
    }

    private boolean mailIsNotUnique(Participant participant) {
//        Participant found = participantRepository.findByMail(participant.getMail());
//        return found != null && !found.getId().equals(participant.getId());
        return false;
    }

    private RegistrationConfiguration getConfiguration() {
        return configurationRepository.findAll().get(0);
    }

    public List<String> fetchCitiesStartingWith(String name) {
        return participantRepository.fetchCitiesStartingWith(name + "%");
    }

    public void changeStatusFor(String token, RegistrationStatus status) {
        Participant participant = getParticipant(token);
        if (participant != null) {
            doChange(participant, status);
        } else {
            throw new RuntimeException(NOT_FOUND_ERROR);
        }
    }

    public Participant getParticipant(String token) {
        return participantRepository.findByToken(token);
    }

    private void doChange(Participant participant, RegistrationStatus status) {
        participant.status(status);
        participantRepository.save(participant);
    }

    public void mailSender(ParticipantMailSender mailSender) {
        this.mailSender = mailSender;
    }

}

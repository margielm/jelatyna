package jelatyna.pages.confitura.registration.process;

import static jelatyna.domain.RegistrationStatus.*;

import org.apache.wicket.request.mapper.parameter.PageParameters;

@SuppressWarnings("serial")
public class FinallyConfirmRegistrationPage extends ChangeRegistrationStatePage {

    public FinallyConfirmRegistrationPage(PageParameters params) {
        super(params, FINAL_CONFIRMED);
        setPageTitlePostfix("Ostateczne potwierdzenie rejestracji");
    }

    @Override
    String getMessage() {
        return "Dziękujemy za ostateczne potwierdzenie swojej rejestracji. ";
    }

}

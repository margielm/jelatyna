package jelatyna.pages.confitura.registration.process;

import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import jelatyna.domain.RegistrationStatus;
import jelatyna.pages.confitura.BaseWebPage;
import jelatyna.pages.confitura.registration.Registration;
import jelatyna.pages.confitura.registration.RegistrationInfoPage;

@SuppressWarnings("serial")
public abstract class ChangeRegistrationStatePage extends BaseWebPage {
    @SpringBean
    private Registration registration;

    public ChangeRegistrationStatePage(PageParameters params, RegistrationStatus status) {
        String token = params.get("token").toString();
        try {
            changeStateFor(token, status);
            redirectWith(getMessage());
        } catch (Exception ex) {
            redirectWith(ex.getMessage());
        }
    }

    private void changeStateFor(String token, RegistrationStatus status) {
        registration.changeStatusFor(token, status);
    }

    private void redirectWith(String info) {
        setResponsePage(new RegistrationInfoPage(info));
    }

    abstract String getMessage();
}

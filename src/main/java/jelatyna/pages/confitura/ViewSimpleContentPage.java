package jelatyna.pages.confitura;

import jelatyna.domain.SimpleContent;
import jelatyna.repositories.SimpleContentRepository;
import jelatyna.utils.HtmlUtils;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import static jelatyna.utils.Components.*;

@SuppressWarnings("serial")
public class ViewSimpleContentPage extends BaseWebPage {
    @SpringBean
    private SimpleContentRepository repository;

    public ViewSimpleContentPage(PageParameters params) {
        SimpleContent simpleContent = repository.findOne(params.get("id").toInteger());
        add(richLabel("content", simpleContent.getContent()));
        addSomeDataToHeader(simpleContent);
    }

    public ViewSimpleContentPage(SimpleContent simpleContent) {
        add(richLabel("content", simpleContent.getContent()));
        addSomeDataToHeader(simpleContent);
    }

    private void addSomeDataToHeader(SimpleContent simpleContent) {
        setPageTitlePostfix(simpleContent.getTitle());
        addImagesForFacebook(HtmlUtils.extractImageLinksFromHtml(simpleContent.getContent()));
    }
}

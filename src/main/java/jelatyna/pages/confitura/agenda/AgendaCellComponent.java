package jelatyna.pages.confitura.agenda;

import static jelatyna.utils.Components.*;

import org.apache.wicket.Component;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import jelatyna.components.RedirectLink;
import jelatyna.components.presentation.PresentationLinksPanel;
import jelatyna.domain.AgendaItem;
import jelatyna.domain.Presentation;
import jelatyna.pages.confitura.speaker.ListPresentationPage;
import jelatyna.services.FileService;
import jelatyna.services.PresentationService;

@SuppressWarnings("serial")
public class AgendaCellComponent extends Panel {
    @SpringBean
    private PresentationService service;
    @SpringBean
    private FileService fileService;

    public AgendaCellComponent(String id, AgendaItem agendaItem) {
        super(id);
        Label valueTag = label("value", agendaItem.getValue());
        WebMarkupContainer presentationTag = new WebMarkupContainer("item");
        if (agendaItem.hasPresentation()) {
            valueTag.setVisible(false);
            showPresentation(agendaItem, presentationTag);
        } else {
            presentationTag.setVisible(false);
        }
        add(presentationTag);
        add(valueTag);
    }

    private void showPresentation(AgendaItem agendaItem, WebMarkupContainer container) {
        Presentation presentation = getPresentation(agendaItem);
        container.add(createLinkTo(presentation));
        container.add(label("speaker", presentation.getOwner().getFullName()));
        container.add(new PresentationLinksPanel("links", presentation));

    }

    private Component createLinkTo(final Presentation presentation) {
        RedirectLink link = new RedirectLink("link", ListPresentationPage.class) {
            @Override
            protected CharSequence appendAnchor(ComponentTag tag, CharSequence url) {
                return super.appendAnchor(tag, url + "#" + presentation.getId());
            }
        };
        link.add(label("title", presentation.getTitle()));
        return link;
    }

    private Presentation getPresentation(AgendaItem agendaItem) {
        return service.findBy(agendaItem.presentationId);
    }
}

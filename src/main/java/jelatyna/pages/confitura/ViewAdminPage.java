package jelatyna.pages.confitura;

import jelatyna.components.user.UserInfoPanel;
import jelatyna.domain.Admin;
import jelatyna.services.AdminService;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.Collections;
import java.util.List;

@SuppressWarnings("serial")
public class ViewAdminPage extends BaseWebPage {

    @SpringBean
    private AdminService service;

    public ViewAdminPage() {
        add(new PeopleList("admins", getAdmins()));
        setPageTitlePostfix("Kapituła konferencji");
    }

    private List<Admin> getAdmins() {
        List<Admin> admins = service.findAll();
        Collections.shuffle(admins);
        return admins;
    }

    private final class PeopleList extends ListView<Admin> {
        private PeopleList(String id, List<? extends Admin> list) {
            super(id, list);
        }

        @Override
        protected void populateItem(ListItem<Admin> item) {
            item.setRenderBodyOnly(true);
            item.add(new UserInfoPanel<Admin>("userInfoPanel", item.getModelObject()));
        }

    }
}

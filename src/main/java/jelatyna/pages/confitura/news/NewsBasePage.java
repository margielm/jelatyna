package jelatyna.pages.confitura.news;

import jelatyna.domain.Admin;
import jelatyna.domain.News;
import jelatyna.pages.confitura.BaseWebPage;
import jelatyna.pages.confitura.ViewAdminPage;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.request.mapper.parameter.PageParameters;

@SuppressWarnings("serial")
public class NewsBasePage extends BaseWebPage {
    ExternalLink linktToAuthor(News news) {
        return new ExternalLink("author", getLinkTo(news.getAutor()), news.getAutor().getFullName());
    }

    private String getLinkTo(Admin author) {
        return "/" + urlFor(ViewAdminPage.class, new PageParameters()) + "#" + author.getFullName();
    }
}

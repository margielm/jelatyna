package jelatyna.pages.confitura.news;

import jelatyna.domain.News;
import jelatyna.pages.confitura.BaseWebPage;
import jelatyna.services.NewsService;
import jelatyna.utils.HtmlUtils;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.spring.injection.annot.SpringBean;

@SuppressWarnings("serial")
public class ListNewsPage extends BaseWebPage {

    @SpringBean
    private NewsService service;

    public ListNewsPage() {
        NewsList newsList = new NewsList("news", service);
        add(newsList);
        add(new NewsNavigator("navigator", newsList));
        setPageTitlePostfix("Aktualności");

    }

    private final class NewsList extends DataView<News> {
        private NewsList(String id, NewsService repository) {
            super(id, new NewsProvider(repository), 5);
        }

        @Override
        protected void populateItem(Item<News> item) {
            item.add(new NewsPanel("newsPanel", item.getModelObject(), false));
            addImagesForFacebook(HtmlUtils.extractImageLinksFromHtml(item.getModelObject().getShortDescription()));
        }
    }

}
package jelatyna.pages.confitura.news;

import org.apache.wicket.markup.html.navigation.paging.IPageable;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigator;

@SuppressWarnings("serial")
public class NewsNavigator extends PagingNavigator {

    public NewsNavigator(String id, IPageable pageable) {
        super(id, pageable);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        getPagingNavigation().setViewSize(5);
    }

    @Override
    protected void onBeforeRender() {
        super.onBeforeRender();
        get("first").setVisible(false);
        get("last").setVisible(false);
    }

}

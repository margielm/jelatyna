package jelatyna.pages.confitura.news;

import jelatyna.domain.News;
import jelatyna.pages.confitura.BaseWebPage;
import jelatyna.services.NewsService;
import jelatyna.utils.HtmlUtils;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import static jelatyna.utils.PageParametersBuilder.*;

@SuppressWarnings("serial")
public class NewsDetailsPage extends BaseWebPage {
    @SpringBean
    private NewsService service;

    public NewsDetailsPage(PageParameters params) {
        News news = loadNewsBy(params);
        add(new NewsPanel("news", news, true));
        addSomeDataToHeader(news);
    }

    public NewsDetailsPage(News news) {
        add(new NewsPanel("news", news, true));
        addSomeDataToHeader(news);
    }

    private News loadNewsBy(PageParameters params) {
        if (params.get(ID).isEmpty()) {
            return service.findByTitle(params.get(TITLE).toString());
        } else {
            return service.findPublishedById(params.get(ID).toInt());
        }
    }

    private void addSomeDataToHeader(News news) {
        setPageTitlePostfix(news.getTitle());
        addImagesForFacebook(news);
    }

    private void addImagesForFacebook(News news) {
        addImagesForFacebook(HtmlUtils.extractImageLinksFromHtml(news.getShortDescription()));
        addImagesForFacebook(HtmlUtils.extractImageLinksFromHtml(news.getDescription()));
    }

}

package jelatyna.pages;

import jelatyna.pages.confitura.BaseWebPage;

@SuppressWarnings("serial")
public class PageNotFound extends BaseWebPage {

    public PageNotFound() {
        setPageTitlePostfix("404 - strona nie odnaleziona");
    }

    @Override
    public boolean isErrorPage() {
        return true;
    }

    @Override
    public boolean isVersioned() {
        return false;
    }

}

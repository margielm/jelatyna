package jelatyna.pages.admin.agenda;

import jelatyna.domain.Agenda;
import jelatyna.domain.AgendaCell;
import jelatyna.services.PresentationService;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.AjaxEditableLabel;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.injection.Injector;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.io.Serializable;
import java.util.List;

import static jelatyna.utils.Models.*;

@SuppressWarnings("serial")
abstract class ColumnWithPossibleHeaderPopulator implements ICellPopulator<List<AgendaCell>>, Serializable {

    static final String CELL_VALUE = "cellValue";
    static final String MODAL_WINDOW_ID = "modalWindow";
    protected final int colNum;
    protected IModel<Agenda> agendaModel;

    protected ColumnWithPossibleHeaderPopulator(int colNum, IModel<Agenda> agendaModel) {
        this.colNum = colNum;
        this.agendaModel = agendaModel;
    }

    @Override
    public void populateItem(Item<ICellPopulator<List<AgendaCell>>> cellItem, String componentId,
                             final IModel<List<AgendaCell>> rowModel) {
        if (isRoomCell(rowModel)) {
            populateLabelItem(cellItem, componentId, rowModel);
        } else {
            populateCellItem(cellItem, componentId, rowModel);
        }
    }

    private void populateLabelItem(Item<ICellPopulator<List<AgendaCell>>> cellItem, String componentId,
                                   IModel<List<AgendaCell>> rowModel) {
        final AgendaCell cell = getCellAtColNum(rowModel);
        final AjaxEditableLabel<Serializable> label = new AjaxEditableLabel<Serializable>(CELL_VALUE, propertyModel(
                cell, "value")) {
            @Override
            protected void onSubmit(AjaxRequestTarget target) {
                cell.value = this.getEditor().getValue();
                super.onSubmit(target);
            }
        };
        Component dummyModalWindow = new ModalWindow(MODAL_WINDOW_ID).setVisible(false);
        addComponentsToCellItem(cellItem, componentId, label, dummyModalWindow);
    }

    protected int getRowNumber(IModel<List<AgendaCell>> rowModel) {
        return getCellAtColNum(rowModel).rowNum;
    }

    protected AgendaCell getCellAtColNum(IModel<List<AgendaCell>> rowModel) {
        return rowModel.getObject().get(colNum);
    }

    private boolean isRoomCell(IModel<List<AgendaCell>> rowModel) {
        return getRowNumber(rowModel) == 0;
    }

    protected void addComponentsToCellItem(Item<ICellPopulator<List<AgendaCell>>> cellItem, String componentId,
                                           Component label, Component dummyModalWindow) {
        final WebMarkupContainer markupContainer = new WebMarkupContainer(componentId);
        markupContainer.setOutputMarkupId(true);
        markupContainer.add(label);
        markupContainer.add(dummyModalWindow);
        cellItem.add(markupContainer);
    }

    protected Label createClickablePresentationLabel(IModel<List<AgendaCell>> rowModel, final ModalWindow modalWindow) {
        Label label = new Label(CELL_VALUE, new AdminAgendaCellModel(getCellAtColNum(rowModel)));
        label.add(new AjaxEventBehavior("onclick") {
            @Override
            protected void onEvent(AjaxRequestTarget target) {
                modalWindow.show(target);
            }
        });
        return label;
    }

    protected abstract void populateCellItem(Item<ICellPopulator<List<AgendaCell>>> cellItem, String componentId,
                                             IModel<List<AgendaCell>> rowModel);

    @Override
    public void detach() {
    }

    private final class AdminAgendaCellModel extends Model<String> {
        private final AgendaCell cell;
        @SpringBean
        private PresentationService service;

        private AdminAgendaCellModel(AgendaCell cell) {
            this.cell = cell;
            Injector.get().inject(this);
        }

        @Override
        public String getObject() {
            if (cell.hasPresentation()) {
                return service.findBy(cell.presentationId).getTitle();
            } else {
                return cell.getValue();
            }
        }
    }
}

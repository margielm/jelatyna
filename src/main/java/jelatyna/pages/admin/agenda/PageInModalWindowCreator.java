package jelatyna.pages.admin.agenda;

import jelatyna.domain.Agenda;
import org.apache.wicket.Page;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.model.IModel;

import java.io.Serializable;

interface PageInModalWindowCreator extends Serializable {
    Page createPageInModalWindow(IModel<Agenda> agendaModel, int colNum, int rowNum, ModalWindow modalWindow);
}

package jelatyna.pages.admin.agenda;

import jelatyna.domain.Agenda;
import jelatyna.domain.AgendaCell;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.AjaxEditableLabel;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

import java.util.List;

@SuppressWarnings({"rawtypes", "unchecked", "serial"})
class TimespanPopulator extends ColumnWithPossibleHeaderPopulator {
    private final FeedbackPanel feedbackPanel;

    protected TimespanPopulator(int colNum, IModel<Agenda> agendaModel, FeedbackPanel feedbackPanel) {
        super(colNum, agendaModel);
        this.feedbackPanel = feedbackPanel;
    }

    @Override
    protected void populateCellItem(Item<ICellPopulator<List<AgendaCell>>> cellItem, String componentId,
                                    IModel<List<AgendaCell>> rowModel) {
        final AgendaCell cell = getCellAtColNum(rowModel);
        final AjaxEditableLabel label = new CellValueAjaxEditableLabel(cell).add(new TimespanValidator());
        Component dummyModalWindow = new ModalWindow(MODAL_WINDOW_ID).setVisible(false);
        addComponentsToCellItem(cellItem, componentId, label, dummyModalWindow);
    }

    private class CellValueAjaxEditableLabel extends AjaxEditableLabel {
        private final AgendaCell cell;

        public CellValueAjaxEditableLabel(AgendaCell cell) {
            super(ColumnWithPossibleHeaderPopulator.CELL_VALUE, new PropertyModel(cell, "value"));
            this.cell = cell;
        }

        @Override
        protected void onSubmit(AjaxRequestTarget target) {
            cell.value = this.getEditor().getValue();
            target.add(feedbackPanel);
            super.onSubmit(target);
        }

        @Override
        protected void onError(AjaxRequestTarget target) {
            target.add(feedbackPanel);
            super.onError(target);
        }
    }
}

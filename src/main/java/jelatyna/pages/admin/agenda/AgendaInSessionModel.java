package jelatyna.pages.admin.agenda;

import jelatyna.ConfituraSession;
import jelatyna.domain.Agenda;
import org.apache.wicket.model.LoadableDetachableModel;

@SuppressWarnings("serial")
class AgendaInSessionModel extends LoadableDetachableModel<Agenda> {
    @Override
    protected Agenda load() {
        return ConfituraSession.get().getAgenda();
    }
}

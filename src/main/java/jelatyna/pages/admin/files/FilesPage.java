package jelatyna.pages.admin.files;

import jelatyna.components.ValidateBehavior;
import jelatyna.pages.admin.AdminBasePage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.services.FileService;
import org.apache.wicket.extensions.breadcrumb.BreadCrumbBar;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;

import static jelatyna.utils.Components.*;

@SuppressWarnings("serial")
public class FilesPage extends AdminBasePage {

    @SpringBean
    private FileService service;

    public FilesPage() {
        BreadCrumbBar bar = new BreadCrumbBar("folders");
        add(bar);
        FilesPanel filesPanel = new FilesPanel("files_panel", bar, "ROOT");
        add(new UploadFilesPanel("uploadFiles", filesPanel));
        add(new AddFolderForm("form", filesPanel));
        add(filesPanel);
        bar.setActive(filesPanel);
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.CONFERENCE_SITE;
    }

    @Override
    public String getPageTitle() {
        return "Dodaj pliki";
    }

    private final class AddFolderForm extends Form<String> {
        private Model<String> folderName = new Model<String>();
        private FilesPanel filesPanel;

        private AddFolderForm(String id, FilesPanel filesPanel) {
            super(id);
            this.filesPanel = filesPanel;
            add(required(textField("folder", folderName)).add(new ValidateBehavior()));
        }

        @Override
        protected void onSubmit() {
            service.newFolder(filesPanel.getFolderPath(), folderName.getObject());
        }
    }

}

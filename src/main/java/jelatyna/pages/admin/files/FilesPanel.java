package jelatyna.pages.admin.files;

import jelatyna.components.DeleteLink;
import jelatyna.components.StaticImage;
import jelatyna.services.FileService;
import jelatyna.utils.Models;
import org.apache.wicket.Component;
import org.apache.wicket.extensions.breadcrumb.IBreadCrumbModel;
import org.apache.wicket.extensions.breadcrumb.IBreadCrumbParticipant;
import org.apache.wicket.extensions.breadcrumb.panel.BreadCrumbPanel;
import org.apache.wicket.extensions.breadcrumb.panel.BreadCrumbPanelLink;
import org.apache.wicket.extensions.breadcrumb.panel.IBreadCrumbPanelFactory;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

import static jelatyna.utils.Components.*;

@SuppressWarnings("serial")
public class FilesPanel extends BreadCrumbPanel {

    @SpringBean
    private FileService service;
    private String folder = "";

    public FilesPanel(String componentId, IBreadCrumbModel breadCrumbModel, String folder) {
        super(componentId, breadCrumbModel);
        this.folder = folder;
        add(new FilesList(new ItemsInFolderModel()));
    }

    public FilesPanel(String id, final IBreadCrumbModel breadCrumbModel) {
        this(id, breadCrumbModel, "");

    }

    public List<String> getFolderPath() {
        List<IBreadCrumbParticipant> allParticipants = getBreadCrumbModel().allBreadCrumbParticipants();
        return allParticipants
                .stream()
                .skip(1)
                .map(p -> p.getTitle().getObject())
                .collect(Collectors.toList());
    }


    @Override
    public IModel<String> getTitle() {
        return Models.model(folder);
    }

    private final class FilesList extends ListView<File> {

        private FilesList(IModel<List<File>> model) {
            super("files", model);
        }


        @Override
        protected void populateItem(ListItem<File> item) {
            File file = item.getModelObject();
            item.add(folderIcon(file));
            item.add(fileIcon(file));
            item.add(new DeleteFileLink(file));
            item.add(label("name", file.getName()));
        }

        private Component fileIcon(final File file) {
            return new StaticImage("file", service.getUrlTo(getFolderPath(), file.getName())).setVisible(file.isFile());
        }

        private BreadCrumbPanelLink folderIcon(File file) {
            BreadCrumbPanelLink link = new BreadCrumbPanelLink("folder", getBreadCrumbModel(), new PanelFactory(file.getName()));
            link.setVisible(file.isDirectory());
            return link;
        }
    }

    private final class DeleteFileLink extends DeleteLink {
        private final File file;

        private DeleteFileLink(File file) {
            super("delete");
            this.file = file;
        }

        @Override
        public void onClick() {
            service.delete(getFolderPath(), file.getName());
        }
    }

    private final class ItemsInFolderModel extends AbstractReadOnlyModel<List<File>> {
        @Override
        public List<File> getObject() {
            return service.listFilesIn(getFolderPath());
        }
    }

    private final class PanelFactory implements IBreadCrumbPanelFactory {
        private final String fileName;

        private PanelFactory(String fileName) {
            this.fileName = fileName;
        }

        @Override
        public BreadCrumbPanel create(String componentId, IBreadCrumbModel breadCrumbModel) {
            return new FilesPanel(componentId, breadCrumbModel, this.fileName);
        }
    }
}
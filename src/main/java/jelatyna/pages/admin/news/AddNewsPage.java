package jelatyna.pages.admin.news;

import com.google.common.annotations.VisibleForTesting;
import jelatyna.ConfituraSession;
import jelatyna.components.RedirectLink;
import jelatyna.components.datetimepicker.DateTimePicker;
import jelatyna.components.feedback.ConfituraFeedbackPanel;
import jelatyna.domain.News;
import jelatyna.pages.admin.AbstractAddPage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.services.NewsService;
import org.apache.wicket.datetime.PatternDateConverter;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import static jelatyna.utils.Components.*;
import static jelatyna.utils.Models.*;
import static jelatyna.utils.PageParametersBuilder.*;

@SuppressWarnings("serial")
public class AddNewsPage extends AbstractAddPage<News> {
    @SpringBean
    private NewsService service;

    public AddNewsPage(PageParameters params) {
        super(params);
    }

    @Override
    protected void initPage(News news) {
        add(new AddNewsForm(news));
    }

    @Override
    protected News createNew() {
        return new News();
    }

    @Override
    protected News findById(int id) {
        return service.findById(id);
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.CONFERENCE_SITE;
    }

    @Override
    public String getPageTitle() {
        return "Dodaj/edytuj news'a";
    }

    private DateTimePicker dateTimePicker(News news) {
        DateTimePicker dateField = new DateTimePicker("creationDate", propertyModel(news, "creationDate"), new PatternDateConverter("dd-MM-yyyy HH:mm", false));
        dateField.setRequired(true);
        return dateField;
    }

    @VisibleForTesting
    class AddNewsForm extends Form<News> {
        @VisibleForTesting
        final News news;

        private AddNewsForm(News news) {
            super("form");
            this.news = news;
            add(new RedirectLink("newsListLink", ListNewsPage.class));
            add(new ConfituraFeedbackPanel("feedback"));
            add(new CheckBox("published", propertyModel(news, "published")));
            add(dateTimePicker(news));
            add(textField("title", propertyModel(news, "title"), true));
            add(richEditor("shortDescription", propertyModel(news, "shortDescription")));
            add(richEditor("description", propertyModel(news, "description")));
            add(cancelLink(ListNewsPage.class));
            add(new NewsPreviewLink("previewLink", news));
        }

        @Override
        protected void onSubmit() {
            if (news.isNew()) {
                news.autor(ConfituraSession.get().getAdmin());
            }
            service.save(news);
            getSession().info("News został zapisany");
            setResponsePage(AddNewsPage.class, paramsFor("id", news.getId()));
        }
    }

}

package jelatyna.pages.admin.news;

import jelatyna.components.LabeledLink;
import jelatyna.domain.News;
import jelatyna.pages.confitura.news.NewsDetailsPage;

@SuppressWarnings("serial")
public class NewsPreviewLink extends LabeledLink {

    static final String LABEL_FOR_DISABLED = "Zapisz, aby zobaczyć podgląd";
    static final String LABEL_FOR_ENABLED = "Zobacz podgląd";

    private final News news;

    public NewsPreviewLink(String id, News news) {
        super(id, news.isNotNew() ? LABEL_FOR_ENABLED : LABEL_FOR_DISABLED);
        this.news = news;
    }

    @Override
    public void onClick() {
        NewsDetailsPage newsPage = new NewsDetailsPage(news);
        setResponsePage(newsPage);
    }

    @Override
    public boolean isEnabled() {
        return news.isNotNew();
    }

}
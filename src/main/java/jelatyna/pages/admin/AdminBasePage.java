package jelatyna.pages.admin;

import static jelatyna.pages.admin.headerpanel.AdminMenuItemEnum.*;

import org.apache.wicket.Application;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.html.TransparentWebMarkupContainer;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import jelatyna.Confitura;
import jelatyna.ConfituraBasePage;
import jelatyna.ConfituraSession;
import jelatyna.domain.AbstractEntity;
import jelatyna.pages.admin.agenda.DefineAgendaPage;
import jelatyna.pages.admin.c4p.DefineRegistrationMailPage;
import jelatyna.pages.admin.c4p.DefineResetPassMailPage;
import jelatyna.pages.admin.c4p.presentation.ListPresentationPage;
import jelatyna.pages.admin.c4p.speaker.ListSpeakerPage;
import jelatyna.pages.admin.files.FilesPage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.pages.admin.headerpanel.AdminTopHeaderPanel;
import jelatyna.pages.admin.login.LoginPage;
import jelatyna.pages.admin.lottery.LotteryPage;
import jelatyna.pages.admin.menu.DefineMainMenuPage;
import jelatyna.pages.admin.news.ListNewsPage;
import jelatyna.pages.admin.registration.DefineParticipantMailPage;
import jelatyna.pages.admin.registration.GroupMailPage;
import jelatyna.pages.admin.registration.ListParticipantsPage;
import jelatyna.pages.admin.registration.RegisterParticipantsPage;
import jelatyna.pages.admin.registration.RegistrationSettingsPage;
import jelatyna.pages.admin.registration.StatisticsPage;
import jelatyna.pages.admin.scan.ScanPage;
import jelatyna.pages.admin.simple.ListSimpleContentPage;
import jelatyna.pages.admin.sponsor.ListSponsorPage;
import jelatyna.pages.admin.sponsor.SponsorTypesPage;
import jelatyna.pages.admin.user.ListAdminPage;
import jelatyna.pages.volunteer.user.ListVolunteerPage;
import jelatyna.utils.PageParametersBuilder;

@SuppressWarnings("serial")
public abstract class AdminBasePage extends ConfituraBasePage {
    private WebMarkupContainer wrapper;

    public AdminBasePage() {
        init();
        setPageTitlePostfix("Panel administracyjny");
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        super.renderHead(response);
//        response.render(new PriorityHeaderItem(JavaScriptHeaderItem.forReference(Application.get().getJavaScriptLibrarySettings()
//                .getJQueryReference())));
    }

    protected void init() {
        add(new AdminTopHeaderPanel.Builder("headerPanel", AdminHomePage.class, "Jelatyna", getActiveMenu())
                .withMenuItemAsDropdown(CONFERENCE_SITE, DefineMainMenuPage.class, "Menu")
                .withMenuItemAsDropdown(CONFERENCE_SITE, ListSimpleContentPage.class, "Strony statyczne")
                .withMenuItemAsDropdown(CONFERENCE_SITE, FilesPage.class, "Dodaj pliki")
                .withMenuItemAsDropdown(CONFERENCE_SITE, ListNewsPage.class, "Aktualności")
                .withMenuItem(ORGANIZERS, ListAdminPage.class)
                .withMenuItemAsDropdown(ORGANIZERS, ListVolunteerPage.class, "Wolontariusze")
                .withMenuItemAsDropdown(CALL_4_PAPERS, ListSpeakerPage.class, "Prelegenci")
                .withMenuItemAsDropdown(CALL_4_PAPERS, ListPresentationPage.class, "Prezentacje")
                .withMenuItemAsDropdown(CALL_4_PAPERS, DefineRegistrationMailPage.class,
                        "E-mail - Powierdzenie rejestracji")
                .withMenuItemAsDropdown(CALL_4_PAPERS, DefineResetPassMailPage.class, "E-mail - Reset hasła")
                .withMenuItemAsDropdown(PARTICIPANTS, ScanPage.class, "Scan")
                .withMenuItemAsDropdown(PARTICIPANTS, RegistrationSettingsPage.class, "Ustawienia rejestracji")
                .withMenuItemAsDropdown(PARTICIPANTS, DefineParticipantMailPage.class, "E-mail rejestracyjny")
                .withMenuItemAsDropdown(PARTICIPANTS, ListParticipantsPage.class, "Uczestnicy")
                .withMenuItemAsDropdown(PARTICIPANTS, RegisterParticipantsPage.class, "Obecność")
                .withMenuItemAsDropdown(PARTICIPANTS, GroupMailPage.class, "E-mail grupowy")
                .withMenuItemAsDropdown(PARTICIPANTS, StatisticsPage.class, "Statystyki")
                .withMenuItemAsDropdown(PARTICIPANTS, LotteryPage.class, "Losowanie nagród")
                .withMenuItemAsDropdown(SPONSORS, ListSponsorPage.class, "Sponsorzy")
                .withMenuItemAsDropdown(SPONSORS, SponsorTypesPage.class, "Typy Sponsorów")
                .withMenuItemAsDropdown(AGENDA, DefineAgendaPage.class, "Agenda")
                .build());

        wrapper = new TransparentWebMarkupContainer("wrapper");
        wrapper.setOutputMarkupId(true);
        wrapper.add(new AdminPageTitlePanel("pageTitle", getPageTitle()));

        add(wrapper);

        if (isInactive()) {
            redirectToInterceptPage(new LoginPage());
        }
    }

    protected boolean isInactive() {
        return ConfituraSession.get().hasAdmin() == false;
    }

    protected Confitura getApp() {
        return (Confitura) Application.get();
    }

    protected PageParameters paramsWithId(AbstractEntity<?> entity) {
        return PageParametersBuilder.paramsFor("id", entity.getId());
    }

    public abstract AdminMenuItemEnum getActiveMenu();

    public abstract String getPageTitle();
}

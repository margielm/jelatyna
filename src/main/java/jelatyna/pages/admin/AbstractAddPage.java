package jelatyna.pages.admin;

import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.util.string.StringValue;

@SuppressWarnings("serial")
public abstract class AbstractAddPage<T> extends AdminBasePage {

    public AbstractAddPage(PageParameters params) {
        handleAdditionalParameters(params);
        initPage(get(params.get("id")));
    }

    protected void handleAdditionalParameters(PageParameters params) {

    }

    protected T get(StringValue id) {
        return id.isEmpty() ? createNew() : findById(id.toInt());
    }

    protected abstract T findById(int id);

    protected abstract T createNew();

    protected abstract void initPage(T entity);

}
package jelatyna.pages.admin.registration;

import static jelatyna.utils.Components.*;
import static jelatyna.utils.Models.*;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.wicket.Page;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.request.Url;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.util.string.StringValue;

import jelatyna.components.RedirectLink;
import jelatyna.domain.Participant;
import jelatyna.services.ParticipantService;

public class ScanPanel extends Panel {

    public ScanPanel(String id, StringValue token, ParticipantService service, Class<? extends Page> page, Class<? extends Page>
            searchPage) {
        super(id);
        Participant participant = getParticipant(token, service);
        add(new ExternalLink("scan", "zxing://scan/?ret=" + getUrl(page) + "{CODE}&SCAN_FORMATS=QR_CODE"));
        add(new RedirectLink("search", searchPage));
        add(textField("firstName", propertyModel(participant, "firstName")));
        add(textField("lastName", propertyModel(participant, "lastName")));
        add(textField("email", propertyModel(participant, "mail")));
        add(textField("size", propertyModel(participant, "size")));
        if (participant.getId() != null) {
            participant.participated(true);
            service.save(participant);
        }

    }

    private Participant getParticipant(StringValue token, ParticipantService service) {
        Participant participant = null;
        if (!token.isEmpty()) {
            participant = service.findByToken(token.toOptionalString());
        }
        if (participant == null) {
            participant = new Participant();
        }
        return participant;
    }

    private String getUrl(Class<? extends Page> page) {
        try {
            String fullUrl = RequestCycle.get().getUrlRenderer().renderFullUrl(
                    Url.parse(urlFor(page, null).toString())) + "/";
            return URLEncoder.encode(fullUrl, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return "";
        }
    }
}

package jelatyna.pages.admin.registration;

import static jelatyna.utils.Components.*;
import static jelatyna.utils.Models.*;

import java.util.List;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.ajax.markup.html.form.AjaxCheckBox;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import jelatyna.components.nogeneric.AjaxLink;
import jelatyna.domain.Participant;
import jelatyna.services.ParticipantService;

@SuppressWarnings("serial")
public class RegisterParticipantsPanel extends Panel {

    @SpringBean
    private ParticipantService service;

    private TextField<String> searchField = textField("userData", model());

    private WebMarkupContainer container = new WebMarkupContainer("container");

    public RegisterParticipantsPanel(String id) {
        super(id);
        add(configureSearchField());
        container.add(new ParticipantsListView("rows", createParticipantsModel()));
        add(container.setOutputMarkupId(true));
    }

    private LoadableDetachableModel<List<Participant>> createParticipantsModel() {
        return new LoadableDetachableModel<List<Participant>>() {
            @Override
            protected List<Participant> load() {
                return service.findByLastName(searchField.getValue());
            }
        };
    }

    private Component configureSearchField() {
        Behavior searchParticipants = new AjaxFormComponentUpdatingBehavior("onInput") {
            @Override
            protected void onUpdate(AjaxRequestTarget target) {
                target.add(container);
            }
        };
        return searchField.add(searchParticipants);
    }

    private AjaxLink createAcceptButton(final Participant participant) {
        AjaxLink participate = new AjaxLink("accept") {

            @Override
            public void onClick(AjaxRequestTarget target) {
                service.toggleParticipation(participant);
                target.add(container);
                target.add(searchField);
            }
        };
        if (participant.isParticipated()) {
            participate.add(new AttributeAppender("class", "btn btn-danger"));
        }
        participate.add(label("label", getLabelFor(participant)));
        return participate;
    }

    private AjaxCheckBox createStampsCheckbox(final Participant participant) {
        AjaxCheckBox stamps = new AjaxCheckBox("stamps") {
            @Override
            protected void onUpdate(AjaxRequestTarget target) {
                service.save(participant);
                target.add(container);
                target.add(searchField);
            }
        };
        return stamps;
    }

    private String getLabelFor(Participant participant) {
        return participant.isParticipated() ? "Pomyłka:(" : "Obecny!";
    }

    private final class ParticipantsListView extends ListView<Participant> {
        private ParticipantsListView(String id, IModel<List<Participant>> model) {
            super(id, model);
        }

        @Override
        protected void populateItem(final ListItem<Participant> item) {
            Participant participant = item.getModelObject();
            CompoundPropertyModel<Participant> model = new CompoundPropertyModel<Participant>(participant);
            item.setModel(model);
            if (participant.shouldGetMaterials()) {
                item.add(new AttributeModifier("style", "background-color: #98FB98"));
            }
            item.add(label("firstName"));
            item.add(label("lastName"));
            item.add(label("mail"));
            item.add(label("status"));
            item.add(createAcceptButton(item.getModelObject()));
        }
    }

}

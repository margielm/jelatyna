package jelatyna.pages.admin.registration;

import jelatyna.pages.admin.AdminBasePage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;

@SuppressWarnings("serial")
public class RegisterParticipantsPage extends AdminBasePage {
    public RegisterParticipantsPage() {
        add(new RegisterParticipantsPanel("RegisterPanel"));

    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.PARTICIPANTS;
    }

    @Override
    public String getPageTitle() {
        return "Rejestracja Uczestników";
    }
}

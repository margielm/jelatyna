package jelatyna.pages.admin.registration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jelatyna.domain.Participant;
import jelatyna.services.AbsoluteUrlProvider;
import jelatyna.mail.MailSender;

@Component
public class ParticipantMailSender {
    @Autowired
    private MailSender sender;
    @Autowired
    private AbsoluteUrlProvider urlProvider;

    public ParticipantMailSender() {
    }

    public void sendMessage(Participant participant) {
        sender.setTemplateName("participant");
        sender.set("firstName", participant.getFirstName());
        sender.set("lastName", participant.getLastName());
        sender.set("link", "http://confitura.pl/#/registration/"+participant.getToken());
        sender.set("token", participant.getToken());
        sender.sendMessage(participant);
    }


}

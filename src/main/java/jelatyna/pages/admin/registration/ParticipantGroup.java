package jelatyna.pages.admin.registration;

import edu.emory.mathcs.backport.java.util.Collections;
import jelatyna.domain.Participant;

import java.util.List;

/**
 * TODO: possible optymilisation: make generic Group<A,List<B>> (increase reusability)
 */
public class ParticipantGroup {

    public Object key;

    public List<Participant> data;

    public ParticipantGroup(Object key, List<Participant> data) {
        this.key = key;
        this.data = data;
    }

    public Object getKey() {
        return key;
    }

    public List<Participant> getData() {
        return Collections.unmodifiableList(data);
    }

    public int getSize() {
        return data.size();
    }
}

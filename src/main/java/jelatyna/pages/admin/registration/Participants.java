package jelatyna.pages.admin.registration;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import jelatyna.domain.Participant;


public class Participants {

    private final List<Participant> participants;

    public Participants(List<Participant> participants) {
        this.participants = participants;
    }

    public Participants(Stream<Participant> stream) {
        this.participants = stream.collect(Collectors.toList());
    }

    public Participants limitToWithMailing(boolean limit) {
        if (limit) {
            return new Participants(
                    participants
                            .stream()
                            .filter(Participant::isMailing)
            );
        }
        return this;
    }

    public Participants limitToParticipated(boolean limit) {
        if (limit) {
            return new Participants(
                    participants
                            .stream()
                            .filter(Participant::isParticipated)
            );
        }
        return this;
    }
    public Participants limitToWithStamps(boolean limit) {
        if (limit) {
            return new Participants(
                    participants
                            .stream()
                            .filter(Participant::isStamps)
            );
        }
        return this;
    }

    public List<Participant> asList() {
        return participants;
    }

}

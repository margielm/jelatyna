package jelatyna.pages.admin.registration;

import static com.google.common.collect.Lists.*;
import static jelatyna.utils.Components.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;

import org.apache.wicket.MarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.joda.time.Interval;

import com.google.gson.Gson;

import jelatyna.domain.Participant;
import jelatyna.domain.RegistrationStatus;
import jelatyna.pages.admin.AdminBasePage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.services.ParticipantsProvider;
import jelatyna.services.StatisticsService;

@SuppressWarnings("serial")
public class StatisticsPage extends AdminBasePage {

    private static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("h.mm");
    @SpringBean
    private StatisticsService statisticsService;
    private ParticipantsProvider participantsProvider;

    public StatisticsPage() {
        this.participantsProvider = statisticsService.getParticipantsProvider();
        fillStatisticsRow(this, null);
        add(new StatusStatistics());

        ParticipantsModel byCityParticipantsModel = new ParticipantsModel(participantsProvider.getCityGroupCondition());
        ParticipantsModel byFirstNameParticipantsModel = new ParticipantsModel(participantsProvider.getFirstNameGroupCondition());
        ParticipantsModel byRegistrationDateParticipantsModel = new ParticipantsModel(participantsProvider.getRegistrationDateGroupCondition());

        add(new SingleCriteriaStatistic(byCityParticipantsModel, "byCitiesStats", "city", "cityFreq"));
        add(new SingleCriteriaStatistic(byFirstNameParticipantsModel, "byFirstNamesStats", "firstName", "firstNameFreq"));
        add(new SingleCriteriaStatistic(byRegistrationDateParticipantsModel, "byDatesStats", "registrationDate", "dateFreq"));

        String[] presentParticipantsDate = getJsonsPairForSplitParticipants(participantsProvider.getParticipantsInTimeIntervals());
        add(label("xAxis", presentParticipantsDate[0]).setEscapeModelStrings(false));
        add(label("yAxis", presentParticipantsDate[1]).setEscapeModelStrings(false));
    }

    private void fillStatisticsRow(MarkupContainer component, RegistrationStatus status) {
        int allForStatus = participantsProvider.getAllParticipantsForStatus(status).size();
        component.add(createLabelsFor("sum", allForStatus, participantsProvider.getAllParticipants().size()));
        component.add(createLabelsFor("women", participantsProvider.getParticipantsForGivenSex(Participant.WOMAN, status).size(), allForStatus));
        component.add(createLabelsFor("men", participantsProvider.getParticipantsForGivenSex(Participant.MAN, status).size(), allForStatus));
    }

    private Label[] createLabelsFor(String id, int count, int all) {
        return new Label[]{label(id + "Val", count + ""), label(id + "Per", getPercent(count, all) + "%")};
    }

    private int getPercent(double count, double all) {
        return (int) (all == 0 ? 0 : (count / all) * 100);
    }

    private String[] getJsonsPairForSplitParticipants(Map<Interval, Integer> participantsInTimeIntervals) {
        List<String> timeIntervals = new ArrayList<String>();
        List<Integer> presentParticipants = new ArrayList<Integer>();
        for (Entry<Interval, Integer> entry : participantsInTimeIntervals.entrySet()) {
            if (timeIntervals.isEmpty()) {
                timeIntervals.add(TIME_FORMAT.format(entry.getKey().getStart().toDate()));
                presentParticipants.add(0);
            }
            timeIntervals.add(TIME_FORMAT.format(entry.getKey().getEnd().toDate()));
            presentParticipants.add(entry.getValue());
        }

        String timeIntervalsJson = new Gson().toJson(timeIntervals);
        String presentParticipantsJson = new Gson().toJson(presentParticipants);
        return new String[]{timeIntervalsJson, presentParticipantsJson};
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.PARTICIPANTS;
    }

    @Override
    public String getPageTitle() {
        return "Statystyki";
    }

    private final class ParticipantsModel extends LoadableDetachableModel<List<ParticipantGroup>> {
        private transient Function<? super Participant, ?> groupBy;

        public ParticipantsModel(Function<? super Participant, ?> groupBy) {
            this.groupBy = groupBy;
        }

        @Override
        protected List<ParticipantGroup> load() {
            return participantsProvider.groupParticipantsAndSort(groupBy);
        }
    }

    private final class SingleCriteriaStatistic extends ListView<ParticipantGroup> {
        private final String groupNamelId;
        private final String groupValueId;

        public SingleCriteriaStatistic(IModel<List<ParticipantGroup>> groupedParticipants,
                                       String statisticName, String groupNameId, String groupValueId) {
            super(statisticName, groupedParticipants);
            this.groupNamelId = groupNameId;
            this.groupValueId = groupValueId;
        }

        @Override
        protected void populateItem(ListItem<ParticipantGroup> item) {
            ParticipantGroup group = item.getModelObject();
            item.add(label(groupNamelId, group.getKey()));
            item.add(createLabelsFor(groupValueId, group.getSize(), participantsProvider.getAllParticipants().size()));
        }
    }

    private final class StatusStatistics extends ListView<RegistrationStatus> {

        private StatusStatistics() {
            super("statusInfo", newArrayList(RegistrationStatus.values()));
        }

        @Override
        protected void populateItem(ListItem<RegistrationStatus> item) {
            RegistrationStatus type = item.getModelObject();
            item.add(label("status", type.getName()));
            fillStatisticsRow(item, type);
        }
    }
}

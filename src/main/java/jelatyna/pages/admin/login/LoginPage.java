package jelatyna.pages.admin.login;

import static jelatyna.utils.Components.*;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import jelatyna.ConfituraBasePage;
import jelatyna.components.feedback.ConfituraFeedbackPanel;
import jelatyna.domain.Admin;
import jelatyna.pages.admin.AdminHomePage;
import jelatyna.services.AdminService;
import jelatyna.services.exceptions.LoginException;

@SuppressWarnings("serial")
public class LoginPage extends ConfituraBasePage {
    public static final String FORM_ID = "form";
    public static final String EMAIL_FIELD_ID = "email";
    public static final String PASSWORD_FIELD_ID = "password";

    @SpringBean
    protected AdminService service;

    protected String email;
    protected String password;

    public LoginPage() {
        Form<LoginPage> form = new Form<LoginPage>(FORM_ID, new CompoundPropertyModel<LoginPage>(this)) {
            @Override
            protected void onSubmit() {
                login();
            }
        };
        form.add(withLabel("e-mail", textField(EMAIL_FIELD_ID)).setRequired(true));
        form.add(withLabel("Hasło", new PasswordTextField(PASSWORD_FIELD_ID)));
        form.add(new ConfituraFeedbackPanel("feedback"));
        add(form);

        setPageTitlePostfix("Logowanie");
    }

    protected void login() {
        try {
            tryToLogin();
        } catch (LoginException ex) {
            reportLoginError(ex);
        }
    }

    protected void tryToLogin() {
        Admin admin = service.loginWith(email, password);
        getSession().setUser(admin);
        continueToOriginalDestination();
        setResponsePage(AdminHomePage.class);
    }

    protected void reportLoginError(LoginException ex) {
        getSession().error(ex.getMessage());
        setResponsePage(LoginPage.class);
    }
}

package jelatyna.pages.admin.c4p.speaker;

import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import jelatyna.components.user.speaker.SpeakerPanel;
import jelatyna.domain.Speaker;
import jelatyna.pages.admin.AdminBasePage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.services.FileService;
import jelatyna.services.SpeakerService;

@SuppressWarnings("serial")
public class ViewSpeakerPage extends AdminBasePage {
    @SpringBean
    private SpeakerService service;

    @SpringBean
    private FileService fileService;

    public ViewSpeakerPage(PageParameters params) {
        Speaker speaker = service.findById(params.get("id").toInteger());
        add(new SpeakerPanel(speaker, service, fileService, false));
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.CALL_4_PAPERS;
    }

    @Override
    public String getPageTitle() {
        return "Prelegenci";
    }

}

package jelatyna.pages.admin.c4p.speaker;

import jelatyna.domain.Speaker;
import jelatyna.pages.admin.AbstractAddPage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.services.SpeakerService;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import static jelatyna.components.user.speaker.SpeakerFormPanel.*;


@SuppressWarnings("serial")
public class EditSpeakerPage extends AbstractAddPage<Speaker> {
    @SpringBean
    private SpeakerService service;

    public EditSpeakerPage(PageParameters params) {
        super(params);
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.CALL_4_PAPERS;
    }

    @Override
    public String getPageTitle() {
        return "Edytuj prelegenta";
    }

    @Override
    protected Speaker findById(int id) {
        return service.findById(id);
    }

    @Override
    protected Speaker createNew() {
        return new Speaker();
    }

    @Override
    protected void initPage(Speaker speaker) {
        add(newSpeakerFormPanel(service, speaker, ListSpeakerPage.class));
    }
}

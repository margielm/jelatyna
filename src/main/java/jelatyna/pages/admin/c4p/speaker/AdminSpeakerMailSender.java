package jelatyna.pages.admin.c4p.speaker;

import jelatyna.domain.Speaker;
import jelatyna.mail.MailSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AdminSpeakerMailSender {
    @Autowired
    private MailSender sender;

    public AdminSpeakerMailSender() {
    }

    public void sendMessage(Speaker speaker) {
        sender.setTemplateName("");
        sender.set("fullName", speaker.getFullName());
        sender.sendMessage(speaker);
    }
}

package jelatyna.pages.admin.c4p;

import jelatyna.components.mail.DefineMailPanel;
import jelatyna.pages.admin.AdminBasePage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;

@SuppressWarnings("serial")
public class DefineRegistrationMailPage extends AdminBasePage {

    public DefineRegistrationMailPage() {
        add(new DefineMailPanel("mail", "speaker", "$firstName, $lastName"));
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.CALL_4_PAPERS;
    }

    @Override
    public String getPageTitle() {
        return "Registration C4p E-mail";
    }
}

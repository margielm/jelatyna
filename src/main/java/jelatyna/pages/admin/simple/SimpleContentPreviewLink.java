package jelatyna.pages.admin.simple;

import jelatyna.components.LabeledLink;
import jelatyna.domain.SimpleContent;
import jelatyna.pages.confitura.ViewSimpleContentPage;

@SuppressWarnings("serial")
public class SimpleContentPreviewLink extends LabeledLink {

    static final String LABEL_FOR_DISABLED = "Zapisz, aby zobaczyć podgląd";
    static final String LABEL_FOR_ENABLED = "Zobacz podgląd";

    private final SimpleContent simpleContent;

    public SimpleContentPreviewLink(String id, SimpleContent content) {
        super(id, content.isNotNew() ? LABEL_FOR_ENABLED : LABEL_FOR_DISABLED);
        this.simpleContent = content;
    }

    @Override
    public void onClick() {
        ViewSimpleContentPage contentPage = new ViewSimpleContentPage(simpleContent);
        setResponsePage(contentPage);
    }

    @Override
    public boolean isEnabled() {
        return simpleContent.isNotNew();
    }

}
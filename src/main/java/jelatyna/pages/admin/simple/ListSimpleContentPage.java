package jelatyna.pages.admin.simple;

import jelatyna.components.DeleteEntityLink;
import jelatyna.components.RedirectLink;
import jelatyna.domain.SimpleContent;
import jelatyna.pages.admin.AdminBasePage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.repositories.SimpleContentRepository;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.List;

import static jelatyna.utils.Components.*;

@SuppressWarnings("serial")
public class ListSimpleContentPage extends AdminBasePage {

    @SpringBean
    private SimpleContentRepository repository;

    public ListSimpleContentPage() {
        add(new SimpleContentGrid("rows", repository.findAll()));
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.CONFERENCE_SITE;
    }

    @Override
    public String getPageTitle() {
        return "Strony statyczne";
    }

    private final class SimpleContentGrid extends DataView<SimpleContent> {
        private SimpleContentGrid(String id, List<SimpleContent> list) {
            super(id, new ListDataProvider<SimpleContent>(list));
        }

        @Override
        protected void populateItem(Item<SimpleContent> item) {
            SimpleContent simpleContent = item.getModelObject();
            item.add(label("title", simpleContent.getTitle()));
            item.add(new RedirectLink("edit", AddSimpleContentPage.class, simpleContent));
            item.add(new DeleteEntityLink(simpleContent, ListSimpleContentPage.class) {

                @Override
                protected void deleteById(Integer id) {
                    repository.delete(id);
                }
            });
        }
    }
}

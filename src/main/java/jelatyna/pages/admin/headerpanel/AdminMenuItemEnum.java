package jelatyna.pages.admin.headerpanel;

public enum AdminMenuItemEnum {

    CONFERENCE_SITE("Strona konferencji"),
    ORGANIZERS("Kapituła"),
    CALL_4_PAPERS("Call 4 Papers"),
    PARTICIPANTS("Rejestracja"),
    SPONSORS("Sponsorzy"),
    LOTTERY("Losowanie"),
    AGENDA("Agenda"),
    MY_PROFILE("Mój Profil"),
    NONE("");

    private String label;

    private AdminMenuItemEnum(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    @Override
    public String toString() {
        return label;
    }
}

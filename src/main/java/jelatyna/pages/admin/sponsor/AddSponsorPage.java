package jelatyna.pages.admin.sponsor;

import jelatyna.components.StaticImage;
import jelatyna.domain.Sponsor;
import jelatyna.domain.SponsorType;
import jelatyna.pages.admin.AbstractAddPage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.services.FileService;
import jelatyna.services.SponsorService;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import static jelatyna.utils.Components.*;
import static jelatyna.utils.Models.*;

@SuppressWarnings("serial")
public class AddSponsorPage extends AbstractAddPage<Sponsor> {

    @SpringBean
    private SponsorService service;
    @SpringBean
    private FileService fileService;

    public AddSponsorPage(PageParameters params) {
        super(params);
    }

    @Override
    protected void initPage(Sponsor sponsor) {
        add(new SponsorForm(sponsor));
    }

    @Override
    protected Sponsor createNew() {
        return new Sponsor();
    }

    @Override
    protected Sponsor findById(int id) {
        return service.findById(id);
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.SPONSORS;
    }

    @Override
    public String getPageTitle() {
        return "Dodaj/edytuj sponsora";
    }

    private final class SponsorForm extends Form<Void> {
        private final Sponsor sponsor;
        private FileUploadField fileUpload = new FileUploadField("logo");

        private SponsorForm(Sponsor sponsor) {
            super("form");
            this.sponsor = sponsor;
            add(new StaticImage("logoView", fileService.getUrlTo(sponsor)).setVisible(sponsor.isNotNew()));
            add(textField("name", propertyModel(sponsor, "name"), true));
            add(textField("webPage", propertyModel(sponsor, "webPage"), true));
            add(dropDown("type", new PropertyModel<SponsorType>(sponsor, "sponsorType"), service.findAllTypes(),
                    new ChoiceRenderer<SponsorType>("name")).setRequired(true));
            add(textField("money", propertyModel(sponsor, "money"), true));
            add(required(richEditor("desc", propertyModel(sponsor, "description"))));
            add(richEditor("folderDesc", propertyModel(sponsor, "folderDescription")));
            add(fileUpload.setRequired(!sponsor.isNotNew()));
            add(cancelLink(ListSponsorPage.class));
            add(new FeedbackPanel("feedback"));
        }

        @Override
        protected void onSubmit() {
            service.save(sponsor, fileUpload.getFileUpload());
            setResponsePage(ListSponsorPage.class);
        }
    }

}

package jelatyna.pages.admin.sponsor;

import jelatyna.components.DeleteEntityLink;
import jelatyna.components.RedirectLink;
import jelatyna.components.feedback.ConfituraFeedbackPanel;
import jelatyna.components.nogeneric.Link;
import jelatyna.domain.SponsorType;
import jelatyna.pages.admin.AbstractAddPage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.services.SponsorService;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import static jelatyna.utils.Components.*;
import static jelatyna.utils.Models.*;

@SuppressWarnings("serial")
public class SponsorTypesPage extends AbstractAddPage<SponsorType> {

    @SpringBean
    private SponsorService service;
    private FeedbackPanel feedback;

    public SponsorTypesPage(PageParameters params) {
        super(params);
    }

    @Override
    protected SponsorType createNew() {
        return new SponsorType();
    }

    @Override
    protected void initPage(final SponsorType sponsorType) {
        SponsorTypeForm form = new SponsorTypeForm(sponsorType);
        feedback = new ConfituraFeedbackPanel("feedback");
        form.add(feedback);
        add(form);
        add(new SponsorTypeList());
    }

    @Override
    protected SponsorType findById(int id) {
        return service.findTypeById(id);
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.SPONSORS;
    }

    @Override
    public String getPageTitle() {
        return "Typy sponsorów";
    }

    private final class SponsorTypeList extends DataView<SponsorType> {

        private SponsorTypeList() {
            super("types", new ListDataProvider<SponsorType>(service.findAllTypes()));
        }

        @Override
        protected void populateItem(Item<SponsorType> item) {
            final SponsorType sponsorType = item.getModelObject();
            item.add(label("name", sponsorType.getName()));
            item.add(label("minimalNo", sponsorType.getMinimalNo()));
            item.add(label("logoPlaceholder", sponsorType.getLogoPlaceholder()));
            item.add(label("urlPlaceholder", sponsorType.getUrlPlaceholder()));
            item.add(new MoveUpLink(sponsorType));
            item.add(new MoveDownLink(sponsorType));
            item.add(new RedirectLink("edit", SponsorTypesPage.class, sponsorType));
            item.add(new DeleteSponsorTypeLink(sponsorType));
        }
    }

    private final class DeleteSponsorTypeLink extends DeleteEntityLink {
        private final SponsorType sponsorType;

        public DeleteSponsorTypeLink(SponsorType sponsorType) {
            super(sponsorType, SponsorTypesPage.class);
            this.sponsorType = sponsorType;
        }

        @Override
        public void onClick() {
            if (service.canRemove(sponsorType)) {
                super.onClick();
            } else {
                feedback.error("Nie możesz usunąć tego Typu gdyż przypisani są do niego sponsorzy");
            }
        }

        @Override
        protected void deleteById(Integer id) {
            service.deleteTypeBy(id);
        }
    }

    private final class MoveDownLink extends Link {
        private final SponsorType sponsorType;

        private MoveDownLink(SponsorType sponsorType) {
            super("down");
            this.sponsorType = sponsorType;
        }

        @Override
        public void onClick() {
            service.moveDown(sponsorType);
            setResponsePage(SponsorTypesPage.class);
        }
    }

    private final class MoveUpLink extends Link {
        private final SponsorType sponsorType;

        private MoveUpLink(SponsorType sponsorType) {
            super("up");
            this.sponsorType = sponsorType;
        }

        @Override
        public void onClick() {
            service.moveUp(sponsorType);
            setResponsePage(SponsorTypesPage.class);
        }
    }

    private final class SponsorTypeForm extends Form<SponsorType> {
        private final SponsorType sponsorType;

        private SponsorTypeForm(SponsorType sponsorType) {
            super("form");
            this.sponsorType = sponsorType;
            add(withLabel("Nazwa", textField("name", propertyModel(sponsorType, "name"), true)));
            add(withLabel("Min. liczba pozycji", textField("minimalNo", propertyModel(sponsorType, "minimalNo"), true)));
            add(withLabel("Zastępcze logo", textField("logoPlaceholder", propertyModel(sponsorType, "logoPlaceholder"), true)));
            add(withLabel("Zastępczy URL", textField("urlPlaceholder", propertyModel(sponsorType, "urlPlaceholder"), true)));
        }

        @Override
        protected void onSubmit() {
            service.save(sponsorType);
            setResponsePage(SponsorTypesPage.class);
        }
    }
}

package jelatyna.pages.admin.menu;

import jelatyna.components.menu.MenuLinks;
import jelatyna.domain.MenuItem;
import jelatyna.domain.MenuLinkItem;
import jelatyna.pages.admin.AdminBasePage;
import jelatyna.repositories.MenuRepository;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.attributes.AjaxCallListener;
import org.apache.wicket.ajax.attributes.AjaxRequestAttributes;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import static jelatyna.utils.Components.*;
import static jelatyna.utils.Models.*;

@SuppressWarnings("serial")
public abstract class DefineMenuBasePage extends AdminBasePage {

    @SpringBean
    protected MenuRepository repository;
    @SpringBean
    protected MenuLinks menuItemList;

    protected FeedbackPanel feedbackPanel = new FeedbackPanel("feedback");
    protected Form<MenuItem> form = new Form<MenuItem>("form");
    protected MenuItem menu = getMenu();
    private ListView<MenuItem> itemsView;

    public DefineMenuBasePage() {
        initComponents();
    }

    protected void initComponents() {
        feedbackPanel.setOutputMarkupId(true);
        add(feedbackPanel);
        createAddItemButton();
        createSaveButton();
        initMenuItems();
        add(form);
    }

    protected void initMenuItems() {
        itemsView = new ListView<MenuItem>("menu_item", menu.getChildren()) {
            @Override
            protected void populateItem(final ListItem<MenuItem> item) {
                createManageItemButtons("item", item);
            }
        };
        form.add(itemsView);
    }

    protected void createSaveButton() {
        form.add(new Button("save") {
            @Override
            public void onSubmit() {
                menu = repository.save(menu);
                menuItemList.initLinks();
                itemsView.setModelObject(menu.getChildren());
                info("Menu zostało zapis");
            }
        });
    }

    void createManageItemButtons(String id, final ListItem<MenuItem> item) {
        createItemName(id, item);
        createRemoveButton(id, item);
        createUpButton(id, item);
        createDownButton(id, item);
        createPublishButton(id, item);
        createDropDown(id, item);
    }

    private void createDropDown(String id, final ListItem<MenuItem> item) {
        PropertyModel<MenuLinkItem> model = propertyModel(item.getModelObject(), "linkItem");
        DropDownChoice<MenuLinkItem> comboBox = dropDown(id + "_content", model, menuItemList.getAllItems());
        comboBox.setChoiceRenderer(new ChoiceRenderer<MenuLinkItem>("title", "title"));
        item.add(comboBox);
    }

    private void createPublishButton(String id, final ListItem<MenuItem> item) {
        AbstractReadOnlyModel<String> labelModel = new AbstractReadOnlyModel<String>() {

            @Override
            public String getObject() {
                return item.getModelObject().isPublished() + "";
            }
        };
        AjaxButton publishButton = new AjaxButton(id + "_publish", labelModel, form) {

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                item.add(new AttributeModifier("class", "btn-success"));
                item.getModelObject().togglePublished();
                target.add(form);
            }

            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form) {
            }

        };
        item.add(publishButton);

    }

    private void createRemoveButton(String id, final ListItem<MenuItem> item) {
        item.add(new RemoveButton(id + "_remove", form, item));
    }

    protected MarkupContainer createAddItemButton() {
        return form.add(new AjaxButton("item_add", form) {

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                menu.addEmptyChild();
                target.add(form);
                info("Dodano nowe puste menu");
                target.add(feedbackPanel);
            }

            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form) {
            }

        });
    }

    private MarkupContainer createItemName(String id, final ListItem<MenuItem> item) {
        return item.add(new TextField<String>(id + "_name", new PropertyModel<String>(item.getModelObject(), "name")));
    }

    private MarkupContainer createUpButton(String id, final ListItem<MenuItem> item) {
        return item.add(new AjaxButton(id + "_up", form) {

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                item.getModelObject().up();
                target.add(form);
            }

            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form) {

            }
        });
    }

    private MarkupContainer createDownButton(String id, final ListItem<MenuItem> item) {
        return item.add(new AjaxButton(id + "_down", form) {

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                item.getModelObject().down();
                target.add(form);
            }

            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form) {

            }
        });
    }

    protected abstract MenuItem getMenu();

    private final class RemoveButton extends AjaxButton {
        private final ListItem<MenuItem> item;

        private RemoveButton(String id, Form<?> form, ListItem<MenuItem> item) {
            super(id, form);
            this.item = item;
        }

        @Override
        protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
            item.getModelObject().remove();
            target.add(form);
        }

        @Override
        protected void updateAjaxAttributes(AjaxRequestAttributes attributes) {
            super.updateAjaxAttributes(attributes);
            attributes.getAjaxCallListeners().add(new AjaxCallListener().onPrecondition("if(!confirm('Na pewno usunąć?')) return false;"));
        }

        @Override
        protected void onError(AjaxRequestTarget target, Form<?> form) {

        }
    }

}

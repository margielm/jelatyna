package jelatyna.pages.admin.lottery;

import static jelatyna.utils.Components.*;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import jelatyna.components.nogeneric.Link;
import jelatyna.domain.Participant;
import jelatyna.pages.admin.AdminBasePage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.services.LotteryService;

@SuppressWarnings("serial")
public class LotteryPage extends AdminBasePage {

    @SpringBean
    private LotteryService service;

    private DrawModel model = new DrawModel();

    public LotteryPage() {
        Form<Participant> form = new Form<Participant>("form", new CompoundPropertyModel<>(model));
        form.add(label("firstName").setOutputMarkupId(true));
        form.add(label("lastName").setOutputMarkupId(true));
        form.add(label("mail"));
        add(form);
        add(new DrawButton("draw", form));
        add(new ResetLink("reset"));
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.LOTTERY;
    }

    @Override
    public String getPageTitle() {
        return "Losowanie";
    }

    private final class ResetLink extends Link {
        private ResetLink(String id) {
            super(id);
        }

        @Override
        public void onClick() {
            service.clear();
        }
    }

    private final class DrawButton extends AjaxLink<Void> {
        private final Form<Participant> form;

        private DrawButton(String id, Form<Participant> form) {
            super(id);
            this.form = form;
        }

        @Override
        public void onClick(AjaxRequestTarget target) {
            if (target != null) {
                model.doLoad();
                target.add(form);
            }
        }
    }

    private final class DrawModel extends LoadableDetachableModel<Participant> {

        private Participant drown = Participant.EMPTY;

        @Override
        protected Participant load() {
            return drown;
        }

        public void doLoad() {
            drown = service.draw();
        }
    }
}

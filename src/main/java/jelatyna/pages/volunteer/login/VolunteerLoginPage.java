package jelatyna.pages.volunteer.login;

import org.apache.wicket.spring.injection.annot.SpringBean;

import jelatyna.domain.Volunteer;
import jelatyna.pages.admin.login.LoginPage;
import jelatyna.pages.volunteer.VolunteerHomePage;
import jelatyna.services.VolunteerService;
import jelatyna.services.exceptions.LoginException;

@SuppressWarnings("serial")
public class VolunteerLoginPage extends LoginPage {

    @SpringBean
    private VolunteerService service;

    public VolunteerLoginPage() {
    }

    @Override
    protected void tryToLogin() {
        Volunteer volunteer = service.loginWith(email, password);
        getSession().setUser(volunteer);
        setResponsePage(VolunteerHomePage.class);
    }

    @Override
    protected  void reportLoginError(LoginException ex) {
        getSession().error(ex.getMessage());
        setResponsePage(VolunteerLoginPage.class);
    }
}

package jelatyna.pages.volunteer.participant;

import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.pages.admin.registration.RegisterParticipantsPanel;
import jelatyna.pages.volunteer.VolunteerBasePage;

@SuppressWarnings("serial")
public class RegisterParticipantsPage extends VolunteerBasePage {
    public RegisterParticipantsPage() {
        add(new RegisterParticipantsPanel("RegisterPanel"));

    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.PARTICIPANTS;
    }

    @Override
    public String getPageTitle() {
        return "Rejestracja Uczestników";
    }
}

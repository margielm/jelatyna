package jelatyna.pages.volunteer;

import org.apache.wicket.Application;
import org.apache.wicket.markup.html.TransparentWebMarkupContainer;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import jelatyna.Confitura;
import jelatyna.ConfituraSession;
import jelatyna.components.feedback.ConfituraFeedbackPanel;
import jelatyna.domain.AbstractEntity;
import jelatyna.pages.admin.AdminBasePage;
import jelatyna.pages.admin.AdminPageTitlePanel;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.pages.admin.headerpanel.AdminTopHeaderPanel;
import jelatyna.pages.admin.registration.RegisterParticipantsPage;
import jelatyna.pages.volunteer.login.VolunteerLoginPage;
import jelatyna.pages.volunteer.scan.ScanPage;
import jelatyna.pages.volunteer.user.ChangePasswordPage;
import jelatyna.pages.volunteer.user.EditProfilePage;
import jelatyna.utils.PageParametersBuilder;

@SuppressWarnings("serial")
public abstract class VolunteerBasePage extends AdminBasePage {
    private WebMarkupContainer wrapper;
    protected FeedbackPanel feedback;


    public VolunteerBasePage() {
        feedback = new ConfituraFeedbackPanel("feedback");
        add(feedback);
    }

    @Override
    protected void init() {
        add(new AdminTopHeaderPanel.Builder("headerPanel", VolunteerHomePage.class, "Jelatyna", getActiveMenu())
                .withMenuItemAsDropdown(AdminMenuItemEnum.PARTICIPANTS, ScanPage.class, "Scan")
                .withMenuItemAsDropdown(AdminMenuItemEnum.PARTICIPANTS, RegisterParticipantsPage.class, "Szukaj")
                .withMenuItemAsDropdown(AdminMenuItemEnum.MY_PROFILE, EditProfilePage.class, "Edycja")
                .withMenuItemAsDropdown(AdminMenuItemEnum.MY_PROFILE, ChangePasswordPage.class, "Zmień hasło")
                .build());

        wrapper = new TransparentWebMarkupContainer("wrapper");
        wrapper.setOutputMarkupId(true);
        wrapper.add(new AdminPageTitlePanel("pageTitle", getPageTitle()));

        add(wrapper);
        if (!ConfituraSession.get().isVolunteerAvailable()) {
            setResponsePage(VolunteerLoginPage.class);
        }

    }

    protected Confitura getApp() {
        return (Confitura) Application.get();
    }

    protected PageParameters paramsWithId(AbstractEntity<?> entity) {
        return PageParametersBuilder.paramsFor("id", entity.getId());
    }

}

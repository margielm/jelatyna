package jelatyna.pages.volunteer.scan;

import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.pages.admin.registration.ScanPanel;
import jelatyna.pages.volunteer.VolunteerBasePage;
import jelatyna.pages.volunteer.participant.RegisterParticipantsPage;
import jelatyna.services.ParticipantService;

public class ScanPage extends VolunteerBasePage {
    @SpringBean
    private ParticipantService service;

    public ScanPage(PageParameters params) {
        add(new ScanPanel("scanPanel", params.get("token"), service, this.getClass(), RegisterParticipantsPage.class));
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.PARTICIPANTS;
    }

    @Override
    public String getPageTitle() {
        return "Scan";
    }
}

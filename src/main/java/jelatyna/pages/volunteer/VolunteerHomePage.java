package jelatyna.pages.volunteer;

import org.apache.wicket.RestartResponseException;
import org.apache.wicket.spring.injection.annot.SpringBean;

import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.pages.volunteer.scan.ScanPage;
import jelatyna.services.ParticipantService;

@SuppressWarnings("serial")
public class VolunteerHomePage extends VolunteerBasePage {

    @SpringBean
    private ParticipantService service;

    public VolunteerHomePage() {
//        redirectToInterceptPage(ScanPage);
//        add(new ScanPanel("RegisterPanel",new Participant(),service));

        throw new RestartResponseException(
                ScanPage.class
        );
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.NONE;
    }

    @Override
    public String getPageTitle() {
        return "Wolontariusz";
    }
}

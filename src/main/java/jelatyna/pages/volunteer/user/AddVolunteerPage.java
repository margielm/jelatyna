package jelatyna.pages.volunteer.user;

import static jelatyna.components.user.UserFormPanel.*;

import org.apache.wicket.Page;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import jelatyna.domain.Volunteer;
import jelatyna.pages.admin.AbstractAddPage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.pages.admin.user.ListAdminPage;
import jelatyna.services.VolunteerService;

@SuppressWarnings("serial")
public class AddVolunteerPage extends AbstractAddPage<Volunteer> {

    @SpringBean
    private VolunteerService service;

    public AddVolunteerPage(PageParameters params) {
        super(params);
    }

    @Override
    protected void initPage(Volunteer admin) {
        add(newUserFormPanel(service, admin, getRedirectPage()));
    }

    @Override
    protected Volunteer createNew() {
        return new Volunteer();
    }

    @Override
    protected Volunteer findById(int id) {
        return service.findById(id);
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.ORGANIZERS;
    }

    @Override
    public String getPageTitle() {
        return "Dodaj/edytuj wolontariusza";
    }

    Class<? extends Page> getRedirectPage() {
        return ListAdminPage.class;
    }
}

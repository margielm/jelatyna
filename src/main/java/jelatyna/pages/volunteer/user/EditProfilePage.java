package jelatyna.pages.volunteer.user;

import static jelatyna.components.user.UserFormPanel.*;
import static jelatyna.utils.PageParametersBuilder.*;

import org.apache.wicket.Page;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.string.StringValue;

import jelatyna.ConfituraSession;
import jelatyna.domain.Volunteer;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.pages.volunteer.VolunteerBasePage;
import jelatyna.pages.volunteer.VolunteerHomePage;
import jelatyna.services.VolunteerService;

@SuppressWarnings("serial")
public class EditProfilePage extends VolunteerBasePage {

    @SpringBean
    private VolunteerService service;

    public EditProfilePage() {
        PageParameters params = paramsFor("id", ConfituraSession.get().getUser().getId());
        initPage(get(params.get("id")));
    }

    protected void initPage(Volunteer admin) {
        add(newUserFormPanel(service, admin, getRedirectPage()));
    }

    protected Volunteer findById(int id) {
        return service.findById(id);
    }

    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.MY_PROFILE;
    }

    public String getPageTitle() {
        return "Edytuj profil";
    }

    Class<? extends Page> getRedirectPage() {
        return VolunteerHomePage.class;
    }

    protected Volunteer get(StringValue id) {
        return findById(id.toInt());
    }
}

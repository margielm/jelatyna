package jelatyna.pages.volunteer.user;

import org.apache.wicket.spring.injection.annot.SpringBean;

import jelatyna.ConfituraSession;
import jelatyna.components.user.ChangePasswordPanel;
import jelatyna.domain.Volunteer;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.pages.volunteer.VolunteerBasePage;
import jelatyna.pages.volunteer.VolunteerHomePage;
import jelatyna.services.VolunteerService;

@SuppressWarnings("serial")
public class ChangePasswordPage extends VolunteerBasePage {

    @SpringBean
    private VolunteerService service;

    public ChangePasswordPage() {
        add(new ChangePasswordPanel<>(getUser(ConfituraSession.get().getUser().getId()), service,
                VolunteerHomePage.class));
    }

    private Volunteer getUser(Integer id) {
        return service.findById(id);
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.MY_PROFILE;
    }

    @Override
    public String getPageTitle() {
        return "Zmień Hasło";
    }

}

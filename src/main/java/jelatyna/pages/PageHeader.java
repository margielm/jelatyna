package jelatyna.pages;

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import jelatyna.utils.WicketUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.PageHeaderItem;
import org.apache.wicket.request.cycle.RequestCycle;

import java.io.Serializable;
import java.util.Set;

import static com.google.common.base.Preconditions.*;
import static java.lang.String.format;
import static org.apache.commons.lang.StringEscapeUtils.*;

@SuppressWarnings("serial")
public class PageHeader implements Serializable {

    private String defaultPageTitlePrefix;

    private String title;
    private String descriptionForFacebook;
    private String conferenceLogoImagePath;
    private Set<String> imagesForFacebook;

    public PageHeader(String defaultPageTitlePrefix, String defaultDescriptionForFacebook, String conferenceLogoImagePath) {
        checkArguments(defaultPageTitlePrefix, defaultDescriptionForFacebook, conferenceLogoImagePath);

        this.defaultPageTitlePrefix = defaultPageTitlePrefix;
        this.descriptionForFacebook = defaultDescriptionForFacebook;
        this.title = defaultPageTitlePrefix;
        this.conferenceLogoImagePath = conferenceLogoImagePath;
        this.imagesForFacebook = Sets.newLinkedHashSet();
    }

    public IHeaderResponse renderHead(IHeaderResponse response) {
        renderTitle(response);
        renderTitleDataForFacebook(response);
        renderDescriptionDataForFacebook(response);
        renderImagesDataForFacebook(response);

        return response;
    }

    public void setPageTitlePostfix(String pageTitlePostfix) {
        checkNotNull(pageTitlePostfix, "Page title postfix can not be null");
        checkArgument(pageTitlePostfix.trim().length() > 0, "Page title postfix can not be an empty String");

        setTitle(pageTitlePostfix);
    }

    public void setDescriptionForFacebook(String descriptionForFacebook) {
        this.descriptionForFacebook = descriptionForFacebook;
    }

    public void addImageForFacebook(String imageUrl) {
        if (StringUtils.isEmpty(imageUrl)) {
            return;
        }
        imageUrl = changeRelativeUrlToNonRelative(imageUrl);
        imagesForFacebook.add(imageUrl);
    }

    private void checkArguments(String defaultPageTitlePrefix, String defaultDescriptionForFacebook, String conferenceLogoImagePath) {
        Preconditions.checkNotNull(defaultPageTitlePrefix, "Default page title prefix can not be null");
        Preconditions.checkNotNull(defaultDescriptionForFacebook, "Description for Facebook can not be null");
        Preconditions.checkNotNull(conferenceLogoImagePath, "Conference logo path can not be null");
    }

    private void renderTitle(IHeaderResponse response) {
        response.render(new PageHeaderItem(format("<title>%s</title>", escapeHtml(title))));
    }

    private void renderTitleDataForFacebook(IHeaderResponse response) {
        response.render(new PageHeaderItem(format("<meta property=\"og:title\" content=\"%s\" />", escapeHtml(title))));
    }

    private void renderDescriptionDataForFacebook(IHeaderResponse response) {
        response.render(new PageHeaderItem(
                format("<meta property=\"og:description\" content=\"%s\" />", escapeHtml(descriptionForFacebook))));
    }

    private void renderImagesDataForFacebook(IHeaderResponse response) {
        for (String imagePath : imagesForFacebook) {
            appendImageDataForFacebook(response, imagePath);
        }
        appendImageDataForFacebook(response, conferenceLogoImagePath);
    }

    private String changeRelativeUrlToNonRelative(String imageUrl) {
        if (urlIsRelative(imageUrl)) {
            return WicketUtils.getRootUrlString(RequestCycle.get()) + imageUrl;
        }
        return imageUrl;
    }

    private boolean urlIsRelative(String imageUrl) {
        return !(StringUtils.startsWithIgnoreCase(imageUrl.toLowerCase(), "http://") ||
                StringUtils.startsWithIgnoreCase(imageUrl, "https://")
                || StringUtils.startsWithIgnoreCase(imageUrl, "www."));
    }

    private void appendImageDataForFacebook(IHeaderResponse response, String imagePath) {
        response.render(new PageHeaderItem(format("<meta property=\"og:image\" content=\"%s\" />", escapeHtml(imagePath))));
    }

    private void setTitle(String pageTitlePostfix) {
        String postfix = pageTitlePostfix != null ? " - " + pageTitlePostfix : "";
        title = defaultPageTitlePrefix + postfix;
    }
}

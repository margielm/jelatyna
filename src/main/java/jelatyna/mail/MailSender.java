package jelatyna.mail;

import java.io.StringWriter;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jelatyna.domain.WithMail;
import jelatyna.repositories.MailTemplateRepository;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MailSender {
    protected VelocityContext ctx = new VelocityContext();

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private MailTemplateRepository repository;

    @Autowired
    private SendGridSender sender;

    @Value("${conference.year}")
    private String conferenceYear;

    private String templateName;

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public void sendMessage(WithMail recipient) {
        send(recipient.getMail());
    }

    private void send(String address) {
        try {
            logger.info("Sending to " + address);
            sender.send(address, getSubject(), getContent());
            logger.info("Message sent");
        } catch (Exception ex) {
            logger.error(String.format("Exception while sending message to [%s]", address), ex);
        }
    }

    public String getContent() {
        return getText(getTemplate());
    }

    public String getText(String template) {
        try {
            StringWriter message = new StringWriter();
            Velocity.evaluate(ctx, message, "message", template);
            return message.toString();
        } catch (Exception ex) {
            throw new RuntimeException("Exception while evaluating message template [%s]", ex);
        }
    }

    public String getSubject() {
        return getText(getSubjectTemplate());
    }

    public String getSubjectTemplate() {
        return repository.readByType(templateName).getSubject();
    }

    public String getTemplate() {
        return repository.readByType(templateName).getTemplate();
    }

    public void set(String name, Object value) {
        ctx.put(name, value);
    }

}

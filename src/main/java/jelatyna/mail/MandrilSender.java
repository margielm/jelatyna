package jelatyna.mail;

import static com.google.common.collect.Lists.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.microtripit.mandrillapp.lutung.MandrillApi;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage;

@Service
public class MandrilSender implements MailService {

    private MandrillApi api;

    @Autowired
    public MandrilSender(@Value("${mandril.key}") String key) {
        api = new MandrillApi(key);
    }

    @Override
    public void send(String to, String subject, String content) {
        try {
            api.messages().send(createMessage(to, subject, content), false);
        } catch (Exception ex) {
            throw new RuntimeException("Exception while sending email", ex);
        }
    }

    private MandrillMessage createMessage(String to, String subject, String content) {
        MandrillMessage.Recipient recipient = new MandrillMessage.Recipient();
        recipient.setEmail(to);

        MandrillMessage message = new MandrillMessage();
        message.setFromEmail("confitura@confitura.pl");
        message.setFromName("Confitura");
        message.setSubject(subject);
        message.setTo(newArrayList(recipient));
        message.setHtml(content);
        return message;
    }
}

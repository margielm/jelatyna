package jelatyna.mail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.sendgrid.SendGrid;

@Service
public class SendGridSender implements MailService {

    private SendGrid api;

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public SendGridSender(@Value("${sendgrid.key}") String key) {
        api = new SendGrid(key);
    }

    @Override
    public void send(String to, String subject, String content) {
        try {
            String message = api.send(createMessage(to, subject, content)).getMessage();
            log.info("Message sent [{}]", message);
        } catch (Exception ex) {
            throw new RuntimeException("Exception while sending email", ex);
        }
    }

    private SendGrid.Email createMessage(String to, String subject, String content) {
        SendGrid.Email email = new SendGrid.Email();
        email.addTo(to);
        email.setFrom("confitura@confitura.pl");
        email.setFromName("Confitura");
        email.setSubject(subject);
        email.setHtml(content);
        return email;
    }
}

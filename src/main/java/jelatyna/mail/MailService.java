package jelatyna.mail;

public interface MailService {
    void send(String to, String subject, String content);
}

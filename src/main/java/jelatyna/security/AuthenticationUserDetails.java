package jelatyna.security;

import jelatyna.domain.Speaker;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.HashSet;

public class AuthenticationUserDetails implements UserDetails {
    private static final long serialVersionUID = -8297478803363997090L;

    private final String login;
    private final String password;
    private final boolean enabled;
    private Speaker speaker;
    private HashSet<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>();

    public AuthenticationUserDetails(Speaker speaker) {
        this.login = speaker.getMail();
        this.password = speaker.getPassword();
        this.enabled = true;
        this.grantedAuthorities.addAll(new HashSet<GrantedAuthority>());
        this.speaker = speaker;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return grantedAuthorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public Speaker getSpeaker() {
        return speaker;
    }
}

package jelatyna.security;

import jelatyna.domain.Speaker;
import jelatyna.repositories.SpeakerRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.openid.OpenIDAuthenticationToken;

public class AuthenticationWithOpenIdUserDetailsGetter implements UserDetailsService {
    private SpeakerRepository speakerRepository;

    public AuthenticationWithOpenIdUserDetailsGetter(SpeakerRepository speakerRepository) {
        this.speakerRepository = speakerRepository;
    }

    /**
     * Probuje znalezc w bazie speakera na podstawie open id, jesli znajdzie
     * umieszcza go w SecurityContext. W przeciwnym wypadku rzuca wyjatek ktory
     * (patrz security.xml) powoduje wywolanie klasy obslugujacej nieudane
     * logowanie.
     */
    @Override
    public UserDetails loadUserByUsername(String openIdLogin) throws UsernameNotFoundException {

        Speaker speaker = speakerRepository.findByOpenIdLogin(openIdLogin);

        throwExceptionIfNotFound(speaker, openIdLogin);

        AuthenticationUserDetails userDetails = new AuthenticationUserDetails(speaker);
        OpenIDAuthenticationToken openIdAuthenticationToken = new OpenIDAuthenticationToken(userDetails,
                userDetails.getAuthorities(), null, null);
        SecurityContextHolder.getContext().setAuthentication(openIdAuthenticationToken);

        return new AuthenticationUserDetails(speaker);
    }

    private void throwExceptionIfNotFound(Object object, String openIdLogin) {
        if (object == null) {
            throw new UsernameNotFoundException("User with open id login " + openIdLogin + " has not been found.");
        }
    }
}
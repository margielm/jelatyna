package jelatyna.security;


import jelatyna.domain.Participant;
import jelatyna.domain.Speaker;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtils {

    public static String getUsername(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = "";
        if( authentication != null && authentication.getPrincipal() instanceof Participant){
            username = ((Participant) authentication.getPrincipal()).getFirstName();
        }
        return authentication != null ? username : "";
    }

    public  static boolean isAnonymous(){
        String username = getUsername();
        return "".equals(username) ? true : false;
    }
}
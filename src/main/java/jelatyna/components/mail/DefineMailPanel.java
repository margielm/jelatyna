package jelatyna.components.mail;

import jelatyna.domain.MailTemplate;
import jelatyna.pages.admin.AdminHomePage;
import jelatyna.repositories.MailTemplateRepository;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import static jelatyna.utils.Components.*;
import static jelatyna.utils.Models.*;


@SuppressWarnings("serial")
public class DefineMailPanel extends Panel {
    private final String mailType;
    private final String availableVars;
    @SpringBean
    private MailTemplateRepository repository;

    public DefineMailPanel(String id, String mailType, String availableVars) {
        super(id);
        this.mailType = mailType;
        this.availableVars = availableVars;
        add(new DefineMailTemplateForm(getMailTemplate()));
    }

    private MailTemplate getMailTemplate() {
        MailTemplate mailTemplate = repository.readByType(mailType);
        return mailTemplate != null ? mailTemplate : new MailTemplate(mailType);
    }

    private final class DefineMailTemplateForm extends Form<Void> {
        private final MailTemplate mailTemplate;

        private DefineMailTemplateForm(MailTemplate mailTemplate) {
            super("form");
            this.mailTemplate = mailTemplate;
            add(label("vars", availableVars));
            add(textField("subject", propertyModel(mailTemplate, "subject")));
            add(richEditor("template", propertyModel(mailTemplate, "template")));
            add(cancelLink(AdminHomePage.class));
        }

        @Override
        protected void onSubmit() {
            repository.save(mailTemplate);
        }
    }
}

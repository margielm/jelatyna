package jelatyna.components.validatotion;

import org.apache.wicket.validation.INullAcceptingValidator;
import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.IValidator;
import org.apache.wicket.validation.ValidationError;

import java.io.Serializable;

import static org.springframework.util.Assert.*;

@SuppressWarnings("serial")
public class MoreThanZeroIntegerValidator implements IValidator<Integer>, INullAcceptingValidator<Integer>,
        Serializable {
    @Override
    public void validate(IValidatable<Integer> toBeValidated) {
        notNull(toBeValidated);
        if (toBeValidated.getValue() == null || toBeValidated.getValue() < 1) {
            toBeValidated.error(new ValidationError().setMessage("Bez jaj, nie możesz wpisać mniej niż 1"));
        }
    }
}

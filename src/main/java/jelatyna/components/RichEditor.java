package jelatyna.components;

import static org.apache.wicket.markup.head.JavaScriptHeaderItem.*;

import java.io.Serializable;

import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.html.IHeaderContributor;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.model.IModel;

@SuppressWarnings("serial")
public class RichEditor<T extends Serializable> extends TextArea<T> implements IHeaderContributor {

    public RichEditor(String id, IModel<T> model) {
        super(id, model);
        add(new AttributeAppender("class", " richeditor"));
    }

    @Override
    public void renderHead(IHeaderResponse response) {
//        response.render(forUrl("/ckeditor/ckeditor.js"));
//        response.render(forUrl("/ckeditor/adapters/jquery.js"));
        response.render(forScript("$(document).ready(function(){$('textarea.richeditor').ckeditor();});", "init_rich_editor"));
    }
}

package jelatyna.components;

import jelatyna.components.nogeneric.Link;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.MarkupStream;

import static org.apache.commons.lang.StringUtils.*;

@SuppressWarnings("serial")
public abstract class LabeledLink extends Link {

    private String label;

    public LabeledLink(String id, String label) {
        super(id);
        this.label = label;
    }

    @Override
    public void onComponentTagBody(MarkupStream markupStream, ComponentTag openTag) {
        if (isNotEmpty(getLabel())) {
            replaceComponentTagBody(markupStream, openTag, getLabel());
        } else {
            super.onComponentTagBody(markupStream, openTag);
        }
    }

    public String getLabel() {
        return label;
    }
}

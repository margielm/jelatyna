package jelatyna.components;

import org.apache.wicket.Component;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.form.FormComponent;

@SuppressWarnings("serial")
public class ValidateBehavior extends Behavior {

    @Override
    public void onComponentTag(Component component, ComponentTag tag) {
        super.onComponentTag(component, tag);
        if (((FormComponent<?>) component).isValid() == false) {
            tag.append("class", "error", " ");
        }
    }

}

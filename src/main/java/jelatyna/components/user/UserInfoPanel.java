package jelatyna.components.user;

import static jelatyna.utils.Components.*;
import static jelatyna.utils.Models.*;

import org.apache.commons.lang.StringUtils;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import jelatyna.components.StaticImage;
import jelatyna.components.nogeneric.Link;
import jelatyna.domain.User;
import jelatyna.services.FileService;

@SuppressWarnings("serial")
public class UserInfoPanel<USER extends User<?>> extends Panel {
    @SpringBean
    private FileService fileService;

    public UserInfoPanel(String id, USER user) {
        super(id);
        add(new AttributeModifier("id", model(user.getFullName())));
        add(label("first_name", user.getFirstName()).setRenderBodyOnly(true));
        add(label("last_name", user.getLastName()));
        add(richLabel("bio", user.getBio()));
        add(photoOf(user));

    }

    private WebMarkupContainer photoOf(USER user) {
        String photoUrl = fileService.getUrlTo(user);
        WebMarkupContainer figure = new WebMarkupContainer("figure");
        figure.setVisible(StringUtils.isNotBlank(photoUrl));
        figure.add(linkToBigPhoto(user).add(new StaticImage("photo", photoUrl)));
        return figure;
    }

    private Link linkToBigPhoto(USER user) {
        Link link = new Link("big_photo") {

            @Override
            public void onClick() {
            }
        };
        link.add(new AttributeModifier("href", fileService.getUrlTo(user)));
        return link;
    }

}

package jelatyna.components.user.speaker;

import edu.emory.mathcs.backport.java.util.Arrays;
import jelatyna.components.user.UserFormPanel;
import jelatyna.domain.BioLocation;
import jelatyna.domain.Speaker;
import jelatyna.domain.User;
import jelatyna.pages.confitura.c4p.UserEditedCallback;
import jelatyna.services.UserService;
import org.apache.wicket.Component;
import org.apache.wicket.Page;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.EnumChoiceRenderer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.PropertyModel;

import static jelatyna.utils.Components.*;

@SuppressWarnings("serial")
public class SpeakerFormPanel extends UserFormPanel<Speaker> {

    public SpeakerFormPanel(UserService<Speaker> service, Speaker user, Class<? extends Page> redirectPage) {
        super(service, user, redirectPage);
    }

    public SpeakerFormPanel(UserService<Speaker> service, Speaker user, Class<? extends Page> redirectPage, UserEditedCallback<Speaker> callback) {
        super(service, user, redirectPage, callback);
    }

    public static SpeakerFormPanel newSpeakerFormPanel(UserService<Speaker> service, Speaker user,
                                                       Class<? extends Page> redirectPage) {
        return new SpeakerFormPanel(service, user, redirectPage);
    }

    public static SpeakerFormPanel newSpeakerFormPanel(UserService<Speaker> service, Speaker user,
                                                       Class<? extends Page> redirectPage, UserEditedCallback<Speaker> callback) {
        return new SpeakerFormPanel(service, user, redirectPage, callback);
    }

    @Override
    protected void addAdditionalComponentsToForm(Form form, User speaker) {
        super.addAdditionalComponentsToForm(form, speaker);
        form.add(fillFullProfileWarning(speaker.isProfileFilled()));
        form.add(i18nLabel("speaker.description.location"));
        form.add(bioLocationDropdown(speaker));
    }

    private Component fillFullProfileWarning(boolean isProfileFilled) {
        WebMarkupContainer wmc = new WebMarkupContainer("warning_fill_profile");
        wmc.add(i18nLabel("warning_container", "speaker.fill.profile.warning"));
        wmc.setVisible(!isProfileFilled);
        return wmc;
    }

    private Component bioLocationDropdown(User user) {
        return new DropDownChoice<BioLocation>("bioLocation",
                new PropertyModel<>(user, "bioLocation"),
                Arrays.asList(BioLocation.values()),
                new EnumChoiceRenderer<>());
    }
}

package jelatyna.components.user.speaker;

import static jelatyna.utils.Components.*;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.Page;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.springframework.security.core.context.SecurityContextHolder;

import jelatyna.ConfituraSession;
import jelatyna.components.DeleteEntityLink;
import jelatyna.components.RedirectLink;
import jelatyna.components.feedback.ConfituraFeedbackPanel;
import jelatyna.components.nogeneric.Link;
import jelatyna.components.user.UserInfoPanel;
import jelatyna.domain.AbstractEntity;
import jelatyna.domain.Presentation;
import jelatyna.domain.Speaker;
import jelatyna.pages.admin.c4p.presentation.EditPresentationPage;
import jelatyna.pages.admin.c4p.presentation.ViewPresentationPage;
import jelatyna.pages.confitura.c4p.ChangePasswordPage;
import jelatyna.pages.confitura.c4p.EditSpeakerPage;
import jelatyna.pages.confitura.c4p.ViewSpeakerPage;
import jelatyna.pages.confitura.c4p.login.LoginSpeakerPage;
import jelatyna.pages.confitura.c4p.presentation.AddPresentationPage;
import jelatyna.services.FileService;
import jelatyna.services.SpeakerService;

@SuppressWarnings("serial")
public class SpeakerPanel extends Panel {

    private final SpeakerService service;

    private final FileService fileService;

    public SpeakerPanel(Speaker speaker, SpeakerService service, FileService fileService, boolean editable) {
        super("speaker_panel");
        this.service = service;
        this.fileService = fileService;

        addPresentationLink(speaker);
        add(new UserInfoPanel<>("speaker", speaker));
        add(new Toolbar(speaker.isFacebookAccount()).setVisible(editable));
        add(new ConfituraFeedbackPanel("feedback"));
        add(i18nLabel("item.status.label"), i18nLabel("votes.count.label"), i18nLabel("votes.avg.label"));
        add(new PresentationsList("presentations", speaker, editable));
        add(i18nLabel("item.avg.rating.description"));
    }

    private void addPresentationLink(Speaker speaker) {
        Class<? extends Page> page = AddPresentationPage.class;
        PageParameters params = new PageParameters();
        if (ConfituraSession.get().hasAdmin()) {
            page = EditPresentationPage.class;
            params.add("speaker_id", speaker.getId());
        }
        add(new RedirectLink("add_presentation", page, params)
                .add(i18nLabel("item.add.label"))
                .setVisible(ConfituraSession.get().hasAdmin() || service.isCall4PapersActive()));
    }

    private final class Toolbar extends WebMarkupContainer {
        public Toolbar(boolean isFacebookUser) {
            super("toolbar");
            add(new RedirectLink("edit", EditSpeakerPage.class).add(i18nLabel("edit.label")));
            RedirectLink changePassword = new RedirectLink("change_password", ChangePasswordPage.class);
            changePassword.setVisible(!isFacebookUser);
            add(changePassword.add(i18nLabel("password.change.label")));

            add(new LogoutLink("logout").add(i18nLabel("logout.label")));
        }

    }

    private final class PresentationsList extends ListView<Presentation> {

        private final boolean editable;

        private final Integer speakerId;

        private PresentationsList(String id, Speaker speaker, boolean editable) {
            super(id, speaker.getPresentations());
            this.editable = editable;
            this.speakerId = speaker.getId();
        }

        @Override
        protected void populateItem(ListItem<Presentation> item) {
            Presentation presentation = item.getModelObject();
            item.add(label("title", presentation.getTitle()));
            item.add(createFileLink(presentation));
            item.add(label("votes_count", service.countVotesFor(presentation)));
            item.add(label("votes_rate", service.averageRateFor(presentation)));
            item.add(i18nLabel("status", presentation.getStatusKey()));
            item.add(createEditToolbar(presentation).setVisible(editable && isOwner(presentation)));
            item.add(new RedirectLink("view", "view.label", ViewPresentationPage.class, presentation).setVisible(!editable));

        }

        private Component createFileLink(Presentation presentation) {
            return new ExternalLink("file", fileService.getUrlTo(presentation))
                    .setVisible(presentation.hasFile())
                    .add(new AttributeModifier("title", presentation.getFileName()))
                    .add(new AttributeModifier("target", "_blank"));
        }

        private boolean isOwner(Presentation presentation) {
            return presentation.getOwner().getId().equals(speakerId);
        }

        private WebMarkupContainer createEditToolbar(final Presentation presentation) {
            WebMarkupContainer toolbar = new WebMarkupContainer("presentation_edit_toolbar");
            toolbar.add(new RedirectLink("edit", "edit.label", AddPresentationPage.class, presentation));
            toolbar.add(new DeletePresentationLink(presentation, ViewSpeakerPage.class, presentation));
            return toolbar;
        }
    }

    private final class LogoutLink extends Link {

        private LogoutLink(String id) {
            super(id);
        }

        @Override
        public void onClick() {
            ConfituraSession.get().invalidateNow();
            SecurityContextHolder.getContext().setAuthentication(null);
            setResponsePage(LoginSpeakerPage.class);
        }
    }

    private final class DeletePresentationLink extends DeleteEntityLink {

        private final Presentation presentation;

        private DeletePresentationLink(AbstractEntity<?> entity, Class<? extends Page> page, Presentation presentation) {
            super(entity, page);
            add(i18nLabel("delete.label"));
            this.presentation = presentation;
        }

        @Override
        public void onClick() {
            if (!presentation.isAccepted()) {
                super.onClick();
            } else {
                error(new ResourceModel("item.delete-accepted.error"));
            }
        }

        @Override
        protected void deleteById(Integer id) {
            service.delete(presentation);
        }
    }
}

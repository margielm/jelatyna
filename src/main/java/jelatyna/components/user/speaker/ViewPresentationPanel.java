package jelatyna.components.user.speaker;

import jelatyna.components.RedirectLink;
import jelatyna.components.presentation.PresentationLinksPanel;
import jelatyna.domain.Presentation;
import jelatyna.domain.Speaker;
import jelatyna.pages.confitura.speaker.ListSpeakerPage;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.markup.html.panel.Panel;

import static jelatyna.utils.Components.*;

@SuppressWarnings("serial")
public class ViewPresentationPanel extends Panel {

    private ViewPresentationPanel(Presentation presentation, boolean withLinkToSpeaker) {
        super("presentation_panel");
        add(new AttributeModifier("id", presentation.getId()));
        add(label("title", presentation.getTitle()));
        add(linkToSpeaker(presentation.getOwner()).setRenderBodyOnly(!withLinkToSpeaker));
        add(richLabel("description", presentation.getDescription()));
        add(richLabel("shortDescription", presentation.getShortDescription()));
        add(new PresentationLinksPanel("links", presentation));
    }

    public static ViewPresentationPanel createPanelWithLinkToSpeaker(Presentation presentation) {
        return new ViewPresentationPanel(presentation, true);
    }

    public static ViewPresentationPanel createPanel(Presentation presentation) {
        return new ViewPresentationPanel(presentation, false);
    }

    private Component linkToSpeaker(final Speaker speaker) {
        RedirectLink link = new RedirectLink("speaker", ListSpeakerPage.class) {
            @Override
            protected CharSequence appendAnchor(org.apache.wicket.markup.ComponentTag tag, CharSequence url) {
                return super.appendAnchor(tag, url + "#" + speaker.getFullName());
            }
        };
        link.add(label("first_name", speaker.getFirstName()));
        link.add(label("last_name", speaker.getLastName()));
        link.setEnabled(speaker.isAccepted());
        return link;
    }

}

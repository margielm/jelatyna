package jelatyna.components.user;

import static jelatyna.utils.Components.*;
import static jelatyna.utils.Models.*;

import java.io.Serializable;

import org.apache.wicket.Page;
import org.apache.wicket.extensions.validation.validator.RfcCompliantEmailAddressValidator;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.ResourceModel;

import jelatyna.Confitura;
import jelatyna.components.RichEditor;
import jelatyna.components.feedback.ConfituraFeedbackPanel;
import jelatyna.domain.User;
import jelatyna.pages.confitura.c4p.UserEditedCallback;
import jelatyna.services.PhotoValidator;
import jelatyna.services.UserService;
import jelatyna.services.exceptions.LoginException;
import jelatyna.utils.Models;

@SuppressWarnings("serial")
public class UserFormPanel<USER extends User<?>> extends Panel {
    public static final int MIN_BIO_LENGTH = 300;

    private UserService<USER> service;
    private Class<? extends Page> redirectPage;
    private UserEditedCallback<USER> callback = new UserEditedCallback<USER>();

    public UserFormPanel(UserService<USER> service, USER user, Class<? extends Page> redirectPage) {
        super("userForm");
        this.service = service;
        this.redirectPage = redirectPage;
        add(new UserForm(user));
    }

    public UserFormPanel(UserService<USER> service, USER user, Class<? extends Page> redirectPage, UserEditedCallback<USER> callback) {
        this(service, user, redirectPage);
        this.callback = callback;
    }

    public static <T extends User<?>> UserFormPanel<T> newUserFormPanel(UserService<T> service, T user,
                                                                        Class<? extends Page> redirectPage) {
        return new UserFormPanel<T>(service, user, redirectPage);
    }

    public static <T extends User<?>> UserFormPanel<T> newUserFormPanel(UserService<T> service, T user,
                                                                        Class<? extends Page> redirectPage, UserEditedCallback<T> callback) {
        return new UserFormPanel<T>(service, user, redirectPage, callback);
    }

    protected void addAdditionalComponentsToForm(Form components, User speaker) {
        //callback method for subclasses
    }

    private final class UserForm extends Form<Void> {

        private FileUploadField fileUploadField = new FileUploadField("photoUpload");
        private USER user;

        public UserForm(USER user) {
            super("form");
            this.user = user;
            add(new ConfituraFeedbackPanel("feedback"));
            add(i18nLabel("firstName.label"),
                    withLabelKey("firstName.label", textField("firstName", propertyModel(user, "firstName"), true)));
            add(i18nLabel("lastName.label"), withLabelKey("lastName.label", textField("lastName", propertyModel(user, "lastName"), true)));
            add(i18nLabel("email.label"), withLabelKey("email.label", textField("mail", Models.<String>propertyModel(user, "mail"), true)
                    .add(RfcCompliantEmailAddressValidator.getInstance())));
            add(i18nLabel("www.label"), textField("webPage", propertyModel(user, "webPage")));
            add(i18nLabel("twitter.label"), textField("twitter", propertyModel(user, "twitter")));
            RichEditor<Serializable> bioRichEditor = richEditor("bio", propertyModel(user, "bio"));
            add(i18nLabel("about.label"), withLabelKey("about.label", bioRichEditor));
            add(i18nLabel("photo.label"), fileUploadField.add(new PhotoValidator()));
            add(i18nLabel("photo.description"));

            add(cancelLinkWithLabel(redirectPage));
            add(i18nLabel("save.label"));
            PasswordPanel passwordPanel = new PasswordPanel("passwordPanel", user);
            add(passwordPanel);
            setMaxSize(Confitura.UPLOADED_PHOTO_MAX_SIZE);

            addAdditionalComponentsToForm(this, user);
        }

        @Override
        protected void onSubmit() {
            try {
                service.save(user, fileUploadField.getFileUpload());
                callback.apply(user);
                setResponsePage(redirectPage);
            } catch (LoginException ex) {
                warn(new ResourceModel(ex.getMessage()).getObject());
            }
        }
    }

}

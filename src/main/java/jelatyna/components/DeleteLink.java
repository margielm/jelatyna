package jelatyna.components;

import jelatyna.components.nogeneric.Link;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.model.ResourceModel;

@SuppressWarnings("serial")
public abstract class DeleteLink extends Link {

    public DeleteLink(String id) {
        super(id);
        add(AttributeModifier.replace("onclick", "return confirm('" + getMessage() + "');"));
    }

    private String getMessage() {
        return new ResourceModel("delete.confirmation.label").getObject();
    }
}
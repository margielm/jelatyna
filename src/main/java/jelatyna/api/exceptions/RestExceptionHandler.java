package jelatyna.api.exceptions;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletResponse;

@ControllerAdvice
public class RestExceptionHandler {


    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseBody
    public String handleEntityNotFoundException(Exception ex, WebRequest request, HttpServletResponse response) {
        response.setHeader("Content-Type", "text/plain");
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        return ex.getMessage();
    }


    @ExceptionHandler(VoiceInvalidException.class)
    @ResponseBody
    public String handleVoiceInvalidException(Exception ex, WebRequest request, HttpServletResponse response) {
        response.setHeader("Content-Type", "text/plain");
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        return ex.getMessage();
    }
}

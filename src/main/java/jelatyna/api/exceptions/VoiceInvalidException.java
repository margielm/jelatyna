package jelatyna.api.exceptions;


public class VoiceInvalidException extends RuntimeException {
    public VoiceInvalidException() {
        super("Voice has illegal value");
    }
}

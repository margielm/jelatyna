package jelatyna.api;

import static java.lang.String.*;

import java.util.Map;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.ImmutableMap;

import jelatyna.domain.Participant;
import jelatyna.domain.RegistrationStatus;
import jelatyna.domain.dto.ParticipantDTO;
import jelatyna.domain.dto.converters.PresentationConverter;
import jelatyna.pages.confitura.registration.Registration;

@Controller
public class RegistrationApi {

    @Autowired
    private Registration registration;

    @Autowired
    private PresentationConverter converter;


    @RequestMapping(value = "/register/{token}", method = RequestMethod.GET)
    @ResponseBody
    public ParticipantDTO getByToken(@PathVariable String token) {
        Participant participant = registration.getParticipant(token);
        if (participant != null) {
            ParticipantDTO dto = new ParticipantDTO();
            dto.setId(participant.getId());
            dto.setToken(participant.getToken());
            dto.setFirstName(participant.getFirstName());
            dto.setLastName(participant.getLastName());
            dto.setMail(participant.getMail());
            dto.setStatus(participant.getStatus());
            return dto;
        }else {
            throw new RuntimeException(format("User with token[%s] not found!"));
        }
    }

    @RequestMapping(value = "/register/{token}", method = RequestMethod.POST)
    public ResponseEntity<String> register(@Valid @RequestBody Participant participant) {
        try {
            registration.register(participant);
        } catch (Exception ex) {
            return new ResponseEntity(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/register/confirm/{token}", method = RequestMethod.POST)
    @ResponseBody
    public void register(@PathVariable String token) {
        registration.changeStatusFor(token, RegistrationStatus.CONFIRMED);
    }

    @RequestMapping(value = "/register/final/{token}", method = RequestMethod.POST)
    @ResponseBody
    public void finalConfirmation(@PathVariable String token) {
        registration.changeStatusFor(token, RegistrationStatus.FINAL_CONFIRMED);
    }

    @RequestMapping(value = "/register/cancel/{token}", method = RequestMethod.POST)
    @ResponseBody
    public void cancel(@PathVariable String token) {
        registration.changeStatusFor(token, RegistrationStatus.CANCELED);
    }
    @RequestMapping(value = "/register/status/{token}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getStatus(@PathVariable String token) {
        Participant participant = registration.getParticipant(token);
        return ImmutableMap.of("status", participant.getStatus().getName());
    }

    @ExceptionHandler({ Exception.class })
    @ResponseBody
    public ResponseEntity handleException(Exception ex) {
        return new ResponseEntity(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
package jelatyna.api;

import static com.google.common.collect.Maps.*;
import static java.util.Comparator.*;
import static java.util.Optional.*;
import static java.util.stream.Collectors.*;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import jelatyna.domain.Sponsor;
import jelatyna.domain.SponsorType;
import jelatyna.domain.dto.SponsorDto;
import jelatyna.services.FileService;
import jelatyna.services.SponsorService;

@Controller
public class SponsorApi {
    @Autowired
    private SponsorService sponsorService;

    @Autowired
    private FileService fileService;

    @RequestMapping(value = "sponsors", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, List<SponsorDto>> allSponsors() {
        List<Sponsor> sponsors = sponsorService.findAllSponsors();
        Map<String, List<SponsorDto>> dto = newHashMap();
        for (SponsorType type : sponsorService.findDisplayableSponsorTypes()) {
            dto.put(type.getName(), getSponsorsFor(sponsors, type));
        }
        return dto;
    }

    @RequestMapping(value = "sponsors/{name}", method = RequestMethod.GET)
    @ResponseBody
    public SponsorDto sponsorByName(@PathVariable("name") String name) {
        return ofNullable(sponsorService.findByName(name))
                .map(this::toSponsorDto)
                .orElse(new SponsorDto());
    }

    private List<SponsorDto> getSponsorsFor(List<Sponsor> sponsors, SponsorType type) {
        return sponsors.stream()
                .filter((sponsor) -> sponsor.isOf(type))
                .sorted(comparing(Sponsor::getMoney).reversed())
                .map(this::toSponsorDto)
                .collect(toList());
    }

    private SponsorDto toSponsorDto(Sponsor sponsor) {
        return new SponsorDto(sponsor.getName(), sponsor.getWebPage(), sponsor.getDescription(), fileService.getUrlTo(sponsor));
    }
}

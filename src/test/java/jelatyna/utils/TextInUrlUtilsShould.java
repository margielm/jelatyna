package jelatyna.utils;

import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class TextInUrlUtilsShould {

    @Test
    public void notTouchValidText() {
        // Given
        String text = "validtext";

        // When
        String escapedText = TextInUrlUtils.prettify(text);

        // Then
        assertThat(escapedText).isEqualTo(text);
    }

    @Test
    public void notTouchValidTextWithNumbers() {
        // Given
        String text = "001validtext1212";

        // When
        String escapedText = TextInUrlUtils.prettify(text);

        // Then
        assertThat(escapedText).isEqualTo(text);
    }

    @Test
    public void replaceUpperCaseLettersWithLowerCase() {
        // Given
        String text = "ValidText";

        // When
        String escapedText = TextInUrlUtils.prettify(text);

        // Then
        assertThat(escapedText).isEqualTo(text.toLowerCase());
    }

    @Test
    public void escapeWhiteSpaces() {
        // Given
        String text = "valid text";

        // When
        String escapedText = TextInUrlUtils.prettify(text);

        // Then
        assertThat(escapedText).isEqualTo("valid_text");
    }

    @Test
    public void escapeNonAlphanumerics() {
        // Given
        String text = "!@#$%^&*((((((((())))))=-+Super news!!!!!???...:[]{}<>?/";

        // When
        String escapedText = TextInUrlUtils.prettify(text);

        // Then
        assertThat(escapedText).isEqualTo("super_news");
    }

    @Test
    public void escapePolishSpecificLetters() {
        // Given
        String text = "kółko dąb dość dżdżu źdźbeł śińców";

        // When
        String escapedText = TextInUrlUtils.prettify(text);

        // Then
        assertThat(escapedText).isEqualTo("kolko_dab_dosc_dzdzu_zdzbel_sincow");
    }

    @Test
    public void shouldEscapeRealPhrase() {
        // Given
        String text = "Kolejna odsłona akcji WPA: książka o certyfikacie OCJP 6!";

        // When
        String escapedText = TextInUrlUtils.prettify(text);

        // Then
        assertThat(escapedText).isEqualTo("kolejna_odslona_akcji_wpa_ksiazka_o_certyfikacie_ocjp_6");
    }

}

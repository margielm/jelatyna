package jelatyna.domain;


import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class AbstractEntityShould {
    private AbstractEntity<TestEntity> instance = new TestEntity();

    @Test
    public void setNewId() {
        Integer id = 21;

        AbstractEntity<?> result = instance.id(id);

        assertThat(result.getId()).isEqualTo(21);
    }

    @Test
    public void returnTrueIfIsNotNew() {

        boolean result = instance.isNotNew();

        assertThat(instance.isNew()).isTrue();
        assertThat(result).isFalse();
    }

    @SuppressWarnings("serial")
    public class TestEntity extends AbstractEntity<TestEntity> {
    }
}

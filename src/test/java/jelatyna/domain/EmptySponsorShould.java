package jelatyna.domain;


import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class EmptySponsorShould {

    EmptySponsor instance = new EmptySponsor(SponsorType.EMPTY);

    @Test
    public void returnNullWebPage() {

        String result = instance.getWebPage();

        assertThat(result).isNull();
    }
}

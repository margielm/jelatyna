package jelatyna.domain.dto.converters;

import jelatyna.domain.Speaker;
import jelatyna.domain.dto.UserDto;
import jelatyna.services.FileService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserConverterTest {
    @Mock
    private FileService service;
    @InjectMocks
    private UserConverter converter;

    @Test
    public void map_user() {
        when(service.getUrlTo(any(Speaker.class))).thenReturn("photo");
        Speaker speaker = new Speaker().firstName("Jan").lastName("Kowalski").bio("bio").twitter("twitter").webPage("web");

        UserDto dto = converter.map(speaker);

        assertThat(dto.getFirstName()).isEqualTo("Jan");
        assertThat(dto.getLastName()).isEqualTo("Kowalski");
        assertThat(dto.getBio()).isEqualTo("bio");
        assertThat(dto.getTwitter()).isEqualTo("twitter");
        assertThat(dto.getUrl()).isEqualTo("web");
        assertThat(dto.getPhoto()).isEqualTo("photo");


    }
}
package jelatyna.domain;

import com.googlecode.catchexception.CatchException;
import com.googlecode.catchexception.apis.CatchExceptionBdd;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static jelatyna.domain.RegistrationStatus.*;
import static junitparams.JUnitParamsRunner.*;
import static org.assertj.core.api.Assertions.*;

@RunWith(JUnitParamsRunner.class)
public class ParticipantShould {
    private Participant participant = new Participant();

    @Test
    public void changeStatusFromConfirmedToFinalConfirmed() {
        participant.registrationType = CONFIRMED;

        participant.status(FINAL_CONFIRMED);

        assertThat(participant.getStatus()).isEqualTo(FINAL_CONFIRMED);
    }

    @Test
    @Parameters(method = "newAndCanceled")
    public void throwExceptionOnChangingStatusToFinalConfirmedFromTo(RegistrationStatus status) {
        participant.registrationType = status;

        CatchExceptionBdd.when(participant).status(FINAL_CONFIRMED);

        CatchExceptionBdd.then(CatchException.caughtException())
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Status Twojej rejestracji nie pozwala na tę operację.");
    }

    @Test
    public void changeStatusFromNewToConfirmed() {
        participant.registrationType = NEW;

        participant.status(CONFIRMED);

        assertThat(participant.getStatus()).isEqualTo(CONFIRMED);
    }

    @Test
    @Parameters(method = "finalConfirmedAndCanceled")
    public void throwExceptionOnChangingStatusToConfirmedFrom(RegistrationStatus status) {
        participant.registrationType = status;

        CatchExceptionBdd.when(participant).status(CONFIRMED);

        CatchExceptionBdd.then(CatchException.caughtException())
                .isInstanceOf(RuntimeException.class)
                .hasMessage(Participant.ILLEGAL_STATUS);
    }

    @Test
    @Parameters(method = "all")
    public void changeStatusToCanceledFromAny(RegistrationStatus status) {
        participant.registrationType = status;

        participant.status(CANCELED);

        assertThat(participant.getStatus()).isEqualTo(CANCELED);
    }

    @Test
    public void getMaterialsIfFinalStatusAndMailingIsSelected() {
        participant.registrationType = FINAL_CONFIRMED;
        participant.mailing(true);

        boolean shouldGetMaterials = participant.shouldGetMaterials();

        assertThat(shouldGetMaterials).isTrue();
    }

    @Test
    @Parameters(method = "notFinal")
    public void notGetMaterialsIfMailingIsSelectedButStatusNotFinal(RegistrationStatus status) {
        participant.registrationType = status;
        participant.mailing(true);

        boolean shouldGetMaterials = participant.shouldGetMaterials();

        assertThat(shouldGetMaterials).isFalse();
    }

    @SuppressWarnings("unused")
    private Object[] newAndCanceled() {
        return $(NEW, CANCELED);
    }

    @SuppressWarnings("unused")
    private Object[] finalConfirmedAndCanceled() {
        return $(FINAL_CONFIRMED, CANCELED);
    }

    @SuppressWarnings("unused")
    private Object[] all() {
        return RegistrationStatus.values();
    }

    @SuppressWarnings("unused")
    private Object[] notFinal() {
        return $(NEW, CONFIRMED, CANCELED);
    }

}

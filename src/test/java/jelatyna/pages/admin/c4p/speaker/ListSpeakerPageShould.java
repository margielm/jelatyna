package jelatyna.pages.admin.c4p.speaker;

import jelatyna.ConfituraSession;
import jelatyna.JelatynaTagTester;
import jelatyna.WicketTest;
import jelatyna.domain.Admin;
import jelatyna.domain.Speaker;
import jelatyna.repositories.SpeakerRepository;
import jelatyna.services.AdminService;
import org.apache.wicket.util.tester.TagTester;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Locale;

import static com.google.common.collect.Lists.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ListSpeakerPageShould extends WicketTest {
    @Mock
    private SpeakerRepository repository;
    @Mock
    private AdminService adminService;

    @Override
    public void setupServices() {
        Locale.setDefault(new Locale("pl"));
        ConfituraSession.get().setUser(new Admin());
        put(repository, adminService);
    }

    @Test
    public void dislaySpeakers() {
        List<Speaker> speakers = newArrayList(speaker(0), speaker(1));
        when(repository.findAll()).thenReturn(speakers);

        tester.startPage(ListSpeakerPage.class);

        List<TagTester> rows = tester.getTagsByWicketId("rows");
        assertRowContainsSpeaker(0, rows, speakers);
        assertRowContainsSpeaker(1, rows, speakers);
    }

    private void assertRowContainsSpeaker(int idx, List<TagTester> rows, List<Speaker> speakers) {
        JelatynaTagTester row = new JelatynaTagTester(rows.get(idx));
        assertThat(row.getChildByWicketId("idx").getValue()).isEqualTo("" + (idx + 1));
        assertThat(row.getChildByWicketId("first_name").getValue()).isEqualTo(speakers.get(idx).getFirstName());
        assertThat(row.getChildByWicketId("last_name").getValue()).isEqualTo(speakers.get(idx).getLastName());
    }

    private Speaker speaker(int id) {
        return new Speaker(id).firstName("Imie " + id).lastName("Nazwisko " + id);
    }
}

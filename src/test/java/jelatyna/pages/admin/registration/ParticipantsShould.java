package jelatyna.pages.admin.registration;

import jelatyna.domain.Participant;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static com.google.common.collect.Lists.*;
import static jelatyna.TestUtils.*;
import static org.assertj.core.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
public class ParticipantsShould {

    @Test
    public void returnWithMailingOnly() {
        Participant participant1 = participant(1).mailing(true);
        Participant participant2 = participant(2).mailing(true);
        Participant participant3 = participant(3).mailing(false);
        Participants participants = participants(participant1, participant2, participant3);

        Participants withMailing = participants.limitToWithMailing(true);

        assertThat(withMailing.asList()).containsOnly(participant1, participant2);
    }

    @Test
    public void returnParticipantedOnly() {
        Participant participant1 = participant(1).participated(true);
        Participant participant2 = participant(2).participated(false);
        Participant participant3 = participant(3).participated(true);
        Participants participants = participants(participant1, participant2, participant3);

        Participants participatedOnly = participants.limitToParticipated(true);

        assertThat(participatedOnly.asList()).containsOnly(participant1, participant3);
    }

    @Test
    public void returnParticipatedAndWithMailing() {
        Participant participant1 = participant(1).participated(true).mailing(true);
        Participant participant2 = participant(2).participated(false).mailing(true);
        Participant participant3 = participant(3).participated(true).mailing(false);
        Participants participants = participants(participant1, participant2, participant3);

        Participants filtered = participants.limitToParticipated(true).limitToWithMailing(true);

        assertThat(filtered.asList()).containsOnly(participant1);
    }

    @Test
    public void notLimitParticipants() {
        Participant participant1 = participant(1).participated(true).mailing(true);
        Participant participant2 = participant(2).participated(false).mailing(true);
        Participant participant3 = participant(3).participated(true).mailing(false);
        Participants participants = participants(participant1, participant2, participant3);

        Participants filtered = participants.limitToParticipated(false).limitToWithMailing(false);

        assertThat(filtered.asList()).isEqualTo(participants.asList());
    }

    private Participants participants(Participant... participants) {
        return new Participants(newArrayList(participants));
    }

}

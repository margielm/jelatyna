package jelatyna.pages.admin.lottery;

import jelatyna.WicketTest;
import jelatyna.domain.Participant;
import jelatyna.services.AdminService;
import jelatyna.services.LotteryService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static jelatyna.TestUtils.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class LotteryPageShould extends WicketTest {
    Participant participant = participant("Jan", "Kowalski").mail("jan@kowalski.pl");
    @Mock
    private LotteryService service;

    @Override
    public void setupServices() {
        withAdmin();
        put(service, mock(AdminService.class));
        when(service.draw()).thenReturn(participant);
    }

    @Test
    public void notDrawParticipantOnStartingPage() {

        tester.startPage(LotteryPage.class);

        assertNameShownFor(Participant.EMPTY);
        verify(service, never()).draw();
    }

    @Test
    public void drawParticipantAndShowFirstAndLastNameOnClickingDrawButton() {
        tester.startPage(LotteryPage.class);

        tester.clickLink("draw");

        assertNameShownFor(participant);
        verify(service).draw();
    }

    @Test
    public void clearAllDrawingsOnClickingButton() {
        tester.startPage(LotteryPage.class);

        tester.clickLink("reset");

        verify(service).clear();
    }

    private void assertNameShownFor(Participant participant) {
        tester.assertLabel("form:firstName", participant.getFirstName());
        tester.assertLabel("form:lastName", participant.getLastName());
        tester.assertLabel("form:mail", participant.getMail());
    }
}

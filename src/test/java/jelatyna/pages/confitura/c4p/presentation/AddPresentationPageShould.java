package jelatyna.pages.confitura.c4p.presentation;

import jelatyna.WicketTest;
import jelatyna.domain.Presentation;
import jelatyna.domain.Speaker;
import jelatyna.pages.PageNotFound;
import jelatyna.pages.confitura.c4p.ViewSpeakerPage;
import jelatyna.services.FileService;
import jelatyna.services.PresentationService;
import jelatyna.services.SpeakerService;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.util.tester.FormTester;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static jelatyna.assertions.WicketAssertions.assertThat;
import static jelatyna.assertions.WicketAssertions.*;
import static jelatyna.utils.PageParametersBuilder.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AddPresentationPageShould extends WicketTest {
    private static final String TITLE_ID = "title";
    private static final String DESCRIPTION_ID = "description";
    private static final int PRESENTATION_ID = 1;
    private Speaker speaker = new Speaker(10);
    @Mock
    private PresentationService presentationService;
    @Mock
    private SpeakerService service;

    @Override
    public void setupServices() {
        when(service.isCall4PapersActive()).thenReturn(true);
        put(presentationService, service, mock(FileService.class));
        withUser(speaker);
    }

    @Test
    public void showPresentation() {
        Presentation presentation = new Presentation().owner(speaker).title("My item")
                .description("My description");
        mockRepositoryWith(presentation);

        tester.startPage(AddPresentationPage.class, paramsFor("id", PRESENTATION_ID));

        assertThat(form(tester, "item:form"))
                .hasValue(TITLE_ID, presentation.getTitle())
                .hasValue(DESCRIPTION_ID, presentation.getDescription());
    }

    @Test
    public void redirectToErrorPageIfEditingPresentationOfDifferentSpeaker() {
        mockRepositoryWith(new Presentation().owner(new Speaker(100)));

        tester.startPage(AddPresentationPage.class, paramsFor("id", PRESENTATION_ID));

        tester.assertRenderedPage(PageNotFound.class);
    }

    @Test
    public void saveNewPresentation() {
        mockSpeakerService();
        tester.startPage(AddPresentationPage.class);
        FormTester formTester = formTester()
                .setValue(TITLE_ID, "new title")
                .setValue(DESCRIPTION_ID, "new description");

        formTester.submit();

        tester.assertRenderedPage(ViewSpeakerPage.class);
        ArgumentCaptor<Presentation> captor = ArgumentCaptor.forClass(Presentation.class);
        verify(presentationService).save(captor.capture(), any(FileUpload.class));
        assertThat(captor.getValue().getTitle()).isEqualTo("new title");
        assertThat(captor.getValue().getDescription()).isEqualTo("new description");
    }

    @Test
    public void redirectIfAddingPresentationWhenCall4PapersIsNotActive() {
        when(service.isCall4PapersActive()).thenReturn(false);

        tester.startPage(AddPresentationPage.class);

        tester.assertRenderedPage(PageNotFound.class);
    }

    private FormTester formTester() {
        return tester.newFormTester("item:form");
    }

    private void mockSpeakerService() {
        when(service.findById(anyInt())).thenReturn(speaker);
    }

    private void mockRepositoryWith(Presentation presentation) {
        when(presentationService.findBy(PRESENTATION_ID)).thenReturn(presentation);
    }
}

package jelatyna.pages.confitura.c4p.voting;

import jelatyna.WicketTest;
import jelatyna.components.user.UserInfoPanel;
import jelatyna.domain.Presentation;
import jelatyna.domain.Speaker;
import jelatyna.pages.PageNotFound;
import jelatyna.services.FileService;
import jelatyna.services.SpeakerService;
import org.apache.wicket.util.tester.FormTester;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static jelatyna.assertions.WicketAssertions.*;
import static jelatyna.utils.PageParametersBuilder.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class VotingPageShould extends WicketTest {
    private Speaker speaker = new Speaker().firstName("Jan").lastName("Kowalski");
    @Mock
    private Voting voting;

    @Override
    public void setupServices() {
        when(voting.isActive()).thenReturn(true);
        put(voting, mock(FileService.class), mock(SpeakerService.class));
    }

    @Test
    public void displayFirstPresentationAndSpeaker() {
        Presentation presentation = presentation("title", "description");
        mockVotingWith(presentation, presentation("", ""));

        startPage(0);

        assertPresentationDisplayed(presentation);
        assertSpeakerDisplayed(speaker);
    }

    @Test
    public void moveSaveVoteAndMoveToNextPresentation() {
        Presentation presentation1 = presentation("", "");
        Presentation presentation2 = presentation("title", "description");
        mockVotingWith(presentation1, presentation2);
        startPage(0);
        FormTester formTester = ratePresentation();

        formTester.submit();

        assertPresentationDisplayed(presentation2);
        assertSpeakerDisplayed(speaker);
        verifyVoteSaved(presentation1, 1);
    }

    @Test
    public void showNotSaveVoteIfPresentationNotRated() {
        Presentation presentation = presentation("2", "2");
        mockVotingWith(presentation("1", "1"), presentation);
        startPage(0);
        FormTester formTester = tester.newFormTester("form");

        formTester.submit();

        verify(voting, never()).save(any(Presentation.class), anyInt());
        assertPresentationDisplayed(presentation);
    }

    @Test
    public void redirectToFinshPageOnSubmittingVoteForLast() {
        Presentation presentation = presentation("1", "1");
        mockVotingWith(presentation);
        lastPresentation();
        startPage(0);
        FormTester formTester = ratePresentation();

        formTester.submit();

        verifyVoteSaved(presentation, 1);
        tester.assertRenderedPage(VotingFinishedPage.class);
    }

    @Test
    public void redirectToPageNotFoundIfVotingNotActive() {
        when(voting.isActive()).thenReturn(false);
        mockVotingWith(presentation("", ""));

        startPage(0);

        tester.assertRenderedPage(PageNotFound.class);
    }

    private void lastPresentation() {
        when(voting.isLast(0)).thenReturn(true);
    }

    private void startPage(int pageIdx) {
        tester.startPage(VotingPage.class, paramsFor("id", pageIdx));
    }

    private void verifyVoteSaved(Presentation presentation, int rate) {
        verify(voting).save(presentation, rate);
    }

    private Presentation presentation(String title, String description) {
        return new Presentation().title(title).description(description).owner(speaker);
    }

    private void assertSpeakerDisplayed(Speaker speaker) {
        assertThat(component(tester, "speaker")).isInstanceOf(UserInfoPanel.class);
        assertThat(label(tester, "speaker:first_name")).hasContent(speaker.getFirstName());
        assertThat(label(tester, "speaker:last_name")).hasContent(speaker.getLastName());
    }

    private void assertPresentationDisplayed(Presentation presentation) {
        assertThat(tag(tester, "title")).hasContent(presentation.getTitle());
        assertThat(tag(tester, "full-desc")).hasContent(presentation.getDescription());
    }

    private void mockVotingWith(Presentation... presentations) {
        for (int idx = 0; idx < presentations.length; idx++) {
            when(voting.getPresentation(idx)).thenReturn(presentations[idx]);
        }
    }

    private FormTester ratePresentation() {
        FormTester formTester = tester.newFormTester("form");
        formTester.select("votes", 0);
        return formTester;
    }
}

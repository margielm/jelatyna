package jelatyna.pages.confitura.presentation;

import jelatyna.WicketIntegrationTest;
import jelatyna.components.user.speaker.ViewPresentationPanel;
import jelatyna.domain.Presentation;
import jelatyna.domain.Speaker;
import org.junit.Test;


public class PresentationPageShould extends WicketIntegrationTest {

    @Test
    public void redirectLinkShouldBeEnabledWhenSpeakerIsAccepted() {
        tester.startComponentInPage(ViewPresentationPanel.createPanelWithLinkToSpeaker(presentation(Boolean.TRUE)));

        tester.assertEnabled("presentation_panel:speaker");
    }

    @Test
    public void redirectLinkShouldBeDisabledWhenSpeakerIsNotAccepted() {
        tester.startComponentInPage(ViewPresentationPanel.createPanelWithLinkToSpeaker(presentation(Boolean.FALSE)));

        tester.assertDisabled("presentation_panel:speaker");
    }

    public Presentation presentation(boolean speakerIsAccepted) {
        return new Presentation().accepted(true).title("Test title").description("description")
                .owner(new Speaker()
                        .accepted(speakerIsAccepted)
                        .firstName("Jan")
                        .lastName("testowy"));
    }

}

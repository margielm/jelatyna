package jelatyna.services;

import jelatyna.domain.Draw;
import jelatyna.domain.Participant;
import jelatyna.domain.RegistrationStatus;
import jelatyna.pages.admin.registration.Participants;
import jelatyna.repositories.DrawRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.google.common.collect.Lists.*;
import static jelatyna.TestUtils.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyCollectionOf;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class LotteryServiceShould {
    @Mock
    private ParticipantService participantService;
    @Mock
    private RandomGenerator randomGenerator;
    @Mock
    private DrawRepository drawRepository;
    @InjectMocks
    private LotteryService service = new LotteryService();

    @Test
    public void drawParticipant() {
        withParticipants(participant(1), participant(2));
        when(randomGenerator.getRandomIdx(anyList())).thenReturn(1);

        Participant participant = service.draw();

        assertThat(participant).isEqualTo(participant(2));
        verify(drawRepository).save(new Draw(participant(2)));
    }

    @Test
    public void drawParticipantAmongNotDrownYet() {
        withParticipants(participant(1), participant(2), participant(3));
        when(randomGenerator.getRandomIdx(anyList())).thenReturn(1);
        when(drawRepository.findAll()).thenReturn(newArrayList(new Draw(participant(2))));

        Participant participant = service.draw();

        assertThat(participant).isEqualTo(participant(3));
        verify(drawRepository).save(new Draw(participant(3)));
    }

    @Test
    public void returnEmptyParticipantIfParticipatedListEmpty() {
        withParticipants();

        Participant participant = service.draw();

        assertThat(participant).isEqualTo(Participant.EMPTY);
    }

    @Test
    public void clearDrawHistory() {

        service.clear();

        verify(drawRepository).deleteAllInBatch();
    }

    private void withParticipants(Participant... participant) {
        Participants participants = participants(participant);
        when(participantService.findByStatus(anyCollectionOf(RegistrationStatus.class))).thenReturn(participants);
    }

    private Participants participants(Participant... participant) {
        Participants participants = mock(Participants.class);
        when(participants.limitToParticipated(anyBoolean())).thenReturn(participants);
        when(participants.limitToWithMailing(anyBoolean())).thenReturn(participants);
        when(participants.asList()).thenReturn(newArrayList(participant));
        return participants;
    }
}

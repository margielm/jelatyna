package jelatyna.services;

import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class RandomGeneratorTest {

    RandomGenerator generator = new RandomGenerator();


    @Test
    public void shouldRemovePlusSigns() {
        // Given
        String token = "token+token+aaaa";

        // When
        String safeToken = generator.replaceUrlUnfriendlyChars(token);

        // Then
        assertThat(safeToken).doesNotContain("+");
    }

    @Test
    public void shouldReplaceSlashes() {
        // Given
        String token = "token/token/aaaa";

        // When
        String safeToken = generator.replaceUrlUnfriendlyChars(token);

        // Then
        assertThat(safeToken).doesNotContain("/");
    }


}

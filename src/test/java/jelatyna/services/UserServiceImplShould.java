package jelatyna.services;

import static com.googlecode.catchexception.CatchException.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.jasypt.util.password.PasswordEncryptor;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.Mock;

import com.googlecode.catchexception.apis.CatchExceptionBdd;

import jelatyna.domain.User;
import jelatyna.repositories.UserRepository;
import jelatyna.services.exceptions.LoginException;

public abstract class UserServiceImplShould<T extends User<?>> {
    @Mock
    FileUpload fileUpload;
    @Mock
    FileService fileService;
    @Mock
    PasswordEncryptor encryptor;
    @Captor
    private ArgumentCaptor<T> captor;

    @Test
    public void encryptPasswordBeforeSavingNewSpeaker() {
        T user = spy((T) createUser().plainPassword("abc"));
        when(user.isNew()).thenReturn(true);

        getService().save(user, fileUpload);

        UserRepository<T> repository = getRepository();
        InOrder inOrder = inOrder(encryptor, repository);
        inOrder.verify(encryptor).encryptPassword("abc");
        inOrder.verify(repository).save(user);
    }

    @Test
    public void saveFileOnSavingSpeaker() {
        T user = createUser();

        getService().save(user, fileUpload);

        verify(fileService).save(fileUpload, user);
    }

    @Test
    public void notSaveFileIfIsNull() {
        T user = createUser();

        getService().save(user, null);

        verifyZeroInteractions(fileService);
    }

    @Test
    public void throwLoginExceptionWhenUserWithGivenMailExistsOnSavingNewSpeaker() {
        T speaker = spy(createUser());
        when(speaker.isNew()).thenReturn(true);
        when(speaker.getMail()).thenReturn("my@mail.pl");
        when(getRepository().findByMail(anyString())).thenReturn(createUser());

        catchException(getService()).save(speaker, fileUpload);

        CatchExceptionBdd.then(caughtException())
                .isInstanceOf(LoginException.class)
                .hasMessage(SpeakerServiceImpl.MAIL_NOT_UNIQUE);
//        verifyZeroInteractions(speaker);
        verifyZeroInteractions(fileService);
    }

    @Test
    public void loginUserIfMailExistsAndPasswordIsValid() {
        // given
        T user = userWithValidPassword(true);
        mockRepositoryToReturn(user);

        // when
        T loggedUser = getService().loginWith(user.getMail(), "password");

        // then
        assertThat(loggedUser).isSameAs(user);
    }

    @Test
    public void throwLoginExceptionIfUserDoesNotExist() {
        // given
        mockRepositoryToReturn(null);

        // when
        catchException(getService()).loginWith("not@existing.pl", "password");

        // then
        CatchExceptionBdd.then(caughtException())
                .isInstanceOf(LoginException.class)
                .hasMessage(AdminServiceImpl.ACCESS_DENIED);
    }

//    @Test
//    @Ignore
//    public void throwLoginExceptionIfPasswordNotValid() {
//        // given
//        T user = userWithValidPassword(false);
//        mockRepositoryToReturn(user);
//
//        // when
//        catchException(getService()).loginWith(user.getMail(), "incorrect-password");
//
//        // then
//        CatchExceptionBdd.then(caughtException())
//                .isInstanceOf(LoginException.class)
//                .hasMessage(AdminServiceImpl.ACCESS_DENIED);
//    }

    @Test
    public abstract void saveOnlyIfSavingExistingUser();

    @Test
    public void changePasswordIfOldOneIsCorrect() {
        String newPassword = "newPassword";
        T user = spy(createUser());

        when(encryptor.checkPassword(anyString(), anyString())).thenReturn(true);

        getService().changePassword(user, "correctPassword", newPassword);

        UserRepository<T> repository = getRepository();
        InOrder inOrder = inOrder(user, repository);
        inOrder.verify(repository).save(user);
    }

    @Test
    public void removeAllJavaScriptsFromBioOnSavingUser() {
        T user = createUser();
        user.bio("<script>aaa</script><p>bio</p><script type=\"text/javascript\">aaa</script>");

        getService().save(user, fileUpload);

        UserRepository<T> repository = getRepository();
        verify(repository).save(captor.capture());
        assertThat(captor.getValue().getBio()).isEqualTo("<p>bio</p>");

    }

    private T userWithValidPassword(boolean validPassword) {
        T user = createUser();
        user.mail("my@mail.pl");
        when(encryptor.checkPassword(anyString(), anyString())).thenReturn(validPassword);
        return user;
    }

    void mockRepositoryToReturn(T user) {
        when(getRepository().count()).thenReturn(1L);
        when(getRepository().findByMail(anyString())).thenReturn(user);
    }

    abstract protected UserService<T> getService();

    abstract protected <REPO extends UserRepository<T>> REPO getRepository();

    abstract protected T createUser();
}
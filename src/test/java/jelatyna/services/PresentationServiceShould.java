package jelatyna.services;


import jelatyna.domain.Presentation;
import jelatyna.repositories.PresentationRepository;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PresentationServiceShould {
    private static final int ID = 1;
    private Presentation saved = new Presentation().id(ID);
    private String fileName = "name.pdf";
    @Captor
    private ArgumentCaptor<Presentation> captor;
    @Mock
    private PresentationRepository repository;
    @Mock
    private FileService fileService;
    @Mock
    private FileUpload fileUpload;
    @InjectMocks
    private PresentationService service = new PresentationServiceImpl();
    private Presentation presentation = new Presentation();

    @Test
    public void removeJavaScriptFromPresentationDescriptionOnSaving() {
        presentation.description("<script>aaa</script><p>bio</p><script type=\"text/javascript\">aaa</script>");
        mockRepositoryToReturnPresentationWithId();

        service.save(presentation, null);

        verify(repository).save(captor.capture());
        assertThat(captor.getValue().getDescription()).isEqualTo("<p>bio</p>");
    }

    @Test
    public void saveFileAfterSavingPresentationIfNotNull() {
        when(fileUpload.getClientFileName()).thenReturn(fileName);
        mockRepositoryToReturnPresentationWithId();

        service.save(presentation, fileUpload);

        InOrder inOrder = inOrder(repository, fileService);
        inOrder.verify(repository).save(captor.capture());
        inOrder.verify(fileService).save(fileUpload, saved);
        assertThat(captor.getValue().getFileName()).isEqualTo(fileName);
    }

    @Test
    public void notClearFileNameIfFileUpladNull() throws Exception {
        presentation.fileName(fileName);

        service.save(presentation, null);

        verify(repository).save(captor.capture());
        assertThat(captor.getValue().getFileName()).isEqualTo(fileName);
    }

    @Test
    public void callFindAllAcceptedRepositoryMethod() {

        service.acceptedPresentations();

        verify(repository, times(1)).findAllAccepted();
    }

    @Test
    public void shouldReturnAcceptedPresentations() {
        Presentation presentation1 = acceptedPresentation(1);
        Presentation presentation2 = acceptedPresentation(2);
        when(repository.findAllAccepted()).thenReturn(Arrays.asList(presentation1, presentation2));

        List<Presentation> result = service.acceptedPresentations();

        assertThat(result).containsExactly(presentation1, presentation2);
    }

    private Presentation acceptedPresentation(Integer id) {
        return new Presentation().id(id).accepted(true);
    }


    private void mockRepositoryToReturnPresentationWithId() {

        when(repository.save(any(Presentation.class))).thenReturn(saved);
    }
}

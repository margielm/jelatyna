package jelatyna.services;

import jelatyna.domain.RegistrationConfiguration;
import jelatyna.repositories.ParticipantRepository;
import jelatyna.repositories.RegistrationConfigurationRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.google.common.collect.Lists.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RegistrationServiceShould {
    private RegistrationConfiguration registration = new RegistrationConfiguration();
    @Mock
    private RegistrationConfigurationRepository registrationRepository;
    @Mock
    private ParticipantRepository participantRepository;
    @InjectMocks
    private RegistrationService service;

    @Test
    public void getEmptyStringIfNoRegistrationAvailable() {
        when(registrationRepository.count()).thenReturn(0L);

        String widgetInfo = service.getWidgetInfo();

        assertThat(widgetInfo).isEmpty();
    }

    @Test
    public void getEmptyStringIfRegistrationAvailableButNoInfoDeclared() {
        mockRepositoryToReturnRegistration();

        String widgetInfo = service.getWidgetInfo();

        assertThat(widgetInfo).isEmpty();
    }

    @Test
    public void getPlainInfoIfDeclared() {
        widgetInfo("INFO");
        mockRepositoryToReturnRegistration();

        String widgetInfo = service.getWidgetInfo();

        assertThat(widgetInfo).isEqualTo(widgetInfo);
    }

    @Test
    public void replaceCounterMarkerWithNumberOfParticipants() {
        widgetInfo("We have $counter participants!");
        mockRepositoryToReturnRegistration();
        when(participantRepository.count()).thenReturn(10L);

        String widgetInfo = service.getWidgetInfo();

        assertThat(widgetInfo).isEqualTo("We have 10 participants!");

    }

    private void mockRepositoryToReturnRegistration() {
        when(registrationRepository.count()).thenReturn(1L);
        when(registrationRepository.findAll()).thenReturn(newArrayList(registration));
    }

    private RegistrationConfiguration widgetInfo(String widgetInfo) {
        return registration.widgetInfo(widgetInfo);
    }
}

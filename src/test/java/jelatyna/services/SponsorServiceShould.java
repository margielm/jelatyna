package jelatyna.services;

import jelatyna.domain.Sponsor;
import jelatyna.domain.SponsorType;
import jelatyna.repositories.SponsorRepository;
import jelatyna.repositories.SponsorTypeRepository;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.collect.Lists.*;
import static jelatyna.TestUtils.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class SponsorServiceShould {
    private SponsorType sponsorType1 = sponsorType(0, 0);
    private SponsorType sponsorType2 = sponsorType(1, 10);
    private SponsorType sponsorType3 = sponsorType(2, 11);

    @Mock
    private SponsorTypeRepository sponsorTypeRepository;
    @Mock
    private SponsorRepository sponsorRepository;
    @Mock
    private FileService fileService;
    @InjectMocks
    private SponsorService service = new SponsorService();

    @Test
    public void switchSponsorTypesOnMovingUp() {
        mockRepositoryToHave(sponsorType1, sponsorType2);

        service.moveUp(sponsorType2);

        verifyPositionChangedTo(sponsorType2, 0);
        verifyPositionChangedTo(sponsorType1, 10);
    }

    @Test
    public void moveUpThirdSponsorType() {
        mockRepositoryToHave(sponsorType1, sponsorType2, sponsorType3);

        service.moveUp(sponsorType3);

        verify(sponsorTypeRepository).findAllSorted();
        verifyPositionChangedTo(sponsorType3, 10);
        verifyPositionChangedTo(sponsorType2, 11);
        verifyNoMoreInteractions(sponsorTypeRepository);
    }

    @Test
    public void notMoveUpIfMovingFirstSponsorType() {
        mockRepositoryToHave(sponsorType1, sponsorType2);

        service.moveUp(sponsorType1);

        verifyZeroInteractions(sponsorTypeRepository);
    }

    @Test
    public void switchSponsorTypesOnMovingDown() {
        mockRepositoryToHave(sponsorType1, sponsorType2);

        service.moveDown(sponsorType1);

        verifyPositionChangedTo(sponsorType2, 0);
        verifyPositionChangedTo(sponsorType1, 10);
    }

    @Test
    public void moveDownSecondSponsorType() {
        mockRepositoryToHave(sponsorType1, sponsorType2, sponsorType3);

        service.moveDown(sponsorType2);

        verify(sponsorTypeRepository).findAllSorted();
        verifyPositionChangedTo(sponsorType3, 10);
        verifyPositionChangedTo(sponsorType2, 11);
        verifyNoMoreInteractions(sponsorTypeRepository);
    }

    @Test
    public void notMoveDownIfMovingLastSponsorType() {
        mockRepositoryToHave(sponsorType1, sponsorType2);

        service.moveDown(sponsorType2);

        verify(sponsorTypeRepository, never()).save(any(SponsorType.class));
    }

    @Test
    public void setZeroPositionOnSavingFirstSponsorType() {
        SponsorType sponsorType = new SponsorType();

        service.save(sponsorType);

        assertThat(sponsorType.getPosition()).isEqualTo(0);
    }

    @Test
    public void setPositionBigerThenLastSponsorTypeOnAdding() {
        mockRepositoryToHave(sponsorType1, sponsorType2);
        SponsorType sponsorType = new SponsorType();

        service.save(sponsorType);

        assertThat(sponsorType.getPosition()).isEqualTo(sponsorType2.getPosition() + 1);
    }

    @Test
    public void disallowToRemoveTypeifAnySponsorsInThisType() {
        withSponsor(sponsorType1);

        boolean canRemove = service.canRemove(sponsorType1);

        assertThat(canRemove).isFalse();
    }

    @Test
    public void allowToRemoveTypeifNonSponsorsInThisType() {
        when(sponsorRepository.findByType(sponsorType1)).thenReturn(new ArrayList<Sponsor>());

        boolean canRemove = service.canRemove(sponsorType1);

        assertThat(canRemove).isTrue();
    }

    @Test
    public void getSponsorTypesWithAnySponsor() {
        mockRepositoryToHave(withSponsor(sponsorType1), sponsorType2);

        List<SponsorType> displayableTypes = service.findDisplayableSponsorTypes();

        assertThat(displayableTypes).containsOnly(sponsorType1);
    }

    @Test
    public void getSponsorTypesWithoutSponsorsButWithMinimalNoBiggerThenZero() {
        sponsorType2.minimalNo(1);
        mockRepositoryToHave(sponsorType1, sponsorType2);

        List<SponsorType> displayableTypes = service.findDisplayableSponsorTypes();

        assertThat(displayableTypes).containsOnly(sponsorType2);
    }

    @Test
    public void getSponsorsSupplementedWithPlaceHoldersIfLessThenMinimalNo() {
        sponsorType1.minimalNo(2);
        Sponsor sponsor = new Sponsor("best company", "www.dot.com");
        withSponsor(sponsorType1, sponsor);

        List<Sponsor> sponsors = service.findDisplayableSponsorsBy(sponsorType1);

        assertThat(sponsors).containsExactly(sponsor, new Sponsor());
    }

    @Test
    public void getOnlyRealSponsorsIfMoreThenMinimalNo() {
        sponsorType1.minimalNo(0);
        withSponsor(sponsorType1, sponsor(0));

        List<Sponsor> sponsors = service.findDisplayableSponsorsBy(sponsorType1);

        assertThat(sponsors).containsExactly(sponsor(0));
    }

    @Test
    public void deleteSponsorById() {
        Integer id = 10;

        service.deleteSponsorBy(id);

        verify(sponsorRepository).delete(id);
    }

    @Test
    public void deleteSponsorTypeById() {
        Integer id = 20;

        service.deleteTypeBy(id);

        verify(sponsorTypeRepository).delete(id);
    }

    @Test
    public void getFirstTypeAsMainType() {
        mockRepositoryToHave(withSponsor(sponsorType1), withSponsor(sponsorType2), withSponsor(sponsorType3));

        List<SponsorType> mainTypes = service.getMainTypes();

        assertThat(mainTypes).containsOnly(sponsorType1);
    }

    @Test
    public void getEmptyTypeAsMainIfNoDisplayableTypes() {
        mockRepositoryToHave(sponsorType1);

        List<SponsorType> mainTypes = service.getMainTypes();

        assertThat(mainTypes).isEmpty();
    }

    @Test
    public void getEmptyListAsMainIfNoTypes() {
        mockRepositoryToHave();

        List<SponsorType> mainTypes = service.getMainTypes();

        assertThat(mainTypes).isEmpty();
    }

    @Test
    public void getSecondAndThirdTypeAsOthers() {
        mockRepositoryToHave(withSponsor(sponsorType1), withSponsor(sponsorType2), withSponsor(sponsorType3),
                withSponsor(sponsorType(30, 30)));

        List<SponsorType> otherTypes = service.getOtherTypes();

        assertThat(otherTypes).containsExactly(sponsorType2, sponsorType3);
    }

    @Test
    public void getOnlySecondTypeAsOthersIfOnlyTwoTypesAvailable() {
        mockRepositoryToHave(withSponsor(sponsorType1), withSponsor(sponsorType2));

        List<SponsorType> otherTypes = service.getOtherTypes();

        assertThat(otherTypes).containsExactly(sponsorType2);
    }

    @Test
    public void getEmptyListAsOthersIfNoDisplayableTypes() {
        mockRepositoryToHave(sponsorType1);

        List<SponsorType> otherTypes = service.getOtherTypes();

        assertThat(otherTypes).isEmpty();
    }

    @Test
    public void getEmptyTypeAsOthersIfNoTypes() {
        mockRepositoryToHave();

        List<SponsorType> otherTypes = service.getOtherTypes();

        assertThat(otherTypes).isEmpty();
    }

    @Test
    // TODO: displaying photo placeholders in paage
    public void shouldGetOnlyPlaceholdersWithLogoAndUrlFromTypeIfNoDisplayableSponsors() {
        sponsorType1.minimalNo(2).logoPlaceholder("logo_url").urlPlaceholder("web_url");
        mockRepositoryToHave(sponsorType1);

        List<Sponsor> sponsors = service.findDisplayableSponsorsBy(sponsorType1);

        assertThat(sponsors).hasSize(2)
                .extracting("webPage").containsOnly("web_url");
    }

    @Test
    public void notChangePositionIfSavingExistingType() {

        service.save(sponsorType(1, 1));

        ArgumentCaptor<SponsorType> captor = ArgumentCaptor.forClass(SponsorType.class);
        verify(sponsorTypeRepository).save(captor.capture());
        assertThat(captor.getValue().getPosition()).isEqualTo(1);
    }

    @Test
    public void findTypesWithSponsors() {
        sponsorType1.minimalNo(10);
        withSponsor(sponsorType2);
        mockRepositoryToHave(sponsorType1, sponsorType2);

        List<SponsorType> types = service.findTypesWithSponsors();

        assertThat(types).containsOnly(sponsorType2);
    }

    @Test
    public void saveLogoAfterSavingSponsor() {
        FileUpload fileUpload = mock(FileUpload.class);

        service.save(sponsor(0), fileUpload);

        InOrder inOrder = inOrder(sponsorRepository, fileService);
        inOrder.verify(sponsorRepository).save(sponsor(0));
        inOrder.verify(fileService).save(fileUpload, sponsor(0));

    }

    private SponsorType withSponsor(SponsorType sponsorType) {
        withSponsor(sponsorType, new Sponsor());
        return sponsorType;
    }

    private void withSponsor(SponsorType sponsorType, Sponsor... sponsors) {
        when(sponsorRepository.findByType(sponsorType)).thenReturn(newArrayList(sponsors));
    }

    private SponsorType sponsorType(int id, int order) {
        return new SponsorType().position(order).minimalNo(0).id(id);
    }

    private void mockRepositoryToHave(SponsorType... sponsorTypes) {
        when(sponsorTypeRepository.findAllSorted()).thenReturn(newArrayList(sponsorTypes));
    }

    private void verifyPositionChangedTo(SponsorType sponsorType, int order) {
        verify(sponsorTypeRepository).save(sponsorType(sponsorType.getId(), order));
    }
}

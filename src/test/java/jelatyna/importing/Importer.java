package jelatyna.importing;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Charsets;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

import jelatyna.domain.Participant;
import jelatyna.repositories.ParticipantRepository;
import jelatyna.services.RandomGenerator;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring.xml"})
@TransactionConfiguration(defaultRollback = false)
@Transactional
public class Importer {

    @Autowired
    private ParticipantRepository repository;

    @Autowired
    private RandomGenerator randomGenerator;

    @Test
    public void name() throws IOException {
        Path path = Paths.get("/Users/margielm/jelatyna", "users.csv");
        try (Stream<String> lines = Files.lines(path)) {
            List<String[]> participants = lines
                    .map(line -> line.split(";")).collect(Collectors.toList());
            for (String[] participant : participants) {
                int count = Integer.parseInt(participant[0]);
                while (count-- != 0) {
                    System.out.println("sending " + participant[1]);
                    repository.save(new Participant().mail(participant[1]).token(randomGenerator.getRandomId()));
                }
            }
        }
    }

    @Test
    @Ignore
    public void export() throws Exception {
        Path path = Paths.get("/Users/margielm/jelatyna", "hashes");

        HashFunction hashFunction = Hashing.sha1();
        List<String> hashes = repository.findAll().stream()
                .map(Participant::getMail)
                .map(mail -> hashFunction.hashString(mail, Charsets.UTF_8).toString())
                .collect(Collectors.toList());

        Files.write(path, hashes, StandardOpenOption.WRITE);

    }
}

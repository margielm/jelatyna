package jelatyna.api;

import java.util.Base64;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;

public class TwitterShould {

    @Test
    public void get_token() {
        try {
            CloseableHttpClient httpclient = HttpClients.createDefault();
            HttpPost post = new HttpPost("https://api.twitter.com/oauth2/token");
            Base64.Encoder encoder = Base64.getEncoder();

            post.setHeader("Authorization", "Basic " +  getPassword(encoder));
//            post.setHeader("Content-Type", );
            post.setEntity(new StringEntity("grant_type=client_credentials", ContentType.create("application/x-www-form-urlencoded", "UTF-8")));

            CloseableHttpResponse response = httpclient.execute(post);
            JsonNode node = new ObjectMapper().readTree(EntityUtils.toString(response.getEntity()));
            System.out.println(node.get("access_token"));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private String getPassword(Base64.Encoder encoder) {
        return encoder
                .encodeToString("7ouqIqvrgXivnf1GCPT2g53zB:yj9eMGTKWqTkvVfvOSw8IN46C32xbglbotabHhJ8cdn8ca3g4J".getBytes());
    }

}

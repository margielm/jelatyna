package jelatyna.assertions;

import org.apache.wicket.util.tester.TagTester;

import static org.assertj.core.api.Assertions.*;

public class TagAssert {

    private TagTester tagTester;

    public TagAssert(TagTester tagTester) {
        this.tagTester = tagTester;
    }

    public void hasAttribute(String name, String value) {
        assertThat(tagTester.getAttribute(name)).isEqualTo(value);
    }

    public TagAssert hasContent(String value) {
        assertThat(tagTester.getValue()).isEqualTo(value);
        return this;
    }
}
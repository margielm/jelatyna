package jelatyna.assertions;

import org.apache.wicket.util.tester.FormTester;

import static org.assertj.core.api.Assertions.*;

public class FormAssert {

    private FormTester formTester;

    public FormAssert(FormTester formTester) {
        this.formTester = formTester;
    }

    public FormAssert hasValue(String id, String value) {
        assertThat(formTester.getTextComponentValue(id)).isEqualTo(value);
        return this;
    }

}

package jelatyna.repositories;

import jelatyna.IntegrationTest;
import jelatyna.domain.MainMenu;
import jelatyna.domain.MenuItem;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;


public class MenuRepositoryShould extends IntegrationTest {
    @Autowired
    private MenuRepository repository;
    private MenuItem mainMenu = new MainMenu().children(new MenuItem("main item"));

    @Test
    public void saveMenuItem() {
        repository.save(new MenuItem("name"));

        assertThat(repository.findAll()).hasSize(1);
    }

    @Test
    public void saveAndFetchMainMenu() {
        repository.save(mainMenu);

        MenuItem fetchedMenu = repository.getMainMenu();

        assertEquals(mainMenu, fetchedMenu);
    }

    @Test
    public void removeNotExistingMenuItem() {
        repository.delete(new MenuItem(""));
    }

}

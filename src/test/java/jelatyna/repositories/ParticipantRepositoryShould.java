package jelatyna.repositories;

import jelatyna.IntegrationTest;
import jelatyna.domain.Participant;
import jelatyna.domain.RegistrationStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.google.common.collect.Lists.*;
import static jelatyna.domain.RegistrationStatus.*;
import static org.assertj.core.api.Assertions.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-spring.xml")
@Transactional
public class ParticipantRepositoryShould extends IntegrationTest {

    @Autowired
    private ParticipantRepository repository;

    @Test
    public void returnParticipantByMail() {
        // given
        String mail = "abc@mail.com";
        Participant participant = save(new Participant().mail(mail).participated(true));

        // when
        Participant foundParticipant = repository.findByMail(mail);

        // then
        assertThat(foundParticipant.getId()).isEqualTo(participant.getId());
    }

    @Test
    public void fetchAllParticipantsCitiesStartingWithGivenString() {
        save(new Participant().city("Warszawa"));
        save(new Participant().city("warka"));
        save(new Participant().city("Wrocław"));
        save(new Participant().city("Kraków"));

        List<String> cities = repository.fetchCitiesStartingWith("Wa%");

        assertThat(cities).containsOnly("WARSZAWA", "WARKA");
    }

    @Test
    public void fetchDistinctParticipantsCities() {
        save(new Participant().city("Warszawa"));
        save(new Participant().city("warszawa"));

        List<String> cities = repository.fetchCitiesStartingWith("Wa%");

        assertThat(cities).containsExactly("WARSZAWA");
    }

    @Test
    public void fetchParticipantsWithNewStatus() {
        Participant participant1 = save(participant(NEW));
        Participant participant2 = save(participant(NEW));
        save(participant(CONFIRMED));

        List<Participant> participants = repository.findByRegistrationTypeIn(newArrayList(NEW));

        assertThat(participants).containsOnly(participant1, participant2);
    }

    @Test
    public void fetchParticipantsWithConfirmedOrCanceledStatus() {
        Participant participant1 = save(participant(CONFIRMED));
        Participant participant2 = save(participant(CANCELED));
        save(participant(NEW));

        List<Participant> participants = repository.findByRegistrationTypeIn(newArrayList(CONFIRMED, CANCELED));

        assertThat(participants).containsOnly(participant1, participant2);
    }

    private Participant participant(RegistrationStatus status) {
        return new Participant().status(status);
    }

    private Participant save(Participant participant) {
        return repository.saveAndFlush(participant);
    }
}

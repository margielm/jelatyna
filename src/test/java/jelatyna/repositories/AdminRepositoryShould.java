package jelatyna.repositories;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import jelatyna.IntegrationTest;
import jelatyna.domain.Admin;

public class AdminRepositoryShould extends IntegrationTest {
    @Autowired
    private AdminRepository repository;

    @Test
    public void findUserByUserName() {
        Admin user = new Admin().firstName("Michał").lastName("Margiel").mail("a@a.pl");
        repository.saveAndFlush(user);

        Admin foundUser = repository.findByMail("a@a.pl");

        assertEquals("a@a.pl", foundUser.getMail());
    }

    @Test
    public void notFindUserByNotExistingUserName() {
        Admin user = new Admin().firstName("Michał").lastName("Margiel").mail("a@a.pl");
        repository.saveAndFlush(user);

        Admin foundUser = repository.findByMail("wrong@mail.pl");

        assertThat(foundUser).isNull();
    }
}

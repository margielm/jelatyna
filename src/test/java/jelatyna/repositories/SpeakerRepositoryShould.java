package jelatyna.repositories;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import jelatyna.IntegrationTest;
import jelatyna.domain.Presentation;
import jelatyna.domain.Speaker;

public class SpeakerRepositoryShould extends IntegrationTest {
    private static final boolean ACCEPTED = Boolean.TRUE;

    private static final boolean NOT_ACCEPTED = Boolean.FALSE;

    @Autowired
    private SpeakerRepository repository;

    @Autowired
    private PresentationRepository presentationRepository;

    @Test
    public void findSpeakerByMail() {
        String mail = "jan.kowalski@domena.pl";
        Speaker expectedSpeaker = new Speaker().mail(mail);
        save(expectedSpeaker);
        repository.saveAndFlush(new Speaker().mail("michal.nowak@domena.pl"));

        Speaker foundSpeaker = repository.findByMail(mail);

        assertEquals(expectedSpeaker, foundSpeaker);
    }

    @Test
    public void findOnlyAcceptedSpeakersWithAttLeastOneAcceptedPresentations() {

        Speaker speaker = createAndSaveSpeaker("b", ACCEPTED);
        createAndSavePresentation("b1", ACCEPTED).addSpeaker(speaker);
        createAndSavePresentation("b2", ACCEPTED).addSpeaker(speaker);
        createAndSavePresentation("a", ACCEPTED).addSpeaker(createAndSaveSpeaker("a", ACCEPTED));
        createAndSavePresentation("c", ACCEPTED).addSpeaker(createAndSaveSpeaker("c", NOT_ACCEPTED));

        List<Speaker> speakers = repository.allAccepted();

        assertThat(speakers)
                .extracting("firstName").containsOnly("a", "b");
    }

    @Test
    public void shouldNotReturnNotAcceptedSpeakers() {
        createAndSaveSpeaker("a", NOT_ACCEPTED)
                .addPresentation(createAndSavePresentation("a_1", ACCEPTED))
                .addPresentation(createAndSavePresentation("a_2", NOT_ACCEPTED));

        List<Speaker> speakers = repository.allAccepted();

        assertThat(speakers).isEmpty();
    }

    @Test
    public void shouldNotReturnSpeakerWithNotAcceptedPresentation() {
        createAndSaveSpeaker("a", ACCEPTED)
                .addPresentation(createAndSavePresentation("a_1", NOT_ACCEPTED));

        List<Speaker> speakers = repository.allAccepted();

        assertThat(speakers).isEmpty();
    }

    @Test
    public void findSpeakerByToken() {
        Speaker speaker = new Speaker().mail("my@mail.com").token("zZ5SWL9A95Kvm6EJH2Ae7SPuxiwnRjIhimbPVFmZfKDmLN8cArusQi442nkYWX");
        save(speaker);

        Speaker foundSpeaker = repository.findByToken("zZ5SWL9A95Kvm6EJH2Ae7SPuxiwnRjIhimbPVFmZfKDmLN8cArusQi442nkYWX");

        assertThat(foundSpeaker.getMail()).isEqualTo(speaker.getMail());
    }

    private Presentation createAndSavePresentation(String name, boolean accepted) {
        Presentation presentation = new Presentation().setTitle(name).accepted(accepted);
        return presentationRepository.saveAndFlush(presentation);
    }

    private Speaker createAndSaveSpeaker(String firstName, boolean acceptance) {
        Speaker speaker = new Speaker().firstName(firstName).accepted(acceptance);
        return save(speaker);
    }

    private Speaker save(Speaker speaker) {
        return repository.saveAndFlush(speaker);
    }
}

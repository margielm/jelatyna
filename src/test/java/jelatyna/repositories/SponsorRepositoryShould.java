package jelatyna.repositories;

import static org.assertj.core.api.Assertions.*;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import jelatyna.WicketIntegrationTest;
import jelatyna.domain.Sponsor;

public class SponsorRepositoryShould extends WicketIntegrationTest {

    @Autowired
    private SponsorRepository repository;

    @Test
    public void find_sponsor_by_name() {
        repository.save(new Sponsor("name 1", ""));
        repository.save(new Sponsor("name 2", ""));
        repository.flush();

        Sponsor sponsor = repository.findByName("name 2");

        assertThat(sponsor.getName()).isEqualTo("name 2");
    }
}

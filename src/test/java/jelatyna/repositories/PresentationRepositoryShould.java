package jelatyna.repositories;

import jelatyna.IntegrationTest;
import jelatyna.domain.Presentation;
import jelatyna.domain.Speaker;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.assertj.core.api.Assertions.*;

public class PresentationRepositoryShould extends IntegrationTest {
    @Autowired
    private PresentationRepository repository;
    @Autowired
    private SpeakerRepository speakerRepository;
    @PersistenceContext
    private EntityManager em;


    @Test
    public void deletePresentationFromTheSpeaker() {
        Presentation presentation = repository.save(new Presentation());
        Speaker speaker = speakerRepository.saveAndFlush(new Speaker().firstName("name").addPresentation(presentation));

        repository.delete(presentation);
        em.flush();
        em.clear();

        assertThat(repository.findOne(presentation.getId())).isNull();
        assertThat(speakerRepository.findOne(speaker.getId()).getPresentations()).isEmpty();

    }

    @Test
    public void acceptMultipleSpeakers() {
        //given
        Speaker speaker1 = persistSpeakerWith("speaker1");
        Speaker speaker2 = persistSpeakerWith("speaker2");
        Speaker speaker3 = persistSpeakerWith("speaker3");
        Presentation presentation = new Presentation().owner(speaker1).addSpeaker(speaker2).addSpeaker(speaker3);

        //when
        repository.saveAndFlush(presentation);
        em.clear();

        //then
        Presentation found = repository.findOne(presentation.getId());
        assertThat(found.getSpeakers()).containsExactly(speaker1, speaker2, speaker3);
    }

    private Speaker persistSpeakerWith(String firstName) {
        return speakerRepository.saveAndFlush(new Speaker().firstName(firstName));
    }
}

package jelatyna;

import jelatyna.domain.Admin;
import jelatyna.domain.User;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.Before;

public class WicketTest {
    private TestApplicationWithSpringContextMock app = TestApplicationWithSpringContextMock.withServicesMocked();
    protected WicketTester tester = new WicketTester(app);

    @Before
    public void setupServices() {
    }

    public void put(Object... services) {
        for (Object service : services) {
            app.putBean(service);
        }
    }

    protected void withAdmin() {
        withUser(new Admin());
    }

    protected User<?> withUser(User<?> user) {
        return ConfituraSession.get().setUser(user);
    }
}

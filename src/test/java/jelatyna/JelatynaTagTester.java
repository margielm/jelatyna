package jelatyna;

import org.apache.wicket.util.tester.TagTester;
import org.jsoup.Jsoup;

import static org.assertj.core.api.Assertions.*;

public class JelatynaTagTester {

    private final TagTester tagTester;

    public JelatynaTagTester(TagTester tagTester) {
        this.tagTester = tagTester;
    }

    public TagTester getChild(String attribute, String value) {
        return tagTester.getChild(attribute, value);
    }

    public TagTester getChildByWicketId(String wicketId) {
        return getChild("wicket:id", wicketId);
    }

    public void assertHasText(String text) {
        assertThat(Jsoup.parse(tagTester.getMarkup()).text()).isEqualTo(text);
    }
}
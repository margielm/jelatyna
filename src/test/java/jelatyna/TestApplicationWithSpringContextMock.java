package jelatyna;

import jelatyna.components.menu.MenuLinks;
import jelatyna.repositories.MenuRepository;
import jelatyna.services.SponsorService;
import org.apache.wicket.Session;
import org.apache.wicket.request.Request;
import org.apache.wicket.request.Response;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.apache.wicket.spring.test.ApplicationContextMock;

import java.util.Locale;

import static org.mockito.Mockito.*;

public class TestApplicationWithSpringContextMock extends Confitura {
    private final boolean withInjector;
    private ApplicationContextMock applicationContext = new ApplicationContextMock();

    private TestApplicationWithSpringContextMock(boolean withInjector) {
        this.withInjector = withInjector;

    }

    public static TestApplicationWithSpringContextMock withInjector() {
        return new TestApplicationWithSpringContextMock(true);
    }

    public static TestApplicationWithSpringContextMock withServicesMocked() {
        TestApplicationWithSpringContextMock app = new TestApplicationWithSpringContextMock(true);
        app.putBean(mock(MenuRepository.class));
        app.putBean(mock(MenuLinks.class));
        app.putBean(mock(SponsorService.class));
        return app;
    }

    public static TestApplicationWithSpringContextMock withoutInjector() {
        return new TestApplicationWithSpringContextMock(false);
    }

    @Override
    protected void productionInit() {
        getMarkupSettings().setStripWicketTags(false);
        if (withInjector) {
            getComponentInstantiationListeners().add(new SpringComponentInjector(this, applicationContext));
        }
    }

    public void putBean(Object bean) {
        applicationContext.putBean(bean);
    }

    @Override
    public Session newSession(Request request, Response response) {
        Session session = super.newSession(request, response);
        session.setLocale(new Locale("pl", "PL"));
        return session;
    }
}

package jelatyna.components.presentation;

import jelatyna.WicketTest;
import jelatyna.domain.Presentation;
import jelatyna.services.FileService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PresentationLinksPanelShould extends WicketTest {
    @Mock
    private FileService fileService;
    private Presentation presentation = new Presentation();

    @Override
    @Before
    public void setupServices() {
        put(fileService);
    }

    @Test
    public void renderLinkToYouTubeIfPresentationContains() {
        presentation.youTube("http://youtube.com");

        startComponent();

        assertThat(tester.getTagByWicketId("youTube").getAttribute("href")).isEqualTo(presentation.getYouTube());
    }


    @Test
    public void notRenderLinkToYouTubeIfEmpty() {
        presentation.youTube(null);

        startComponent();

        tester.assertInvisible("id:youTube");
    }

    @Test
    public void renderLinkToParleysIfContains() {
        presentation.parleys("http://parleys.com");

        startComponent();

        assertThat(tester.getTagByWicketId("parleys").getAttribute("href")).isEqualTo(presentation.getParleys());
    }

    @Test
    public void notRenderLinkToParleysIfEmpty() {
        presentation.parleys(null);

        startComponent();

        tester.assertInvisible("id:parleys");
    }

    @Test
    public void renderLinkToPresentationFileIfContains() {
        presentation.fileName("file");
        when(fileService.getUrlTo(presentation)).thenReturn("url");

        startComponent();

        assertThat(tester.getTagByWicketId("download").getAttribute("href")).isEqualTo("url");
    }

    @Test
    public void notRenderLinkToPresentationFileIfEmpty() {
        presentation.fileName(null);

        startComponent();

        tester.assertInvisible("id:download");
    }

    private void startComponent() {
        tester.startComponentInPage(new PresentationLinksPanel("id", presentation));
    }
}

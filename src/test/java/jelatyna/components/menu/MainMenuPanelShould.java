package jelatyna.components.menu;

import jelatyna.TestApplicationWithSpringContextMock;
import jelatyna.domain.MainMenu;
import jelatyna.domain.MenuItem;
import jelatyna.repositories.MenuRepository;
import org.apache.wicket.util.tester.TagTester;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static java.lang.String.*;
import static org.apache.wicket.util.tester.TagTester.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@Ignore
public class MainMenuPanelShould {
    private WicketTester tester = new WicketTester(TestApplicationWithSpringContextMock.withoutInjector());
    private MainMenu menu = new MainMenu();
    @Mock
    private MenuRepository menuRepository;
    @Mock
    private MenuLinks menuLinks;
    private MainMenuPanel panel;

    @Before
    public void setupMenuRepository() {
        when(menuRepository.getMainMenu()).thenReturn(menu);
    }

    @Test
    public void renderMenuWithOneLabel() {
        add(publishedMenu("item"));

        createAndStartPanel();

        menu().size(1).hasLabels("item");
    }

    @Test
    public void renderMenuWithTwoLabels() {
        add(publishedMenu("A"));
        add(publishedMenu("B"));

        createAndStartPanel();

        menu().size(2).hasLabels("A", "B");
    }

    @Test
    public void renderMenuWithTwoLabelsButOnlyOnePublished() {
        add(notPublishedMenu("A"));
        add(publishedMenu("B"));

        createAndStartPanel();

        menu().size(1).hasLabels("B");
    }

    private MenuTester menu() {
        return MenuTester.menu(tester);
    }

    private MenuItem notPublishedMenu(String name) {
        return new MenuItem(name).published(false);
    }

    private MenuItem add(MenuItem item) {
        return menu.addMenuItem(item);
    }

    private MenuItem publishedMenu(String name) {
        return new MenuItem(name).published(true);
    }

    private void createAndStartPanel() {
        panel = new MainMenuPanel("id", menuRepository, menuLinks);
        tester.startComponentInPage(panel);
    }

    static class MenuTester {

        private List<TagTester> menuTags;

        private MenuTester(WicketTester tester) {
            menuTags = tester.getTagsByWicketId("menuItems");
        }

        public static MenuTester menu(WicketTester tester) {
            return new MenuTester(tester);
        }

        public void hasLabels(String... labels) {
            for (int idx = 0; idx < labels.length; idx++) {
                assertThat(getMenuLabel(idx).getMarkup())
                        .contains(format("<span wicket:id=\"menuItem\">%s</span>", labels[idx]));
            }
        }

        private TagTester getMenuLabel(int idx) {
            return createTagByAttribute(menuTags.get(idx).getMarkup(), "wicket:id", "menuItem");
        }

        public MenuTester size(int size) {
            assertThat(menuTags).hasSize(size);
            return this;
        }

    }

}

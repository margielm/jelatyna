package jelatyna.components.user;

import static org.mockito.Mockito.*;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.util.tester.FormTester;
import org.junit.Ignore;
import org.junit.Test;

import jelatyna.domain.Admin;
import jelatyna.pages.admin.user.ListAdminPage;
import jelatyna.pages.confitura.c4p.AbstractPhotoUploadSizeValidationTest;
import jelatyna.services.AdminService;

public class UserFormPanelShould extends AbstractPhotoUploadSizeValidationTest {

    @Test
    public void notAllowPhotosBiggerThanUploadedPhotoMaxSize() {
        shouldNotAllowPhotosBiggerThanUploadedPhotoMaxSize();
    }

    @Test
    @Ignore
    public void showErrorMessageIfPasswordsAreDifferent() {
        tester.startComponentInPage(createPhotoUploadFormPanelUnderTest());
        FormTester formTester = tester.newFormTester("userForm:form");
        formTester.setValue("firstName", "John");
        formTester.setValue("lastName", "Smith");
        formTester.setValue("mail", "my@mail.com");
        formTester.setValue("passwordPanel:password", "a");
        formTester.setValue("passwordPanel:repassword", "b");

        formTester.submit();

        tester.assertErrorMessages("Hasło i Powtórz hasło muszą być równe.");
    }

    @Test
    public void shouldNotAllowForBioShorterThanSpecifiedLength() {
        tester.startComponentInPage(createPhotoUploadFormPanelUnderTest());
        FormTester formTester = tester.newFormTester("userForm:form");
        formTester.setValue("firstName", "John");
        formTester.setValue("lastName", "Smith");
        formTester.setValue("mail", "my@mail.com");
        formTester.setValue("passwordPanel:password", "aaaaa");
        formTester.setValue("passwordPanel:repassword", "aaaaa");
        formTester.setValue("bio", "to short bio");

        formTester.submit();

        tester.assertErrorMessages("'O sobie' musi mieć co najmniej 300 znaków.");
    }


    @Override
    protected Panel createPhotoUploadFormPanelUnderTest() {
        Admin admin = mock(Admin.class);
        when(admin.isNew()).thenReturn(true);
        return new UserFormPanel<Admin>(mock(AdminService.class), admin, ListAdminPage.class);
    }
}
